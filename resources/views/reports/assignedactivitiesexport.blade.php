<!DOCTYPE html>
<table>
    <thead class="btn-dark">
    <tr>
        <th>Portfolio Name</th>
        <th>Activity Assigned</th>
        <th>Activity Value</th>
        @role('admin')
        <th>User Assigned to</th>
        @endrole
        <th>Due Date</th>
        <th class="last">Status</th>

    </tr>
    </thead>
    <tbody>
    @foreach($activities as $client_name => $activity_array)
        @php
            $clientname = "";
        @endphp
        @foreach($activity_array as $activity)
            @if(isset($activity["client_id"]))
                <tr>
                    <td>
                            @if($client_name == "" || ($client_name != $clientname))
                                {{ $client_name }}
                            @endif
                        </td>
                    <td>{{ $activity['activity_name']}}</td>
                    <td>{{ $activity['activity_value']}}</td>
                    @role('admin')
                    <td>
                        @php
                            $user_string = '';
                            foreach ($activity["user"] as $user){
                            //foreach ($user as $value){
                            $user_string = $user_string.$user.'<br />';
                            //}
                            }

                            echo $user_string;
                        @endphp
                    </td>
                    @endrole
                    <td class="due_date">{{ $activity['due_date'] }}</td>
                    <td class="last" align="center" style="background-color: {{ $activity['class'] }}">{{ $activity['class'] }}</td>
                </tr>
            @endif
            @php
                $clientname = $client_name;
            @endphp
        @endforeach
    @endforeach
    </tbody>
</table>