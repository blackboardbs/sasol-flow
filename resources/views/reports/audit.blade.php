@extends('adminlte.default')

@section('title') Audit Report @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3><i class="fa fa-line-chart"></i> @yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-9" style="text-align: left;">
            <form class="form-inline mt-3">
                <span class="mt-2">Show &nbsp;</span>
                {{Form::select('activities_search',$activities_dropdown ,old('activities_search'),['class'=>'form-control form-control-sm mt-2'])}}
                <span class="mt-2">&nbsp;for&nbsp;</span>
                {{Form::select('client_search',$clients_dropdown ,old('client_search'),['class'=>'form-control form-control-sm mt-2'])}}
                <span class="mt-2">&nbsp; for &nbsp;</span>
                {{Form::select('user',$users_dropdown,old('user'),['class'=>'form-control form-control-sm mt-2'])}}
                <span class="mt-2">&nbsp; from &nbsp;</span>
                {{Form::date('f',old('f'),['class'=>'form-control form-control-sm mt-2'])}}
                <span class="mt-2">&nbsp; to &nbsp;</span>
                {{Form::date('t',old('t'),['class'=>'form-control form-control-sm mt-2'])}}

                <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2 mt-2"><i class="fa fa-search"></i> Search</button>
                <a href="{{route('reports.audit_report')}}" class="btn btn-sm btn-info mt-2"><i class="fa fa-eraser"></i> Clear</a>
            </form>
            </div>
            <div class="col-sm-3" style="text-align: right;">
                <form id="download_pdf" class="form-inline mt-3" style="display: inline-block" action="{{route('audit.pdfexport')}}">
                    <input type="hidden" id="pdf_activities_search" name="pdf_activities_search" value="{{isset($_GET['activities_search'])?$_GET['activities_search']:''}}" />
                    <input type="hidden" id="pdf_client_search" name="pdf_client_search" value="{{isset($_GET['client_search'])?$_GET['client_search']:''}}" />
                    <input type="hidden" id="pdf_user" name="pdf_user" value="{{isset($_GET['user'])?$_GET['user']:''}}" />
                    <input type="hidden" id="pdf_f" name="pdf_f" value="{{isset($_GET['f'])?$_GET['f']:''}}" />
                    <input type="hidden" id="pdf_t" name="pdf_t" value="{{isset($_GET['t'])?$_GET['t']:''}}" />
                    <button style="margin-right: 10px;" type="submit" class="btn btn-default btn-sm"><i class="fa fa-file-pdf-o"></i> PDF</button>
                </form>
                <form id="download_xls" class="form-inline mt-3" style="display: inline-block" action="{{route('audit.export')}}">
                    <input type="hidden" id="pdf_activities_search" name="pdf_activities_search" value="{{isset($_GET['activities_search'])?$_GET['activities_search']:''}}" />
                    <input type="hidden" id="pdf_client_search" name="pdf_client_search" value="{{isset($_GET['client_search'])?$_GET['client_search']:''}}" />
                    <input type="hidden" id="pdf_user" name="pdf_user" value="{{isset($_GET['user'])?$_GET['user']:''}}" />
                    <input type="hidden" id="pdf_f" name="pdf_f" value="{{isset($_GET['f'])?$_GET['f']:''}}" />
                    <input type="hidden" id="pdf_t" name="pdf_t" value="{{isset($_GET['t'])?$_GET['t']:''}}" />
                    <button style="margin-right: 10px;" type="submit" class="btn btn-default btn-sm"><i class="fa fa-file-pdf-o"></i> Excel</button>
                </form>
            </div>
        </div>
        <hr>
        <div class="col-sm-12">
            <div class="table-responsive js-pscroll">
                <table id="logs" class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: block;overflow-x:auto !important;max-height: 75vh;border-collapse: collapse">
                    <thead>
                        <tr width="100%">
                            <th width="20%">Action</th>
                            <th width="40%">Portfolio Name</th>
                            <th width="20%">Activity Name</th>
                            <th width="20%">Activity Value</th>
                            <th width="20%">User</th>
                            <th width="20%">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($log_array as $activity)
                        <tr>
                            <td>{{$activity["action"]}}</td>
                            <td>{{$activity["client"]}}</td>
                            <td>{{$activity["activity_name"]}}</td>
                            <td>{{$activity["new_value"]}}</td>
                            <td>{{$activity["user"]}}</td>
                            <td>{{$activity["created_at"]}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">No records found.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@section('extra-css')

    <style>
        thead th {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            top: 0;
            z-index: 2;
            color: #fff;
            background-color: #343a40;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
        }

        tbody td:first-child {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            left: 0;
        }
        thead th:first-child {
            left: -1px;
            z-index: 3;
        }
        tbody td:first-child {
            left: -1px;
            z-index: 1;
            background: #FFFFFF;
            border-left: 1px solid #ffffff
        }

        .column-shadow{
            box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -webkit-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            border-left: 1px solid #dee2e6;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $(document).ready(function() {
            $('#logs').DataTable();

            $('.js-pscroll').each(function () {
                var ps = new PerfectScrollbar(this);

                $(window).on('resize', function () {
                    ps.update();
                })

                $(this).on('ps-x-reach-start', function () {
                    $('.table100-firstcol').removeClass('column-shadow');
                });

                $(this).on('ps-scroll-x', function () {
                    $('.table100-firstcol').addClass('column-shadow');
                });

            })
        })
    </script>
@endsection