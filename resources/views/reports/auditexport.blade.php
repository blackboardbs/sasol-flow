<table>
    <thead>
    <tr>
        <th>Action</th>
        <th>Portfoliio Name</th>
        <th>Activity Name</th>
        <th>Activity Value</th>
        <th>User</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($log_array as $activity)
        <tr>
            <td>{{$activity["action"]}}</td>
            <td>{{$activity["client"]}}</td>
            <td>{{$activity["activity_name"]}}</td>
            <td>{{$activity["new_value"]}}</td>
            <td>{{$activity["user"]}}</td>
            <td>{{$activity["created_at"]}}</td>
        </tr>
    @endforeach
    </tbody>
</table>