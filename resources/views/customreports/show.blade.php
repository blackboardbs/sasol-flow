@extends('flow.default')
@section('title') Show Report - {{$report_name}}@endsection
@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <div class="nav-btn-group">
            <button onclick="excelExport()" type="button" id="export_button" class="btn btn-success btn-sm mt-3 ml-2 float-right">Excel</button>
            <a href="{{route('custom_report.index')}}" class="btn btn-outline-primary mt-3 btn-sm float-right">Back</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100 pr-0">
            @yield('header')
            <div class="container-fluid index-container-content" style="height: calc(100% - 43px);">
                <div class="table-responsive mt-3" style="border: 1px solid #dee2e6;display: block;overflow-x:auto !important;max-height: 75vh;border-collapse: collapse">
                <div class="js-pscroll">
            <table id="report_table" class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: table;overflow-x:auto !important;max-height: 75vh;border-collapse: collapse;min-width: 100%;">
                <thead>
                    {{-- <tr class="filter">
                        <th style="vertical-align:middle">Name</th>
                        @foreach($fields as $k => $v)
                        <th style="vertical-align:middle">
                            <select name="{{$k}}" multiple class="chosen-select form-control form-control-sm">
                                @foreach($v as $key => $val)
                                    @if($val != null)
                                        <option value="{{$val == 'Blank' ? '' : $val}}">{{$val}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </th>
                        @endforeach
                    </tr> --}}
                    <tr class="non-filter">
                        <th>Name</th>
                        @foreach($field_names as $k => $v)
                        <th>
                            {{$v}}
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                @forelse($clients as $client)
                    @if(isset($client['id']))
                    <tr>
                        <td class="table100-firstcol"><a href="{{route('clients.show',$client['id'])}}">{{$client['company']}}</a></td>
                        @foreach($client["data"] as $key => $val)
                                <td><a href="{{route('clients.show',$client["id"])}}">@if($val != strip_tags($val)) {!! $val !!} @else {{$val}} @endif</a></td>
                            @endforeach
                    </tr>
                    @endif
                @empty
                    <tr>
                        <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        </div>
            <small class="text-muted">Found <b>{{$total}}</b> clients matching those criteria.</small>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        thead th {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            top: 0;
            z-index: 2;
            box-shadow: 0 1px 1px rgba(0,0,0,.075);
            background: #FFFFFF;
        }

        tbody td:first-child {
            position: -webkit-sticky; /* for Safari */
            position: sticky;
            left: 0;
        }
        thead th:first-child {
            left: -1px;
            z-index: 3;
        }
        tbody td:first-child {
            left: -1px;
            z-index: 1;
            background: #FFFFFF;
            border-left: 1px solid #ffffff
        }

        .column-shadow{
            box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -moz-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -webkit-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -o-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            -ms-box-shadow: 8px 0px 10px 0px rgba(0, 0, 0, 0.05);
            border-left: 1px solid #dee2e6;
        }
    </style>
@endsection
@section('extra-js')
<script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
<script>
    
    $(document).ready(function()
    {
        $('select').on('change', function () {
            $(this).closest('form').submit();
        });
        $('.js-pscroll').each(function () {
            var ps = new PerfectScrollbar(this);

            $(window).on('resize', function () {
                ps.update();
            })

            $(this).on('ps-x-reach-start', function () {
                $('.table100-firstcol').removeClass('column-shadow');
            });

            $(this).on('ps-scroll-x', function () {
                $('.table100-firstcol').addClass('column-shadow');
            });

        });

        
    });
    function excelExport()
    {
        var type = 'xlsx';

        // $('.filter').hide();
        // $('.non-filter').show();

        var data = document.getElementById('report_table');

        var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});

        XLSX.write(file, { bookType: type, bookSST: true, type: 'base64' });

        XLSX.writeFile(file, 'file.' + type);

        // $('.filter').show();
        // $('.non-filter').hide();
    }
</script>
@endsection