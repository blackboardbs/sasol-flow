<html>
<head>
    <title>Assigned Actions Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table{
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 5px;
            font-size:12px;
        }

        th{
            background:#ccc;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }

        .fas{
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            line-height: 1;
        }
    </style>
</head>
<body>
<div class="table-responsive">
    <table class="table table-bordered table-sm table-hover">
        <thead class="btn-dark">
        <tr>
            <th>Portfolio Name</th>
            <th>Activity Assigned</th>
            <th>Activity Value</th>
            @role('admin')
            <th>User Assigned to</th>
            @endrole
            <th>Due Date</th>
            <th class="last">Status</th>
        </tr>
        </thead>
        <tbody>
        @forelse($activities as $client_name => $activity_array)
            @php
                $clientname = "";
            @endphp
            @foreach($activity_array as $activity)
                @if(isset($activity["client_id"]))
                    <tr>
                        <td><a href="{{route('clients.show',['client'=>$activity['client_id']])}}">
                                @if($client_name == "" || ($client_name != $clientname))
                                    {{ $client_name }}
                                @endif
                            </a></td>
                        <td><a href="{{route('clients.stepprogressaction',['client'=>$activity['client_id'],'step'=>$activity['step_id'],'action_id'=>$activity['action_id']])}}">{{ $activity['activity_name']}}</a></td>
                        <td>{{ $activity['activity_value']}}</td>
                        @role('admin')
                        <td>
                            @php
                                $user_string = '';
                                foreach ($activity["user"] as $user){
                                //foreach ($user as $value){
                                $user_string = $user_string.$user.'<br />';
                                //}
                                }

                                echo $user_string;
                            @endphp
                        </td>
                        @endrole
                        <td id="due_date">{{ $activity['due_date'] }}</td>
                        <td class="last" align="center"><i class="fas fa-circle" style="color: {{ $activity['class'] }}">&#9679;</i></td>
                    </tr>
                @endif
                @php
                    $clientname = $client_name;
                @endphp
            @endforeach
        @empty
            <tr>
                <td colspan="6">No records found.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>