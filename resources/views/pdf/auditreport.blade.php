<html>
<head>
    <title>Audit Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        table{
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
            padding: 5px;
            font-size:12px;
        }

        th{
            background:#ccc;
        }

        thead { display: table-header-group }
        tfoot { display: table-row-group }
        tr { page-break-inside: avoid }
    </style>
</head>
<body>
<div class="table-responsive">
    <table id="logs" class="table table-bordered table-sm table-hover" style="border: 1px solid #dee2e6;display: block;overflow-x:auto !important;max-height: 75vh;border-collapse: collapse">
        <thead>
        <tr width="100%">
            <th width="20%">Action</th>
            <th width="40%">Portfolio Name</th>
            <th width="20%">Activity Name</th>
            <th width="20%">Activity Value</th>
            <th width="20%">User</th>
            <th width="20%">Date</th>
        </tr>
        </thead>
        <tbody>
        @forelse($log_array as $activity)
            <tr>
                <td>{{$activity["action"]}}</td>
                <td>{{$activity["client"]}}</td>
                <td>{{$activity["activity_name"]}}</td>
                <td>{{$activity["new_value"]}}</td>
                <td>{{$activity["user"]}}</td>
                <td>{{$activity["created_at"]}}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6">No records found.</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
</body>
</html>