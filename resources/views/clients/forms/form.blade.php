@extends('clients.show')

@section('tab-content')
    <div class="col-sm-12">
        <h5 class="form-inline">{{$form->name}}</h5>
    </div>
    <div class="col-lg-12">
        @include('clients.forms.sections')
        <div class="table-responsive mt-3">
            {{Form::open(['url' => route('forms.storeform',[$client->id,$form->id]), 'method' => 'post','files'=>true])}}
            <input type="hidden" value="{{$form->id}}" name="form_id" id="form_id">

            @foreach($form_progress as $key => $step)
                <input type="hidden" value="{{$step["id"]}}" name="form_section_id" id="form_section_id">
                @foreach($step['activities'] as $activity)

                    <div id="list_{{$activity['id']}}" class="list-group-item activity" style="display:table;width:100%;">
                        <div style="display:table-cell;width:20px;/*vertical-align:middle*/"><i class="fa fa-circle" style="color: {{$client->process->getStageHex($activity['stage'])}}"></i> </div>
                        <div style="display:table-cell">

                            {{--<div id="list_{{$activity['id']}}" class="list-group-item" style="background-color: {{$client->process->getStageHex($activity['stage'])}}">--}}
                            {{-- <pre>
                                 @php var_dump($activity); @endphp
                         </pre>--}}
                            @if($activity['type'] == 'dropdown')
                                @php

                                    $arr = (array)$activity['dropdown_items'];
                                    $arr2 = (array)$activity['dropdown_values'];

                                @endphp
                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                            @elseif($activity['type'] == 'radio')
                                @php

                                    $arrradio = (array)$activity['radio_items'];
                                    $arrradio2 = (array)$activity['radio_values'];

                                @endphp
                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                            @elseif($activity['type'] == 'checkbox')
                                    @php

                                        $arrcheck = (array)$activity['checkbox_items'];
                                        $arrcheck2 = (array)$activity['checkbox_values'];

                                    @endphp
                                    <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                                @elseif($activity['type'] == 'amount')
                                <input type="hidden" id="old_currency_{{$activity['id']}}" name="old_currency_{{$activity['id']}}" value="{{(isset($activity['currency']) ? $activity['currency'] : old($activity['id']))}}">
                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}">
                            @else
                                <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}">
                            @endif
                            @php
                                // The Regular Expression filter
                                $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

                                // The Text you want to filter for urls
                                $text = $activity["name"];

                                // Check if there is a url in the text
                                if(preg_match($reg_exUrl, $text, $url)) {

                                // make the urls hyper links
                                echo preg_replace($reg_exUrl, '<a href="'.$url[0].'" target="_blank">'.$url[0].'</a> ', $text);

                                } else {

                                // if no urls in the text just return the text
                                echo $text;

                                }
                            @endphp

                            <small class="text-muted"> [{{$activity['type_display']}}] @if($activity['kpi']==1) <i class="fa fa-asterisk" title="Activity is required for step completion" style="color:#FF0000"></i> @endif</small>
                            @if($activity['tooltip'] != null)
                            <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline">
                                <a href="javascript:void(0)" style="display: inline-block;" data-toggle="tooltip" data-html="true" data-placement="left" title="{{$activity['tooltip']}}"><i class="fa fa-info-circle"></i> </a>
                            </div>
                            @endif

                            <div class="clearfix"></div>

                            @if($activity['type']=='date')
                                <input name="{{$activity['id']}}" type="date" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" placeholder="Insert date..."/>
                            @endif

                            @if($activity['type']=='text')
                                {{Form::text($activity['id'],(isset($activity['value']) ? $activity['value'] : old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...'])}}
                            @endif

                            @if($activity['type']=='textarea')
                                <textarea rows="3" name="{{$activity['id']}}" class="form-control form-control-sm text-area">{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}</textarea>
                            @endif

                            @if($activity['type']=='boolean')
                                {{Form::select($activity['id'],[1=>'Yes',0=>'No'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                            @endif

                                @if($activity['type']=='dropdown')

                                    {{-- Form::select($activity['id'],$activity['dropdown_items'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...']) --}}
                                    <select multiple="multiple" id="{{$activity['id']}}" name="{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                                        @php
                                            foreach((array) $arr as $key => $value){
                                                echo '<option value="'.$key.'" '.(in_array($key,$arr2) ? 'selected' : '').'>'.$value.'</option>';
                                            }
                                        @endphp
                                    </select>
                                    <div>
                                        <small class="form-text text-muted">
                                            Search and select multiple entries
                                        </small>
                                    </div>
                                    {{--{{Form::select($activity['id'],$activity['dropdown_items'],null,['id'=>$activity['id'],'class'=>'form-control'. ($errors->has($activity['id']) ? ' is-invalid' : ''),'multiple'])}}--}}
                                    @foreach($errors->get($activity['id']) as $error)
                                        <div class="invalid-feedback">
                                            {{ $error }}
                                        </div>
                                    @endforeach

                                @endif

                            @if($activity['type']=='radio')

                                @php
                                    foreach((array) $arrradio as $key => $value){
                                        echo '<div class="form-check form-check-inline"><input type="radio" class="form-check-input" name="'.$activity["id"].'" value="'.$key.'" '.(in_array($key,$arrradio2) ? 'checked' : '').'><label class="form-check-label">'.$value.'</label></div>';
                                    }
                                @endphp

                                {{--{{Form::select($activity['id'],$activity['dropdown_items'],null,['id'=>$activity['id'],'class'=>'form-control'. ($errors->has($activity['id']) ? ' is-invalid' : ''),'multiple'])}}--}}
                                @foreach($errors->get($activity['id']) as $error)
                                    <div class="invalid-feedback">
                                        {{ $error }}
                                    </div>
                                @endforeach

                            @endif

                            @if($activity['type']=='checkbox')

                                @php
                                    foreach((array) $arrcheck as $key => $value){
                                        echo '<div class="form-check form-check-inline"><input type="checkbox" class="form-check-input" name="'.$activity["id"].'[]" value="'.$key.'" '.(in_array($key,$arrcheck2) ? 'checked="checked"' : '').'><label class="form-check-label">'.$value.'</label></div>';
                                    }
                                @endphp

                                {{--{{Form::select($activity['id'],$activity['dropdown_items'],null,['id'=>$activity['id'],'class'=>'form-control'. ($errors->has($activity['id']) ? ' is-invalid' : ''),'multiple'])}}--}}
                                @foreach($errors->get($activity['id']) as $error)
                                    <div class="invalid-feedback">
                                        {{ $error }}
                                    </div>
                                @endforeach

                            @endif

                        </div>

                    </div>
                @endforeach
            @endforeach

            @if(count($process_progress)>0)

                <div class="blackboard-fab mr-3 mb-3">
                    <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-save"></i><span style="font-size:1rem;line-height: 1.8;padding-left:10px;float: right;display:block;text-align:left;">Save</span></button>
                </div>
            @endif

            {{Form::close()}}

        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
            min-width: 500px;
        }

        .chosen-container, .chosen-container-multi{
            width:100% !important;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }

        .tooltip-inner {
            max-width: 350px;
            /* If max-width does not work, try using width instead */
            width: 350px;
        }
    </style>
@endsection
@section('extra-js')

    <script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>

    <script>
        $(document).find('textarea').each(function () {
            var offset = this.offsetHeight - this.clientHeight;

            $(this).on('keyup input focus', function () {
                $(this).css('height', 'auto').css('height', this.scrollHeight + offset);
            });

            $(this).trigger("input");
        });
    </script>
@endsection