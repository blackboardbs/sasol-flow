<div class="mt-3 mr-0 ml-0">
    <div class="text-center blackboard-steps blackboard-form-sections">
        <ul class="progress-indicator">
            @if(isset($forms) && isset($client))
                @forelse($forms as $form)
                    <li class="completed0"> <span class="bubble"></span><a href="{{route('forms.dynamic_form',[$client,$form['form_id'],$form['id']])}}" style="color:#00000 !important;">{{$form['name']}}</a></li>
                @empty
                    <p>There are no steps assigned to this process.</p>
                @endforelse
            @else
                @forelse(auth()->user()->office()->processes->first()->steps as $step)
                    <div class="col-lg blackboard-step-{{$step->id}}">
                        {{$step->name}}
                    </div>
                @empty
                    <p>There are no steps assigned to this process.</p>
                @endforelse
            @endif
        </ul>
    </div>

</div>

