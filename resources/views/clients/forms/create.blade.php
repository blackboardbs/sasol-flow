@extends('clients.show')

@section('tab-content')
    <div class="col-lg-12">
        <div id="autoSave" style="position: fixed; top:70px;right:0px;z-index:999999;min-width:300px;border-radius:7px 0px 0px 7px;"></div>
        {{Form::open(['url' => route('forms.storecrf',$client->id), 'method' => 'post','id'=>'crfform'])}}
        {{Form::hidden('form_id',null,['class'=>'form-control form-control-sm','id'=>'form_id'])}}
        <div class="row mt-3">
            <div class="col-lg-12">
                <strong>Section 1</strong><br />
                <strong>Internal Management Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td width="33.33%">
                            {{Form::label('sec1_date', 'Date')}}
                            {{Form::date('sec1_date',now(),['class'=>'form-control form-control-sm'. ($errors->has('sec1_date') ? ' is-invalid' : ''),'placeholder'=>'Date'])}}
                            @foreach($errors->get('sec1_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('client_name', 'Client Name')}}
                            {{Form::text('client_name',$client->company,['class'=>'form-control form-control-sm'. ($errors->has('client_name') ? ' is-invalid' : ''),'placeholder'=>'Client Name'])}}
                            @foreach($errors->get('client_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{--{{Form::label('client_code', 'Client Code')}}--}}
                            {{Form::hidden('client_code',old('client_code'),['class'=>'form-control form-control-sm'. ($errors->has('client_code') ? ' is-invalid' : ''),'placeholder'=>'Client Code'])}}
                            @foreach($errors->get('client_code') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            {{Form::label('director', 'Director')}}
                            {{Form::select('director',["" => "Select","Alan Farrelly" => "Alan Farrelly","Michael Bellew" => "Michael Bellew","Richard Berney" => "Richard Berney","Gareth Evans" => "Gareth Evans","Thomas McDonagh" => "Thomas McDonagh","Darren Connolly"=>"Darren Connolly","Alison Gray"=>"Alison Gray"],old('director'),['class'=>'form-control form-control-sm'. ($errors->has('director') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('director') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <table class="table table-borderless" style="width:700px">
                    <tbody id="managers">
                    <tr>
                        <td width="33.33%">
                            {{Form::label('mandept', 'Managerial Department')}}
                        </td>
                        <td>
                            <label>Manager</label>
                        </td>
                        <td>

                        </td>
                    </tr>
                    @for($i = 0;$i < 1;$i++)

                        <tr>
                            <td width="33.33%">
                                <select name="man[{{$i}}][mandept]" id="manadept{{$i}}" class="form-control form-control-sm" onclick="manadept({{$i}})">
                                    <option value="">None</option>
                                    <option value="Audit/Accounts">Audit/Accounts</option>
                                    <option value="Tax">Tax</option>
                                    <option value="CoSec">Cosec</option>
                                </select>
                            </td>
                            <td>
                                <select name="man[{{$i}}][manager]" class="main{{$i}} form-control form-control-sm">
                                    <option value="">Select</option>
                                    <option value="Martina Gribben">Martina Gribben</option>
                                    <option value="Eric McQuillan">Eric McQuillan</option>
                                    <option value="Michelle Martin">Michelle Martin</option>
                                    <option value="Lauren Campion">Lauren Campion</option>
                                    <option value="Pauline McKevitt">Pauline McKevitt</option>
                                    <option value="Mairead Rooney">Mairead Rooney</option>
                                    <option value="Jane Jackson">Jane Jackson</option>
                                    <option value="Niall Donnelly">Niall Donnelly</option>
                                    <option value="Richard Windrum">Richard Windrum</option>
                                </select>
                                <select name="man[{{$i}}][audit_manager]" class="auditdd{{$i}} form-control form-control-sm" style="display:none;">
                                    <option value="">Select</option>
                                    <option value="Martina Gribben">Martina Gribben</option>
                                    <option value="Eric McQuillan">Eric McQuillan</option>
                                    <option value="Michelle Martin">Michelle Martin</option>
                                    <option value="Lauren Campion">Lauren Campion</option>
                                    <option value="Pauline McKevitt">Pauline McKevitt</option>
                                    <option value="Matthew Whelan">Matthew Whelan</option>
                                    <option value="Tracy Corkey">Tracy Corkey</option>
                                    <option value="Leanne Black">Leanne Black</option>
                                    <option value="Sylwia Krzysztofik">Sylwia Krzysztofik</option>
                                </select>
                                <select name="man[{{$i}}][tax_manager]" class="taxdd{{$i}} form-control form-control-sm" style="display:none;">
                                    <option value="">Select</option>
                                    <option value="Mairead Rooney">Mairead Rooney</option>
                                    <option value="Jane Jackson">Jane Jackson</option>
                                    <option value="Niall Donnelly">Niall Donnelly</option>
                                    <option value="Matthew Whelan">Matthew Whelan</option>
                                    <option value="Fergal Maher">Fergal Maher</option>
                                </select>
                                <select name="man[{{$i}}][cosec_manager]" class="cosecdd{{$i}} form-control form-control-sm" style="display:none;">
                                    <option value="">Select</option>
                                    <option value="Richard Windrum">Richard Windrum</option>
                                </select>
                            </td>
                            <td>

                            </td>
                        </tr>

                    @endfor
                    </tbody>
                </table>
                <a href="javascript:void(0)" class="btn btn-sm btn-primary ml-2" id="addmanager">Add Manager</a><br />
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td colspan="3">
                            {{Form::label('form_completed_by', 'Form Completed By')}}
                            {{Form::text('form_completed_by',old('form_completed_by'),['class'=>'form-control form-control-sm'. ($errors->has('form_completed_by') ? ' is-invalid' : ''),'placeholder'=>'Form Completed By'])}}
                            @foreach($errors->get('form_completed_by') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            {{Form::label('office', 'Office')}}
                            {{Form::select('office',["" => "Select","Dundalk" => "Dundalk","Balbriggan" => "Balbriggan","Dublin" => "Dublin","Belfast" => "Belfast"],$client->office->name,['class'=>'form-control form-control-sm'. ($errors->has('office') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('office') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 2</strong><br />
                <strong>Client Type</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('contact_type', 'Contact Type')}}
                            {{Form::select('contact_type',["" => "Select","Limited" => "Limited","LLP" => "LLP","Nor for Profit Organisation" => "Nor for Profit Organisation","Other Organisation" => "Other Organisation","Other Person" => "Other Person","Partnership" => "Partnership","Trust" => "Trust"],old('contact_type'),['class'=>'form-control form-control-sm'. ($errors->has('contact_type') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('contact_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('business_type', 'Business Type')}}
                            {{Form::select('business_type',["" => "Select","Individual" => "Individual","Limited" => "Limited","LLP" => "LLP","Other" => "Other","Partnership" => "Partnership","Sole Trader" => "Sole Trader","Solicitor" => "Solicitor","Unlimited" => "Unlimited"],old('business_type'),['class'=>'form-control form-control-sm'. ($errors->has('business_type') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('business_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('client_type', 'Client Type')}}
                            {{Form::select('client_type',["" => "Select","Sole Trader" => "Sole Trader","Partnership" => "Partnership","Charity" => "Charity","Individual" => "Individual","Not For Profit Organisation" => "Not For Profit Organisation","Pension Scheme" => "Pension Scheme","Trust" => "Trust","Company Limited by Shares (LTD)" => "Company Limited by Shares (LTD)","Company Limited by Guarantee (CLG)" => "Company Limited by Guarantee (CLG)","Unlimited Company"=>"Unlimited Company","Designated Activity Company (DAC)"=>"Designated Activity Company (DAC)","Public Limited Company (PLC)"=>"Public Limited Company (PLC)","Limited Liability Partnership (LLP)"=>"Limited Liability Partnership (LLP)","Societas Europaea Company (SE)"=>"Societas Europaea Company (SE)","Branch"=>"Branch","Other" => "Other"],old('client_type'),['class'=>'form-control form-control-sm'. ($errors->has('client_type') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('client_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 3</strong><br />
                <strong>General Client Information</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('sec3_country', 'Country')}}
                            {{Form::select('sec3_country',["" => "Select","ROI" => "ROI","NI" => "NI","UK" => "UK","Other" => "Other"],old('sec3_country'),['class'=>'form-control form-control-sm'. ($errors->has('sec3_country') ? ' is-invalid' : ''),'id'=>'sec3_country'])}}
                            @foreach($errors->get('sec3_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::text('sec3_country2',old('sec3_country2'),['class'=>'mt-3 form-control form-control-sm'. ($errors->has('sec3_country2') ? ' is-invalid' : ''),'style'=>'display:none;','placeholder'=>'Country','id'=>'sec3_country2'])}}
                            @foreach($errors->get('form_completed_by') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('sec3_industry', 'Industry')}}
                            {{Form::select('sec3_industry',["" => "Select","Agriculture & Farming"=>"Agriculture & Farming","Auctioneers & Estate Agents"=>"Auctioneers & Estate Agents","Charity"=>"Charity","Construction"=>"Construction","Energy & Natural Resources"=>"Energy & Natural Resources","Financial, Professional & Insurance Services"=>"Financial, Professional & Insurance Services","Fishery & Mariculture"=>"Fishery & Mariculture","Food & Drink Manufacturing"=>"Food & Drink Manufacturing","Hospitality"=>"Hospitality","IT / Software"=>"IT / Software","Legal Services"=>"Legal Services","Manufacturing"=>"Manufacturing","Medical Devices"=>"Medical Devices","Medical Professionals"=>"Medical Professionals","Not for Profit"=>"Not for Profit","Pharma Sector" => "Pharma Sector","Property Investment" => "Property Investment","Property Management Company" => "Property Management Company","Services" => "Services","Transport & Haulage" => "Transport & Haulage","Wholesale & Retail" => "Wholesale & Retail","Holding Company"=>"Holding Company","N/A"=>"N/A","Other" => "Other"],old('sec3_industry'),['class'=>'form-control form-control-sm'. ($errors->has('sec3_industry') ? ' is-invalid' : ''),'id'=>'sec3_industry'])}}
                            @foreach($errors->get('sec3_industry') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::text('sec3_industry2',old('sec3_industry2'),['class'=>'mt-3 form-control form-control-sm'. ($errors->has('sec3_industry2') ? ' is-invalid' : ''),'style'=>'display:none;','id'=>'sec3_industry2','placeholder'=>'Other Industry'])}}
                            @foreach($errors->get('sec3_industry2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td colspan="2">
                            {{Form::label('year_end', 'Year End')}}
                            {{Form::select('year_end',["" => "Select","January" => "January","February"=>"February","March"=>"March","April"=>"April","May"=>"May","June"=>"June","July"=>"July","August"=>"August","September"=>"September","October"=>"October","November"=>"November","December"=>"December","Not Currently Available"=>"Not Currently Available","N/A"=>"N/A"],old('year_end'),['class'=>'form-control form-control-sm'. ($errors->has('sec3_industry') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('year_end') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {{Form::label('provided_services', 'Tick ALL Services To Be Provided')}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::checkbox('provided_services[]',"Accounts Preparation")}}&nbsp;&nbsp;Accounts Preparation<br />
                            {{Form::checkbox('provided_services[]',"Audit")}}&nbsp;&nbsp;Audit<br />
                            {{Form::checkbox('provided_services[]',"Bookkeeping")}}&nbsp;&nbsp;Bookkeeping<br />
                            {{Form::checkbox('provided_services[]',"Business Services")}}&nbsp;&nbsp;Business Services<br />
                            {{Form::checkbox('provided_services[]',"Consultancy")}}&nbsp;&nbsp;Consultancy<br />
                            {{Form::checkbox('provided_services[]',"Corporate Compliance")}}&nbsp;&nbsp;Corporate Compliance<br />
                            {{Form::checkbox('provided_services[]',"Corporate Finance")}}&nbsp;&nbsp;Corporate Finance<br />
                            {{Form::checkbox('provided_services[]',"Corporation Tax")}}&nbsp;&nbsp;Corporation Tax<br />
                            {{Form::checkbox('provided_services[]',"Forensic Accounting")}}&nbsp;&nbsp;Forensic Accounting<br />
                        </td>
                        <td>
                            {{Form::checkbox('provided_services[]',"Insolvency")}}&nbsp;&nbsp;Insolvency<br />
                            {{Form::checkbox('provided_services[]',"Management Accounts")}}&nbsp;&nbsp;Management Accounts<br />
                            {{Form::checkbox('provided_services[]',"Payroll")}}&nbsp;&nbsp;Payroll<br />
                            {{Form::checkbox('provided_services[]',"Personal Tax")}}&nbsp;&nbsp;Personal Tax<br />
                            {{Form::checkbox('provided_services[]',"Tax Consultancy")}}&nbsp;&nbsp;Tax Consultancy<br />
                            {{Form::checkbox('provided_services[]',"VAT Services")}}&nbsp;&nbsp;VAT Services<br />




                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 4</strong><br />
                <strong>Limited Company / Unlimited Company / Limited by Guarantee</strong><br />
                <strong>4.1 Tax &amp; General Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td colspan="2">
                            {{Form::label('company_secretarial_department', 'Is this company to be incorporated by the Company Secretarial Department?')}}
                            {{Form::select('company_secretarial_department',["" => "Select","Yes - CoSec have been informed" => "Yes - CoSec have been informed","No - Already Incorporated" => "No - Already Incorporated"],old('company_secretarial_department'),['class'=>'form-control form-control-sm'. ($errors->has('company_secretarial_department') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('company_secretarial_department') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('company_reg_no', 'Company Registration Number')}}
                            {{Form::text('company_reg_no',old('company_reg_no'),['class'=>'form-control form-control-sm'. ($errors->has('company_reg_no') ? ' is-invalid' : ''),'placeholder'=>'Company Registration Number'])}}
                            @foreach($errors->get('company_reg_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('audit_status', 'Audit Status')}}
                            {{Form::select('audit_status',["" => "Select","Required" => "Required","Exempt" => "Exempt","Not Currently Available"=>"Not Currently Available","N/A"=>"N/A"],old('audit_status'),['class'=>'form-control form-control-sm'. ($errors->has('audit_status') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('audit_status') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('cosec_filing', 'CoSec Filing')}}
                            {{Form::select('cosec_filing',["" => "Select","Yes"=>"Yes","No"=>"No"],old('cosec_filing'),['class'=>'form-control form-control-sm'. ($errors->has('cosec_filing') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cosec_filing') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ct_number', 'CT Number / UTR')}}
                            {{Form::text('ct_number',old('ct_number'),['class'=>'form-control form-control-sm'. ($errors->has('ct_number') ? ' is-invalid' : ''),'placeholder'=>'CT Number / UTR'])}}
                            @foreach($errors->get('ct_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('vat_number', 'VAT Number')}}
                            {{Form::text('vat_number',old('vat_number'),['class'=>'form-control form-control-sm'. ($errors->has('vat_number') ? ' is-invalid' : ''),'placeholder'=>'VAT Number'])}}
                            @foreach($errors->get('vat_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('corp_tax_filing', 'Corporation Tax Filing')}}
                            {{Form::select('corp_tax_filing',["" => "Select","Yes - ROI"=>"Yes - ROI","Yes - NI/UK" =>"Yes - NI/UK","No"=>"No"],old('corp_tax_filing'),['class'=>'form-control form-control-sm'. ($errors->has('corp_tax_filing') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('corp_tax_filing') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('ct_filing_date', 'CT Filing Date')}}
                            {{Form::date('ct_filing_date',old('ct_filing_date'),['class'=>'form-control form-control-sm'. ($errors->has('ct_filing_date') ? ' is-invalid' : ''),'placeholder'=>'CT Filing Date'])}}
                            @foreach($errors->get('vat_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>4.2 Company Contact Details Main / Correspondence / Billing Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('billing_address1', 'Address 1')}}
                            {{Form::text('billing_address1',old('billing_address1'),['class'=>'billing_address1 form-control form-control-sm'. ($errors->has('billing_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('billing_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('billing_address2', 'Address 2')}}
                            {{Form::text('billing_address2',old('billing_address2'),['class'=>'billing_address2 form-control form-control-sm'. ($errors->has('billing_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('billing_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('billing_address1', 'Address 3')}}
                            {{Form::text('billing_address3',old('billing_address3'),['class'=>'billing_address3 form-control form-control-sm'. ($errors->has('billing_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('billing_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('billing_town', 'Town')}}
                            {{Form::text('billing_town',old('billing_town'),['class'=>'billing_town form-control form-control-sm'. ($errors->has('billing_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('billing_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('billing_county', 'County')}}
                            {{Form::text('billing_county',old('billing_county'),['class'=>'billing_county form-control form-control-sm'. ($errors->has('billing_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('billing_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('billing_pcode', 'Post Code')}}
                            {{Form::text('billing_pcode',old('billing_pcode'),['class'=>'billing_pcode form-control form-control-sm'. ($errors->has('billing_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('billing_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('billing_country', 'Country')}}
                            {{Form::text('billing_country',old('billing_country'),['class'=>'billing_country form-control form-control-sm'. ($errors->has('billing_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('billing_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Business Address</strong>&nbsp&nbsp&nbsp{{Form::checkbox('same_as_correspondence',null,null,array('class'=>'same_as_correspondence'))}}&nbsp;Same as correspondence address
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('business_address1', 'Address 1')}}
                            {{Form::text('business_address1',old('business_address1'),['class'=>'business_address1 form-control form-control-sm'. ($errors->has('business_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('business_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('business_address2', 'Address 2')}}
                            {{Form::text('business_address2',old('business_address2'),['class'=>'business_address2 form-control form-control-sm'. ($errors->has('business_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('business_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('business_address3', 'Address 3')}}
                            {{Form::text('business_address3',old('business_address3'),['class'=>'business_address3 form-control form-control-sm'. ($errors->has('business_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('business_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('business_town', 'Town')}}
                            {{Form::text('business_town',old('business_town'),['class'=>'business_town form-control form-control-sm'. ($errors->has('business_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('business_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('business_county', 'County')}}
                            {{Form::text('business_county',old('business_county'),['class'=>'business_county form-control form-control-sm'. ($errors->has('business_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('business_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('business_pcode', 'Post Code')}}
                            {{Form::text('business_pcode',old('business_pcode'),['class'=>'business_pcode form-control form-control-sm'. ($errors->has('business_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('business_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('business_country', 'Country')}}
                            {{Form::text('business_country',old('business_country'),['class'=>'business_country form-control form-control-sm'. ($errors->has('business_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('business_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Registered Address</strong><br />
                {{Form::checkbox('same_as_correspondence2',null,null,array('class'=>'same_as_correspondence2'))}}&nbsp;Same as correspondence address<br />
                {{Form::checkbox('same_as_business',null,null,array('class'=>'same_as_business'))}}&nbsp;Same as business address
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('registered_address1', 'Address 1')}}
                            {{Form::text('registered_address1',old('registered_address1'),['class'=>'registered_address1 form-control form-control-sm'. ($errors->has('registered_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('registered_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('registered_address2', 'Address 2')}}
                            {{Form::text('registered_address2',old('registered_address2'),['class'=>'registered_address2 form-control form-control-sm'. ($errors->has('registered_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('registered_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('registered_address3', 'Address 3')}}
                            {{Form::text('registered_address3',old('registered_address3'),['class'=>'registered_address3 form-control form-control-sm'. ($errors->has('registered_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('registered_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('registered_town', 'Town')}}
                            {{Form::text('registered_town',old('business_town'),['class'=>'registered_town form-control form-control-sm'. ($errors->has('registered_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('registered_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('registered_county', 'County')}}
                            {{Form::text('registered_county',old('registered_county'),['class'=>'registered_county form-control form-control-sm'. ($errors->has('registered_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('registered_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('registered_pcode', 'Post Code')}}
                            {{Form::text('registered_pcode',old('registered_pcode'),['class'=>'registered_pcode form-control form-control-sm'. ($errors->has('registered_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('registered_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('registered_country', 'Country')}}
                            {{Form::text('registered_country',old('registered_country'),['class'=>'registered_country form-control form-control-sm'. ($errors->has('registered_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('registered_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('company_telephone_numbers', 'Company Telephone Numbers')}}
                            {{Form::text('company_telephone_numbers1',old('company_telephone_numbers'),['class'=>'form-control form-control-sm'. ($errors->has('company_telephone_numbers') ? ' is-invalid' : ''),'placeholder'=>'Company Telephone Number'])}}
                            @foreach($errors->get('company_telephone_numbers1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::text('company_telephone_numbers2',old('company_telephone_numbers2'),['class'=>'mt-3 form-control form-control-sm'. ($errors->has('company_telephone_numbers2') ? ' is-invalid' : ''),'placeholder'=>'Company Telephone Number'])}}
                            @foreach($errors->get('company_telephone_numbers2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('company_telephone_numbers_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('company_fax_numbers', 'Company Fax Numbers')}}
                            {{Form::text('company_fax_numbers',old('company_fax_numbers'),['class'=>'form-control form-control-sm'. ($errors->has('company_fax_numbers') ? ' is-invalid' : ''),'placeholder'=>'Company Fax Number'])}}
                            @foreach($errors->get('company_fax_numbers') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('company_fax_numbers_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('company_email_address', 'Company Email Address')}}
                            {{Form::text('company_email_address',old('company_email_address'),['class'=>'form-control form-control-sm'. ($errors->has('company_email_address') ? ' is-invalid' : ''),'placeholder'=>'Company Email Address'])}}
                            @foreach($errors->get('company_email_address') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('company_email_address_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('company_website', 'Company Website')}}
                            {{Form::text('company_website',old('company_website'),['class'=>'form-control form-control-sm'. ($errors->has('company_website') ? ' is-invalid' : ''),'placeholder'=>'Company Website'])}}
                            @foreach($errors->get('company_website') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('company_website_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>4.3 Company Representative Contact Details</strong><br />
                <strong>4.3.1 Company Director Contact Details</strong><br />
                <div id="directors">
                    @for($i = 0;$i < 1;$i++)
                        <div id="directors{{$i+1}}" class="directors">
                            <strong>Director {{$i+1}}</strong>
                            <table class="table table-borderless" style="width:700px">
                                <tr>
                                    <td>
                                        {{Form::label('dir_name', 'Name')}}
                                        {{Form::text('dir['.$i.'][name]',old('dir['.$i.'][name]'),['class'=>'form-control form-control-sm','placeholder'=>'Name'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_position', 'Position')}}
                                        {{Form::text('dir['.$i.'][position]',old('dir['.$i.'][position]'),['class'=>'form-control form-control-sm','placeholder'=>'Position'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_office_number', 'Office Number')}}
                                        {{Form::text('dir['.$i.'][office_number]',old('dir['.$i.'][office_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Office Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('dir['.$i.'][office_number_na]',null,(old('dir['.$i.'][office_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_home_number', 'Home Number')}}
                                        {{Form::text('dir['.$i.'][home_number]',old('dir['.$i.'][home_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Home Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('dir['.$i.'][home_number_na]',null,(old('dir['.$i.'][home_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_mobile_number', 'Mobile Number')}}
                                        {{Form::text('dir['.$i.'][mobile_number]',old('dir['.$i.'][mobile_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Mobile Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('dir['.$i.'][mobile_number_na]',null,(old('dir['.$i.'][mobile_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_email_address', 'E-mail Address')}}
                                        {{Form::text('dir['.$i.'][email_address]',old('dir['.$i.'][email_address]'),['class'=>'form-control form-control-sm','placeholder'=>'E-mail Address'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('dir['.$i.'][email_address_na]',null,(old('dir['.$i.'][email_address_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                            </table>
                            <strong>Residential Address</strong>
                            <table class="table table-borderless" style="width:700px">
                                <tr>
                                    <td>
                                        {{Form::label('dir_address1', 'Address 1')}}
                                        {{Form::text('dir['.$i.'][address1]',old('dir['.$i.'][address1]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 1'])}}
                                    </td>
                                    <td>
                                        {{Form::label('dir_address2', 'Address 2')}}
                                        {{Form::text('dir['.$i.'][address2]',old('dir['.$i.'][address2]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 2'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_address3', 'Address 3')}}
                                        {{Form::text('dir['.$i.'][address3]',old('dir['.$i.'][address3]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 3'])}}
                                    </td>
                                    <td>
                                        {{Form::label('dir_town', 'Town')}}
                                        {{Form::text('dir['.$i.'][town]',old('dir['.$i.'][town]'),['class'=>'form-control form-control-sm','placeholder'=>'Town'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_county', 'County')}}
                                        {{Form::text('dir['.$i.'][county]',old('dir['.$i.'][county]'),['class'=>'form-control form-control-sm','placeholder'=>'County'])}}
                                    </td>
                                    <td>
                                        {{Form::label('dir_pcode', 'Post Code')}}
                                        {{Form::text('dir['.$i.'][pcode]',old('dir['.$i.'][pcode]'),['class'=>'form-control form-control-sm','placeholder'=>'Post Code'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('dir_country', 'Country')}}
                                        {{Form::text('dir['.$i.'][country]',old('dir['.$i.'][country]'),['class'=>'form-control form-control-sm','placeholder'=>'Country'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </div>

                    @endfor
                </div>
                <a href="javascript:void(0)" id="adddirector" onclick="adddirector()" class="btn btn-sm btn-primary">Add director</a><br />

                <strong>4.3.2 Company Shareholder Contact Details</strong><br />
                <div id="shareholders">
                    @for($i = 0;$i < 1;$i++)
                        <div id="shareholders{{$i+1}}" class="shareholders">
                            <strong>Shareholder {{$i+1}}</strong>
                            <table class="table table-borderless" style="width:700px">
                                <tr>
                                    <td>
                                        {{Form::label('sh_name', 'Name')}}
                                        {{Form::text('sh['.$i.'][name]',old('sh['.$i.'][name]'),['class'=>'form-control form-control-sm','placeholder'=>'Name'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_position', 'Position')}}
                                        {{Form::text('sh['.$i.'][position]',old('sh['.$i.'][position]'),['class'=>'form-control form-control-sm','placeholder'=>'Position'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_office_number', 'Office Number')}}
                                        {{Form::text('sh['.$i.'][office_number]',old('sh['.$i.'][office_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Office Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('sh['.$i.'][office_number_na]',null,(old('sh['.$i.'][office_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_home_number', 'Home Number')}}
                                        {{Form::text('sh['.$i.'][home_number]',old('sh['.$i.'][home_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Home Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('sh['.$i.'][home_number_na]',null,(old('sh['.$i.'][home_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_mobile_number', 'Mobile Number')}}
                                        {{Form::text('sh['.$i.'][mobile_number]',old('sh['.$i.'][mobile_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Mobile Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('sh['.$i.'][mobile_number_na]',null,(old('sh['.$i.'][mobile_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_email_address', 'E-mail Address')}}
                                        {{Form::text('sh['.$i.'][email_address]',old('sh['.$i.'][email_address]'),['class'=>'form-control form-control-sm','placeholder'=>'E-mail Address'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('sh['.$i.'][email_address_na]',null,(old('sh['.$i.'][email_address_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                            </table>
                            <strong>Residential Address</strong>
                            <table class="table table-borderless" style="width:700px">
                                <tr>
                                    <td>
                                        {{Form::label('sh_address1', 'Address 1')}}
                                        {{Form::text('sh['.$i.'][address1]',old('sh['.$i.'][address1]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 1'])}}
                                    </td>
                                    <td>
                                        {{Form::label('sh_address2', 'Address 2')}}
                                        {{Form::text('sh['.$i.'][address2]',old('sh['.$i.'][address2]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 2'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_address3', 'Address 3')}}
                                        {{Form::text('sh['.$i.'][address3]',old('sh['.$i.'][address3]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 3'])}}
                                    </td>
                                    <td>
                                        {{Form::label('sh_town', 'Town')}}
                                        {{Form::text('sh['.$i.'][town]',old('sh['.$i.'][town]'),['class'=>'form-control form-control-sm','placeholder'=>'Town'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_county', 'County')}}
                                        {{Form::text('sh['.$i.'][county]',old('sh['.$i.'][county]'),['class'=>'form-control form-control-sm','placeholder'=>'County'])}}
                                    </td>
                                    <td>
                                        {{Form::label('sh_pcode', 'Post Code')}}
                                        {{Form::text('sh['.$i.'][pcode]',old('sh['.$i.'][pcode]'),['class'=>'form-control form-control-sm','placeholder'=>'Post Code'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('sh_country', 'Country')}}
                                        {{Form::text('sh['.$i.'][country]',old('sh['.$i.'][country]'),['class'=>'form-control form-control-sm','placeholder'=>'Country'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </div>

                    @endfor
                </div>
                <a href="javascript:void(0)" id="addshareholder" onclick="addShareholder()" class="btn btn-sm btn-primary">Add Shareholder</a><br />

                <hr />
                <strong>Section 5</strong><br />
                <strong>Sole Trader / Individual</strong><br />
                <br />
                <strong>5.1 Tax Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('st_uk_tax_number', 'PPS / UK Tax Number')}}
                            {{Form::text('st_uk_tax_number',old('st_uk_tax_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_uk_tax_number') ? ' is-invalid' : ''),'placeholder'=>'PPS / UK Tax Number'])}}
                            @foreach($errors->get('st_uk_tax_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_uk_tax_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_ni_number', 'NI Number (UK Only)')}}
                            {{Form::text('st_ni_number',old('st_ni_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_ni_number') ? ' is-invalid' : ''),'placeholder'=>'NI Number (UK Only)'])}}
                            @foreach($errors->get('st_ni_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_ni_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_vat_number', 'VAT Number')}}
                            {{Form::text('st_vat_number',old('st_vat_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_vat_number') ? ' is-invalid' : ''),'placeholder'=>'NI Number (UK Only)'])}}
                            @foreach($errors->get('st_vat_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_vat_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_personal_tax', 'Personal Tax')}}
                            {{Form::select('st_personal_tax',["" => "Select","Yes - ROI"=>"Yes - ROI","Yes - NI/UK" =>"Yes - NI/UK","No"=>"No"],null,['class'=>'form-control form-control-sm'. ($errors->has('st_personal_tax') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('st_personal_tax') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <strong>5.2 Contact Details</strong><br />
                <strong>Personal Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('st_personal_address1', 'Address 1')}}
                            {{Form::text('st_personal_address1',old('st_personal_address1'),['class'=>'st_personal_address1 form-control form-control-sm'. ($errors->has('st_personal_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('st_personal_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_personal_address2', 'Address 2')}}
                            {{Form::text('st_personal_address2',old('st_personal_address2'),['class'=>'st_personal_address2 form-control form-control-sm'. ($errors->has('st_personal_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('st_personal_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_personal_address3', 'Address 3')}}
                            {{Form::text('st_personal_address3',old('st_personal_address3'),['class'=>'st_personal_address3 form-control form-control-sm'. ($errors->has('st_personal_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('st_personal_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_personal_town', 'Town')}}
                            {{Form::text('st_personal_town',old('st_personal_town'),['class'=>'st_personal_town form-control form-control-sm'. ($errors->has('st_personal_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('st_personal_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_personal_county', 'County')}}
                            {{Form::text('st_personal_county',old('st_personal_county'),['class'=>'st_personal_county form-control form-control-sm'. ($errors->has('st_personal_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('st_personal_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_personal_pcode', 'Post Code')}}
                            {{Form::text('st_personal_pcode',old('st_personal_pcode'),['class'=>'st_personal_pcode form-control form-control-sm'. ($errors->has('st_personal_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('st_personal_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_personal_country', 'Country')}}
                            {{Form::text('st_personal_country',old('st_personal_country'),['class'=>'st_personal_country form-control form-control-sm'. ($errors->has('st_personal_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('st_personal_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Main / Correspondence / Billing Address</strong><br />
                {{Form::checkbox('st_same_as_personal',null,null,array('class'=>'st_same_as_personal'))}}&nbsp;&nbsp;Same as Personal Address
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('st_billing_address1', 'Address 1')}}
                            {{Form::text('st_billing_address1',old('st_billing_address1'),['class'=>'st_billing_address1 form-control form-control-sm'. ($errors->has('st_billing_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('st_billing_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_billing_address2', 'Address 2')}}
                            {{Form::text('st_billing_address2',old('st_billing_address2'),['class'=>'st_billing_address2 form-control form-control-sm'. ($errors->has('st_billing_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('st_billing_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_billing_address3', 'Address 3')}}
                            {{Form::text('st_billing_address3',old('st_billing_address3'),['class'=>'st_billing_address3 form-control form-control-sm'. ($errors->has('st_billing_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('st_billing_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_billing_town', 'Town')}}
                            {{Form::text('st_billing_town',old('st_billing_town'),['class'=>'st_billing_town form-control form-control-sm'. ($errors->has('st_billing_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('st_billing_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_billing_county', 'County')}}
                            {{Form::text('st_billing_county',old('st_billing_county'),['class'=>'st_billing_county form-control form-control-sm'. ($errors->has('st_billing_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('st_billing_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_billing_pcode', 'Post Code')}}
                            {{Form::text('st_billing_pcode',old('st_billing_pcode'),['class'=>'st_billing_pcode form-control form-control-sm'. ($errors->has('st_billing_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('st_billing_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_billing_country', 'Country')}}
                            {{Form::text('st_billing_country',old('st_billing_country'),['class'=>'st_billing_country form-control form-control-sm'. ($errors->has('st_billing_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('st_billing_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Business Address</strong><br />
                {{Form::checkbox('st_same_as_personal2',null,null,array('class'=>'st_same_as_personal2'))}}&nbsp;&nbsp;Same as Personal Address<br />
                {{Form::checkbox('st_same_as_correspondence',null,null,array('class'=>'st_same_as_correspondence'))}}&nbsp;&nbsp;Same as Correspondence<br />
                {{Form::checkbox('st_same_as_na',null,null,array('class'=>'st_same_as_na'))}}&nbsp;&nbsp;N/A
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('st_business_address1', 'Address 1')}}
                            {{Form::text('st_business_address1',old('st_business_address1'),['class'=>'st_business_address1 form-control form-control-sm'. ($errors->has('st_business_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('st_business_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_business_address2', 'Address 2')}}
                            {{Form::text('st_business_address2',old('st_business_address2'),['class'=>'st_business_address2 form-control form-control-sm'. ($errors->has('st_business_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('st_business_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_business_address3', 'Address 3')}}
                            {{Form::text('st_business_address3',old('st_business_address3'),['class'=>'st_business_address3 form-control form-control-sm'. ($errors->has('st_business_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('st_business_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_business_town', 'Town')}}
                            {{Form::text('st_business_town',old('st_business_town'),['class'=>'st_business_town form-control form-control-sm'. ($errors->has('st_business_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('st_business_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_business_county', 'County')}}
                            {{Form::text('st_business_county',old('st_business_county'),['class'=>'st_business_county form-control form-control-sm'. ($errors->has('st_business_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('st_business_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('st_business_pcode', 'Post Code')}}
                            {{Form::text('st_business_pcode',old('st_business_pcode'),['class'=>'st_business_pcode form-control form-control-sm'. ($errors->has('st_business_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('st_business_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_business_country', 'Country')}}
                            {{Form::text('st_business_country',old('st_business_country'),['class'=>'st_business_country form-control form-control-sm'. ($errors->has('st_business_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('st_business_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_contact_name', 'Contact Name')}}
                            {{Form::text('st_contact_name',old('st_contact_name'),['class'=>'form-control form-control-sm'. ($errors->has('st_contact_name') ? ' is-invalid' : ''),'placeholder'=>'Contact Name'])}}
                            @foreach($errors->get('st_contact_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_office_number', 'Office Number')}}
                            {{Form::text('st_office_number',old('st_office_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_office_number') ? ' is-invalid' : ''),'placeholder'=>'Office Number'])}}
                            @foreach($errors->get('st_office_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_office_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_home_number', 'Home Number')}}
                            {{Form::text('st_home_number',old('st_home_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_home_number') ? ' is-invalid' : ''),'placeholder'=>'Home Number'])}}
                            @foreach($errors->get('st_home_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_home_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_mobile_number', 'Mobile Number')}}
                            {{Form::text('st_mobile_number',old('st_mobile_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_mobile_number') ? ' is-invalid' : ''),'placeholder'=>'Mobile Number'])}}
                            @foreach($errors->get('st_mobile_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_mobile_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_fax_number', 'Fax Number')}}
                            {{Form::text('st_fax_number',old('st_fax_number'),['class'=>'form-control form-control-sm'. ($errors->has('st_fax_number') ? ' is-invalid' : ''),'placeholder'=>'Fax Number'])}}
                            @foreach($errors->get('st_fax_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_fax_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_email_address1', 'E-mail Address 1')}}
                            {{Form::text('st_email_address1',old('st_email_address1'),['class'=>'form-control form-control-sm'. ($errors->has('st_email_address1') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address 1'])}}
                            @foreach($errors->get('st_email_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_email_address1_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_email_address2', 'E-mail Address 2')}}
                            {{Form::text('st_email_address2',old('st_email_address2'),['class'=>'form-control form-control-sm'. ($errors->has('st_email_address2') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address 2'])}}
                            @foreach($errors->get('st_email_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_email_address2_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('st_website', 'Website')}}
                            {{Form::text('st_website',old('st_website'),['class'=>'form-control form-control-sm'. ($errors->has('st_website') ? ' is-invalid' : ''),'placeholder'=>'Website'])}}
                            @foreach($errors->get('st_website') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('st_website_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 6</strong><br />
                <strong>Partnership / Other Organisation</strong><br />
                <strong>6.1 Tax Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('p_partnership_ref_no', 'Partnership Ref No')}}
                            {{Form::text('p_partnership_ref_no',old('p_partnership_ref_no'),['class'=>'form-control form-control-sm'. ($errors->has('p_partnership_ref_no') ? ' is-invalid' : ''),'placeholder'=>'Partnership Ref No'])}}
                            @foreach($errors->get('p_partnership_ref_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_partnership_ref_no_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_uk_tax_number', 'PPS / UK Tax Number')}}
                            {{Form::text('p_uk_tax_number',old('p_uk_tax_number'),['class'=>'form-control form-control-sm'. ($errors->has('p_uk_tax_number') ? ' is-invalid' : ''),'placeholder'=>'PPS / UK Tax Number'])}}
                            @foreach($errors->get('p_uk_tax_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_uk_tax_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_ni_number', 'NI Number (UK Only)')}}
                            {{Form::text('p_ni_number',old('p_ni_number'),['class'=>'form-control form-control-sm'. ($errors->has('p_ni_number') ? ' is-invalid' : ''),'placeholder'=>'NI Number (UK Only)'])}}
                            @foreach($errors->get('p_ni_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_ni_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_vat_number', 'VAT Number')}}
                            {{Form::text('p_vat_number',old('p_vat_number'),['class'=>'form-control form-control-sm'. ($errors->has('p_vat_number') ? ' is-invalid' : ''),'placeholder'=>'NI Number (UK Only)'])}}
                            @foreach($errors->get('p_vat_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_vat_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_personal_tax', 'Personal Tax')}}
                            {{Form::select('p_personal_tax',["" => "Select","Yes - ROI"=>"Yes - ROI","Yes - NI/UK" =>"Yes - NI/UK","No"=>"No"],null,['class'=>'form-control form-control-sm'. ($errors->has('p_personal_tax') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_personal_tax') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <strong>6.2 Contact Details</strong><br />
                <strong>Main / Correspondence / Billing Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('p_billing_address1', 'Address 1')}}
                            {{Form::text('p_billing_address1',old('p_billing_address1'),['class'=>'p_billing_address1 form-control form-control-sm'. ($errors->has('p_billing_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('p_billing_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_billing_address2', 'Address 2')}}
                            {{Form::text('p_billing_address2',old('p_billing_address2'),['class'=>'p_billing_address2 form-control form-control-sm'. ($errors->has('p_billing_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('p_billing_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_billing_address3', 'Address 3')}}
                            {{Form::text('p_billing_address3',old('p_billing_address3'),['class'=>'p_billing_address3 form-control form-control-sm'. ($errors->has('p_billing_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('p_billing_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_billing_town', 'Town')}}
                            {{Form::text('p_billing_town',old('p_billing_town'),['class'=>'p_billing_town form-control form-control-sm'. ($errors->has('p_billing_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('p_billing_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_billing_county', 'County')}}
                            {{Form::text('p_billing_county',old('p_billing_county'),['class'=>'p_billing_county form-control form-control-sm'. ($errors->has('p_billing_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('p_billing_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_billing_pcode', 'Post Code')}}
                            {{Form::text('p_billing_pcode',old('p_billing_pcode'),['class'=>'p_billing_pcode form-control form-control-sm'. ($errors->has('p_billing_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('p_billing_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_billing_country', 'Country')}}
                            {{Form::text('p_billing_country',old('p_billing_country'),['class'=>'p_billing_country form-control form-control-sm'. ($errors->has('p_billing_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('p_billing_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Business Address</strong><br />
                {{Form::checkbox('p_same_as_correspondence',null,null,array('class'=>'p_same_as_correspondence'))}}&nbsp;&nbsp;Same as Correspondence
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('p_business_address1', 'Address 1')}}
                            {{Form::text('p_business_address1',old('p_business_address1'),['class'=>'p_business_address1 form-control form-control-sm'. ($errors->has('p_business_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('p_business_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_business_address2', 'Address 2')}}
                            {{Form::text('p_business_address2',old('p_business_address2'),['class'=>'p_business_address2 form-control form-control-sm'. ($errors->has('p_business_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('p_business_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_business_address3', 'Address 3')}}
                            {{Form::text('p_business_address3',old('p_business_address3'),['class'=>'p_business_address3 form-control form-control-sm'. ($errors->has('p_business_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('p_business_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_business_town', 'Town')}}
                            {{Form::text('p_business_town',old('p_business_town'),['class'=>'p_business_town form-control form-control-sm'. ($errors->has('p_business_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('p_business_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_business_county', 'County')}}
                            {{Form::text('p_business_county',old('p_business_county'),['class'=>'p_business_county form-control form-control-sm'. ($errors->has('p_business_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('p_business_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_business_pcode', 'Post Code')}}
                            {{Form::text('p_business_pcode',old('p_business_pcode'),['class'=>'p_business_pcode form-control form-control-sm'. ($errors->has('p_business_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('p_business_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_business_country', 'Country')}}
                            {{Form::text('p_business_country',old('p_business_country'),['class'=>'p_business_country form-control form-control-sm'. ($errors->has('p_business_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('p_business_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_office_number', 'Office Number')}}
                            {{Form::text('p_office_number',old('p_office_number'),['class'=>'form-control form-control-sm'. ($errors->has('p_office_number') ? ' is-invalid' : ''),'placeholder'=>'Office Number'])}}
                            @foreach($errors->get('p_office_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_office_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_fax_number', 'Fax Number')}}
                            {{Form::text('p_fax_number',old('p_fax_number'),['class'=>'form-control form-control-sm'. ($errors->has('p_fax_number') ? ' is-invalid' : ''),'placeholder'=>'Fax Number'])}}
                            @foreach($errors->get('p_fax_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_fax_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_email_address1', 'E-mail Address 1')}}
                            {{Form::text('p_email_address1',old('p_email_address1'),['class'=>'form-control form-control-sm'. ($errors->has('p_email_address1') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address 1'])}}
                            @foreach($errors->get('p_email_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_email_address1_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_email_address2', 'E-mail Address 2')}}
                            {{Form::text('p_email_address2',old('p_email_address2'),['class'=>'form-control form-control-sm'. ($errors->has('p_email_address2') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address 2'])}}
                            @foreach($errors->get('p_email_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_email_address2_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_website', 'Website')}}
                            {{Form::text('p_website',old('p_website'),['class'=>'form-control form-control-sm'. ($errors->has('p_website') ? ' is-invalid' : ''),'placeholder'=>'Website'])}}
                            @foreach($errors->get('p_website') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('p_website',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>6.3 Partners / Members Contact Details</strong><br />
                <div id="partners">
                    @for($i = 0;$i < 1;$i++)
                        <div id="partners{{$i+1}}" class="partners">
                            <strong>Partner {{$i+1}}</strong>
                            <table class="table table-borderless" style="width:700px">
                                <tr>
                                    <td>
                                        {{Form::label('part_name', 'Contact Name')}}
                                        {{Form::text('part['.$i.'][name]',old('part['.$i.'][name]'),['class'=>'form-control form-control-sm','placeholder'=>'Name'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_address1', 'Address 1')}}
                                        {{Form::text('part['.$i.'][address1]',old('part['.$i.'][address1]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 1'])}}
                                    </td>
                                    <td>
                                        {{Form::label('part_address2', 'Address 2')}}
                                        {{Form::text('part['.$i.'][address2]',old('part['.$i.'][address2]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 2'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_address3', 'Address 3')}}
                                        {{Form::text('part['.$i.'][address3]',old('part['.$i.'][address3]'),['class'=>'form-control form-control-sm','placeholder'=>'Address 3'])}}
                                    </td>
                                    <td>
                                        {{Form::label('part_town', 'Town')}}
                                        {{Form::text('part['.$i.'][town]',old('part['.$i.'][town]'),['class'=>'form-control form-control-sm','placeholder'=>'Town'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_county', 'County')}}
                                        {{Form::text('part['.$i.'][county]',old('part['.$i.'][county]'),['class'=>'form-control form-control-sm','placeholder'=>'County'])}}
                                    </td>
                                    <td>
                                        {{Form::label('part_pcode', 'Post Code')}}
                                        {{Form::text('part['.$i.'][pcode]',old('part['.$i.'][pcode]'),['class'=>'form-control form-control-sm','placeholder'=>'Post Code'])}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_country', 'Country')}}
                                        {{Form::text('part['.$i.'][country]',old('part['.$i.'][country]'),['class'=>'form-control form-control-sm','placeholder'=>'Country'])}}
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_home_number', 'Home Number')}}
                                        {{Form::text('part['.$i.'][home_number]',old('part['.$i.'][home_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Home Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('part['.$i.'][home_number_na]',null,(old('part['.$i.'][home_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_mobile_number', 'Mobile Number')}}
                                        {{Form::text('part['.$i.'][mobile_number]',old('part['.$i.'][mobile_number]'),['class'=>'form-control form-control-sm','placeholder'=>'Mobile Number'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('part['.$i.'][mobile_number_na]',null,(old('part['.$i.'][mobile_number_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_email_address', 'E-mail Address 1')}}
                                        {{Form::text('part['.$i.'][email_address1]',old('part['.$i.'][email_address1]'),['class'=>'form-control form-control-sm','placeholder'=>'E-mail Address'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('part['.$i.'][email_address1_na]',null,(old('part['.$i.'][email_address1_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {{Form::label('part_email_address', 'E-mail Address 2')}}
                                        {{Form::text('part['.$i.'][email_address2]',old('part['.$i.'][email_address2]'),['class'=>'form-control form-control-sm','placeholder'=>'E-mail Address'])}}
                                    </td>
                                    <td style="vertical-align: bottom;margin-bottom:4px;">
                                        N/A&nbsp;&nbsp;{{Form::checkbox('part['.$i.'][email_address2_na]',null,(old('part['.$i.'][email_address2_na]') == "on" ? true : false))}}
                                    </td>
                                </tr>
                            </table>
                        </div>

                    @endfor
                </div>
                <a href="javascript:void(0)" id="addpartner" onclick="addpartner()" class="btn btn-sm btn-primary">Add partner</a><br />
                <hr />
                <strong>Section 7</strong><br />
                <strong>Pension Schemes &amp; Trusts</strong><br />
                <strong>7.1 Tax Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('pen_ref_no', 'Pension Scheme Ref No')}}
                            {{Form::text('pen_ref_no',old('pen_ref_no'),['class'=>'form-control form-control-sm'. ($errors->has('pen_ref_no') ? ' is-invalid' : ''),'placeholder'=>'Pension Scheme Ref No'])}}
                            @foreach($errors->get('pen_ref_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_ref_no_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_tax_no', 'Tax Ref No')}}
                            {{Form::text('pen_tax_no',old('pen_tax_no'),['class'=>'form-control form-control-sm'. ($errors->has('pen_tax_no') ? ' is-invalid' : ''),'placeholder'=>'Tax Ref No'])}}
                            @foreach($errors->get('pen_tax_no') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_tax_no_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_tax_filing', 'Tax Filing')}}
                            {{Form::select('pen_tax_filing',["" => "Select","Yes - ROI"=>"Yes - ROI","Yes - NI/UK" =>"Yes - NI/UK","No"=>"No"],old('pen_tax_filing'),['class'=>'form-control form-control-sm'. ($errors->has('pen_tax_filing') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('pen_tax_filing') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_tax_filing_date', 'Tax Filing Date')}}
                            {{Form::date('pen_tax_filing_date',old('pen_tax_filing_date'),['class'=>'form-control form-control-sm'. ($errors->has('pen_tax_filing_date') ? ' is-invalid' : ''),'placeholder'=>'Tax Filing Date'])}}
                            @foreach($errors->get('pen_tax_filing_date') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <strong>7.2 Contact Details</strong><br />
                <strong>Main / Correspondence / Billing Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('pen_billing_address1', 'Address 1')}}
                            {{Form::text('pen_billing_address1',old('pen_billing_address1'),['class'=>'pen_billing_address1 form-control form-control-sm'. ($errors->has('pen_billing_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('pen_billing_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_billing_address2', 'Address 2')}}
                            {{Form::text('pen_billing_address2',old('pen_billing_address2'),['class'=>'pen_billing_address2 form-control form-control-sm'. ($errors->has('pen_billing_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('pen_billing_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_address3', 'Address 3')}}
                            {{Form::text('pen_billing_address3',old('pen_billing_address3'),['class'=>'pen_billing_address3 form-control form-control-sm'. ($errors->has('pen_billing_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('pen_billing_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_billing_town', 'Town')}}
                            {{Form::text('pen_billing_town',old('pen_billing_town'),['class'=>'pen_billing_town form-control form-control-sm'. ($errors->has('pen_billing_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('pen_billing_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_county', 'County')}}
                            {{Form::text('pen_billing_county',old('pen_billing_county'),['class'=>'pen_billing_county form-control form-control-sm'. ($errors->has('pen_billing_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('pen_billing_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_billing_pcode', 'Post Code')}}
                            {{Form::text('pen_billing_pcode',old('pen_billing_pcode'),['class'=>'pen_billing_pcode form-control form-control-sm'. ($errors->has('pen_billing_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('pen_billing_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_country', 'Country')}}
                            {{Form::text('pen_billing_country',old('pen_billing_country'),['class'=>'pen_billing_country form-control form-control-sm'. ($errors->has('pen_billing_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('pen_billing_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_contact_name', 'Contact Name')}}
                            {{Form::text('pen_billing_contact_name',old('pen_billing_contact_name'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_contact_name') ? ' is-invalid' : ''),'placeholder'=>'Contact Name'])}}
                            @foreach($errors->get('pen_billing_contact_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_position', 'Position')}}
                            {{Form::text('pen_billing_position',old('pen_billing_position'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_position') ? ' is-invalid' : ''),'placeholder'=>'Position'])}}
                            @foreach($errors->get('pen_billing_position') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_office_number', 'Office Number')}}
                            {{Form::text('pen_billing_office_number',old('pen_billing_office_number'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_office_number') ? ' is-invalid' : ''),'placeholder'=>'Office Number'])}}
                            @foreach($errors->get('pen_billing_office_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_billing_office_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_home_number', 'Home Number')}}
                            {{Form::text('pen_billing_home_number',old('pen_billing_home_number'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_home_number') ? ' is-invalid' : ''),'placeholder'=>'Home Number'])}}
                            @foreach($errors->get('pen_billing_home_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_billing_home_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_mobile_number', 'Mobile Number')}}
                            {{Form::text('pen_billing_mobile_number',old('pen_billing_mobile_number'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_mobile_number') ? ' is-invalid' : ''),'placeholder'=>'Mobile Number'])}}
                            @foreach($errors->get('pen_billing_mobile_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_billing_mobile_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_fax_number', 'Fax Number')}}
                            {{Form::text('pen_billing_fax_number',old('pen_billing_fax_number'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_fax_number') ? ' is-invalid' : ''),'placeholder'=>'Fax Number'])}}
                            @foreach($errors->get('pen_billing_fax_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_billing_fax_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_billing_email_address', 'E-mail Address')}}
                            {{Form::text('pen_billing_email_address',old('pen_billing_email_address'),['class'=>'form-control form-control-sm'. ($errors->has('pen_billing_email_address') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address'])}}
                            @foreach($errors->get('pen_billing_email_address') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('pen_billing_email_address_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>Residential Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('pen_res_address1', 'Address 1')}}
                            {{Form::text('pen_res_address1',old('pen_res_address1'),['class'=>'pen_res_address1 form-control form-control-sm'. ($errors->has('pen_res_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('pen_res_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_res_address2', 'Address 2')}}
                            {{Form::text('pen_res_address2',old('pen_res_address2'),['class'=>'pen_res_address2 form-control form-control-sm'. ($errors->has('pen_res_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('pen_res_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_res_address3', 'Address 3')}}
                            {{Form::text('pen_res_address3',old('pen_res_address3'),['class'=>'pen_res_address3 form-control form-control-sm'. ($errors->has('pen_res_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('pen_res_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_res_town', 'Town')}}
                            {{Form::text('pen_res_town',old('pen_res_town'),['class'=>'pen_res_town form-control form-control-sm'. ($errors->has('pen_res_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('pen_res_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_res_county', 'County')}}
                            {{Form::text('pen_res_county',old('pen_res_county'),['class'=>'pen_res_county form-control form-control-sm'. ($errors->has('pen_res_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('pen_res_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('pen_res_pcode', 'Post Code')}}
                            {{Form::text('pen_res_pcode',old('pen_res_pcode'),['class'=>'pen_res_pcode form-control form-control-sm'. ($errors->has('pen_res_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('pen_res_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pen_res_country', 'Country')}}
                            {{Form::text('pen_res_country',old('pen_res_country'),['class'=>'pen_res_country form-control form-control-sm'. ($errors->has('pen_res_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('pen_res_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 8</strong><br />
                <strong>Liquidation / Insolvency</strong><br />
                <strong>8.1 Company Details</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('liq_company_registration_number', 'Company Registration Number')}}
                            {{Form::text('liq_company_registration_number',old('liq_company_registration_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_company_registration_number') ? ' is-invalid' : ''),'placeholder'=>'Company Registration Number'])}}
                            @foreach($errors->get('liq_company_registration_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_tax_ref', 'Tax Ref No')}}
                            {{Form::text('liq_tax_ref',old('liq_tax_ref'),['class'=>'form-control form-control-sm'. ($errors->has('liq_tax_ref') ? ' is-invalid' : ''),'placeholder'=>'Tax Ref No'])}}
                            @foreach($errors->get('liq_tax_ref') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_tax_ref_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>8.2 Company Representative Contact Details</strong><br />
                <strong>Director 1</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('liq_d1_name', 'Name')}}
                            {{Form::text('liq_d1_name',old('liq_d1_name'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                            @foreach($errors->get('liq_d1_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_position', 'Position')}}
                            {{Form::text('liq_d1_position',old('liq_d1_position'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_position') ? ' is-invalid' : ''),'placeholder'=>'Position'])}}
                            @foreach($errors->get('liq_d1_position') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_office_number', 'Office Number')}}
                            {{Form::text('liq_d1_office_number',old('liq_d1_office_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_office_number') ? ' is-invalid' : ''),'placeholder'=>'Office Number'])}}
                            @foreach($errors->get('liq_d1_office_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d1_office_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_home_number', 'Home Number')}}
                            {{Form::text('liq_d1_home_number',old('liq_d1_home_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_home_number') ? ' is-invalid' : ''),'placeholder'=>'Home Number'])}}
                            @foreach($errors->get('liq_d1_home_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d1_home_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_mobile_number', 'Mobile Number')}}
                            {{Form::text('liq_d1_mobile_number',old('liq_d1_mobile_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_mobile_number') ? ' is-invalid' : ''),'placeholder'=>'Mobile Number'])}}
                            @foreach($errors->get('liq_d1_mobile_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d1_mobile_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_email_address', 'E-mail Address')}}
                            {{Form::text('liq_d1_email_address',old('liq_d1_email_address'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d1_email_address') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address'])}}
                            @foreach($errors->get('liq_d1_email_address') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d1_email_address_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>Residential Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('liq_d1_address1', 'Address 1')}}
                            {{Form::text('liq_d1_address1',old('liq_d1_address1'),['class'=>'liq_d1_address1 form-control form-control-sm'. ($errors->has('liq_d1_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('liq_d1_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d1_address2', 'Address 2')}}
                            {{Form::text('liq_d1_address2',old('liq_d1_address2'),['class'=>'liq_d1_address2 form-control form-control-sm'. ($errors->has('liq_d1_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('liq_d1_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_address3', 'Address 3')}}
                            {{Form::text('liq_d1_address3',old('liq_d1_address3'),['class'=>'liq_d1_address3 form-control form-control-sm'. ($errors->has('liq_d1_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('liq_d1_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d1_town', 'Town')}}
                            {{Form::text('liq_d1_town',old('liq_d1_town'),['class'=>'liq_d1_town form-control form-control-sm'. ($errors->has('liq_d1_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('liq_d1_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_county', 'County')}}
                            {{Form::text('liq_d1_county',old('liq_d1_county'),['class'=>'liq_d1_county form-control form-control-sm'. ($errors->has('liq_d1_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('liq_d1_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d1_pcode', 'Post Code')}}
                            {{Form::text('liq_d1_pcode',old('liq_d1_pcode'),['class'=>'liq_d1_pcode form-control form-control-sm'. ($errors->has('liq_d1_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('liq_d1_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d1_country', 'Country')}}
                            {{Form::text('liq_d1_country',old('liq_d1_country'),['class'=>'liq_d1_country form-control form-control-sm'. ($errors->has('liq_d1_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('liq_d1_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>Director 2</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('liq_d2_name', 'Name')}}
                            {{Form::text('liq_d2_name',old('liq_d2_name'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
                            @foreach($errors->get('liq_d2_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_position', 'Position')}}
                            {{Form::text('liq_d2_position',old('liq_d2_position'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_position') ? ' is-invalid' : ''),'placeholder'=>'Position'])}}
                            @foreach($errors->get('liq_d2_position') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_office_number', 'Office Number')}}
                            {{Form::text('liq_d2_office_number',old('liq_d2_office_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_office_number') ? ' is-invalid' : ''),'placeholder'=>'Office Number'])}}
                            @foreach($errors->get('liq_d2_office_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d2_office_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_home_number', 'Home Number')}}
                            {{Form::text('liq_d2_home_number',old('liq_d2_home_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_home_number') ? ' is-invalid' : ''),'placeholder'=>'Home Number'])}}
                            @foreach($errors->get('liq_d2_home_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d2_home_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_mobile_number', 'Mobile Number')}}
                            {{Form::text('liq_d2_mobile_number',old('liq_d2_mobile_number'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_mobile_number') ? ' is-invalid' : ''),'placeholder'=>'Mobile Number'])}}
                            @foreach($errors->get('liq_d2_mobile_number') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d2_mobile_number_na',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_email_address', 'E-mail Address')}}
                            {{Form::text('liq_d2_email_address',old('liq_d2_email_address'),['class'=>'form-control form-control-sm'. ($errors->has('liq_d2_email_address') ? ' is-invalid' : ''),'placeholder'=>'E-mail Address'])}}
                            @foreach($errors->get('liq_d2_email_address') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td style="vertical-align: bottom;margin-bottom:4px;">
                            N/A&nbsp;&nbsp;{{Form::checkbox('liq_d2_email_address_na',null,null)}}
                        </td>
                    </tr>
                </table>
                <strong>Residential Address</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('liq_d2_address1', 'Address 1')}}
                            {{Form::text('liq_d2_address1',old('liq_d2_address1'),['class'=>'liq_d2_address1 form-control form-control-sm'. ($errors->has('liq_d2_address1') ? ' is-invalid' : ''),'placeholder'=>'Address 1'])}}
                            @foreach($errors->get('liq_d2_address1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d2_address2', 'Address 2')}}
                            {{Form::text('liq_d2_address2',old('liq_d2_address2'),['class'=>'liq_d2_address2 form-control form-control-sm'. ($errors->has('liq_d2_address2') ? ' is-invalid' : ''),'placeholder'=>'Address 2'])}}
                            @foreach($errors->get('liq_d2_address2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_address3', 'Address 3')}}
                            {{Form::text('liq_d2_address3',old('liq_d2_address3'),['class'=>'liq_d2_address3 form-control form-control-sm'. ($errors->has('liq_d2_address3') ? ' is-invalid' : ''),'placeholder'=>'Address 3'])}}
                            @foreach($errors->get('liq_d2_address3') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d2_town', 'Town')}}
                            {{Form::text('liq_d2_town',old('liq_d2_town'),['class'=>'liq_d2_town form-control form-control-sm'. ($errors->has('liq_d2_town') ? ' is-invalid' : ''),'placeholder'=>'Town'])}}
                            @foreach($errors->get('liq_d2_town') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_county', 'County')}}
                            {{Form::text('liq_d2_county',old('liq_d2_county'),['class'=>'liq_d2_county form-control form-control-sm'. ($errors->has('liq_d2_county') ? ' is-invalid' : ''),'placeholder'=>'County'])}}
                            @foreach($errors->get('liq_d2_county') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('liq_d2_pcode', 'Post Code')}}
                            {{Form::text('liq_d2_pcode',old('liq_d2_pcode'),['class'=>'liq_d2_pcode form-control form-control-sm'. ($errors->has('liq_d2_pcode') ? ' is-invalid' : ''),'placeholder'=>'Post Code'])}}
                            @foreach($errors->get('liq_d2_pcode') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('liq_d2_country', 'Country')}}
                            {{Form::text('liq_d2_country',old('liq_d2_country'),['class'=>'liq_d2_country form-control form-control-sm'. ($errors->has('liq_d2_country') ? ' is-invalid' : ''),'placeholder'=>'Country'])}}
                            @foreach($errors->get('liq_d2_country') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 9</strong><br />
                <strong>Extra Info for Database</strong><br />
                <strong>9.1 Marketing</strong><br />
                <br />
                Publications<br />
                <br />
                {{Form::checkbox('x_tailored_emails','Yes',null)}}&nbsp;&nbsp;E-zines &amp; Tailored emails<br />
                {{Form::checkbox('x_uk_mailings','Yes',null)}}&nbsp;&nbsp;NI / UK Specialised Mailings<br />
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('x_how_did_you_hear', 'How did you hear about us?')}}
                            {{Form::select('x_how_did_you_hear',["" => "Select","Referral"=>"Referral","Existing Client" =>"Existing Client","Golden Pages"=>"Golden Pages","Internet Search"=>"Internet Search","Staff Family or Friend"=>"Staff Family or Friend","Targeted Business"=>"Targeted Business","Other"=>"Other"],old('x_how_did_you_hear'),['class'=>'form-control form-control-sm'. ($errors->has('x_how_did_you_hear') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('x_how_did_you_hear') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                            {{Form::text('x_how_did_you_hear2',old('x_how_did_you_hear2'),['class'=>'mt-3 form-control form-control-sm'. ($errors->has('x_how_did_you_hear2') ? ' is-invalid' : ''),'placeholder'=>'Other'])}}
                            @foreach($errors->get('x_how_did_you_hear2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('x_referrer_name', 'Referrer Name')}}
                            {{Form::text('x_referrer_name',old('x_referrer_name'),['class'=>'form-control form-control-sm'. ($errors->has('x_referrer_name') ? ' is-invalid' : ''),'placeholder'=>'Referrer Name'])}}
                            @foreach($errors->get('x_how_did_you_hear') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('x_existing_account_name', 'Existing Account Name')}}
                            {{Form::text('x_existing_account_name',old('x_existing_account_name'),['class'=>'form-control form-control-sm'. ($errors->has('x_existing_account_name') ? ' is-invalid' : ''),'placeholder'=>'Existing Account Name'])}}
                            @foreach($errors->get('x_existing_account_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('x_staff_member_name', 'Staff Member Name')}}
                            {{Form::text('x_staff_member_name',old('x_staff_member_name'),['class'=>'form-control form-control-sm'. ($errors->has('x_staff_member_name') ? ' is-invalid' : ''),'placeholder'=>'Existing Staff Member Name'])}}
                            @foreach($errors->get('x_staff_member_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>

                        </td>
                    </tr>
                </table>
                <strong>9.2 Associated Contacts</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('x_relationship_type1', 'Relationship Type')}}
                            {{Form::text('x_relationship_type1',old('x_relationship_type1'),['class'=>'form-control form-control-sm'. ($errors->has('x_relationship_type1') ? ' is-invalid' : ''),'placeholder'=>'Relationship Type 1'])}}
                            @foreach($errors->get('x_relationship_type1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('x_client_name1', 'Client Name')}}
                            {{Form::text('x_client_name1',old('x_client_name1'),['class'=>'form-control form-control-sm'. ($errors->has('x_client_name1') ? ' is-invalid' : ''),'placeholder'=>'Client Name 1'])}}
                            @foreach($errors->get('x_client_name1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('x_client_code1', 'Client Code')}}
                            {{Form::text('x_client_code1',old('x_client_code1'),['class'=>'form-control form-control-sm'. ($errors->has('x_client_code1') ? ' is-invalid' : ''),'placeholder'=>'Client Code 1'])}}
                            @foreach($errors->get('x_client_code1') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::text('x_relationship_type2',old('x_relationship_type2'),['class'=>'form-control form-control-sm'. ($errors->has('x_relationship_type2') ? ' is-invalid' : ''),'placeholder'=>'Relationship Type 2'])}}
                            @foreach($errors->get('x_relationship_type2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('x_client_name2',old('x_client_name2'),['class'=>'form-control form-control-sm'. ($errors->has('x_client_name2') ? ' is-invalid' : ''),'placeholder'=>'Client Name 2'])}}
                            @foreach($errors->get('x_client_name2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::text('x_client_code2',old('x_client_code2'),['class'=>'form-control form-control-sm'. ($errors->has('x_client_code2') ? ' is-invalid' : ''),'placeholder'=>'Client Code 2'])}}
                            @foreach($errors->get('x_client_code2') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <strong>9.3 Notes</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{ Form::textarea('x_notes', old('x_notes'), ['class'=>'form-control form-control-sm','rows' => 10, 'cols' => 100]) }}
                        </td>
                    </tr>
                </table>
                <strong>9.4 Payments</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('p_credit_limit', 'Credit Limit')}}
                            {{Form::text('p_credit_limit',old('p_credit_limit'),['class'=>'form-control form-control-sm'. ($errors->has('p_credit_limit') ? ' is-invalid' : ''),'placeholder'=>'Relationship Type 1'])}}
                            @foreach($errors->get('p_credit_limit') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_tax_type', 'Tax Type')}}
                            {{Form::select('p_tax_type',["" => "Select","Standard"=>"Standard","Exempt" =>"Exempt","Outside Scope"=>"Outside Scope","Discounted (13.5%)"=>"Discounted (13.5%)","GBP"=>"GBP"],old('p_tax_type'),['class'=>'form-control form-control-sm'. ($errors->has('p_tax_type') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_tax_type') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::label('p_currency', 'Currency')}}
                            {{Form::select('p_currency',["" => "Select","Euro"=>"Euro (&euro;)","GBP" =>"GBP (&pound;)"],old('p_currency'),['class'=>'form-control form-control-sm'. ($errors->has('p_currency') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_currency') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Invoices &amp; Statements to be sent by email only&nbsp;&nbsp;{{Form::checkbox('p_invoice_email_only',null,null)}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_email', 'Send to: Email')}}
                            {{Form::text('p_email',old('p_email'),['class'=>'form-control form-control-sm'. ($errors->has('p_email') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_client_value', 'Fee Quote Issued')}}
                            &nbsp;&nbsp;
                            {{Form::radio('p_fee_quote','Yes',array('class' => 'form-control', 'placeholder'=>'Yes'))}}
                            {{Form::label('p_fee_quote', 'Yes')}}
                            &nbsp;&nbsp;
                            {{Form::radio('p_fee_quote','No',array('class' => 'form-control', 'placeholder'=>'No'))}}
                            {{Form::label('p_fee_quote', 'No')}}
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('p_client_value', 'Client Value')}}
                            {{Form::text('p_client_value',old('p_client_value'),['class'=>'form-control form-control-sm'. ($errors->has('p_client_value') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_client_value') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <strong>9.5 Client Rating</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::select('p_client_rating',["" => "Select","A"=>"A","B" =>"B","C"=>"C","D"=>"D","E"=>"E"],old('p_client_rating'),['class'=>'form-control form-control-sm'. ($errors->has('p_client_rating') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('p_client_rating') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 10</strong><br />
                <strong>AML / Customer Due Diligence</strong><br />
                <strong>10.1 General Information</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('cdd_source', 'Source of Introduction')}}
                            {{Form::text('cdd_source',old('cdd_source'),['class'=>'form-control form-control-sm'. ($errors->has('cdd_source') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cdd_source') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cdd_sort_of_business', 'What sort of business do we expect to undertake for the client?')}}
                            {{Form::text('cdd_sort_of_business',old('cdd_sort_of_business'),['class'=>'form-control form-control-sm'. ($errors->has('cdd_sort_of_business') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cdd_sort_of_business') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cdd_date_and_place', 'Date and place where prospective client was met in person')}}
                            {{Form::text('cdd_date_and_place',old('cdd_date_and_place'),['class'=>'form-control form-control-sm'. ($errors->has('cdd_date_and_place') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cdd_date_and_place') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                            {{Form::checkbox('cdd_date_and_place_na','Yes',null)}}&nbsp;&nbsp;N/A (Existing Client)<br />
                        </td>
                    </tr>
                </table>
                <strong>10.2 Corporate Information</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('cr_company_search', 'Company Search carried out, results reviewed and documents saved to CCH?')}}
                            {{Form::select('cr_company_search',["" => "Select","Yes"=>"Yes","No" =>"No","N/A as the company is a new incorporation"=>"N/A as the company is a new incorporation"],old('cr_company_search'),['class'=>'form-control form-control-sm'. ($errors->has('cr_company_search') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_company_search') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cr_copy_of_certificate', 'Copy of Certificate of Incorporation and Memorandum and Articles of Association obtained and saved to CCH?')}}
                            {{Form::select('cr_copy_of_certificate',["" => "Select","Yes"=>"Yes","No" =>"No","Information not currently available, will provide ASAP"=>"Information not currently available, will provide ASAP"],old('cr_copy_of_certificate'),['class'=>'form-control form-control-sm'. ($errors->has('cr_copy_of_certificate') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_copy_of_certificate') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cr_list_of_directors', 'List of Directors including names and addresses and dates of birth saved to CCH?`')}}
                            {{Form::select('cr_list_of_directors',["" => "Select","Yes"=>"Yes","No" =>"No"],old('cr_list_of_directors'),['class'=>'form-control form-control-sm'. ($errors->has('cr_list_of_directors') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_list_of_directors') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cr_list_of_names', 'List of names, addresses, occupations and dates of birth for shareholders with a shareholding of or in excess of 25% saved to CCH? (If corporate shareholders it is necessary to look behind this again to determine the individuals who have ultimate ownership / control)')}}
                            {{Form::select('cr_list_of_names',["" => "Select","Yes"=>"Yes","No" =>"No"],old('cr_list_of_names'),['class'=>'form-control form-control-sm'. ($errors->has('cr_list_of_names') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_list_of_names') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cr_photo_id', 'Photo ID and proof of address for Directors, Shareholders with shares of 25% or greater and all persons authorised to represent the company saved to CCH')}}
                            {{Form::select('cr_photo_id',["" => "Select","Yes"=>"Yes","No" =>"No"],old('cr_photo_id'),['class'=>'form-control form-control-sm'. ($errors->has('cr_photo_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_photo_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('cr_due_diligence', 'Are enhanced due diligence procedures required to be completed?')}}
                            {{Form::select('cr_due_diligence',["" => "Select","Yes"=>"Yes","No" =>"No"],old('cr_due_diligence'),['class'=>'form-control form-control-sm'. ($errors->has('cr_due_diligence') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('cr_due_diligence') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <strong>10.3 Individual Requirements</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td colspan="2">
                            {{Form::label("ir_satisfied", "Are we satisfied that in general terms we understand the source of the client's funds / wealth?")}}
                            {{Form::select('ir_satisfied',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ir_satisfied'),['class'=>'form-control form-control-sm'. ($errors->has('ir_satisfied') ? ' is-invalid' : ''),'id'=>'ir_satisfied'])}}
                            @foreach($errors->get('ir_satisfied') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr id="ir_y_what_basis" style="display: none;">
                        <td>
                            {{Form::label('ir_y_what_basis', 'If yes, on what basis?')}}
                            {{Form::text('ir_y_what_basis',old('ir_y_what_basis'),['class'=>'form-control form-control-sm'. ($errors->has('ir_y_what_basis') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ir_y_what_basis') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {{Form::label('ir_photo_id', 'Photo ID and proof of address for the client saved to CCH?')}}
                            {{Form::select('ir_photo_id',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ir_photo_id'),['class'=>'form-control form-control-sm'. ($errors->has('ir_photo_id') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ir_photo_id') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {{Form::label('ir_due_diligence', 'Are enhanced due diligence procedures required to be completed?')}}
                            {{Form::select('ir_due_diligence',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ir_due_diligence'),['class'=>'form-control form-control-sm'. ($errors->has('ir_due_diligence') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ir_due_diligence') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                </table>
                <strong>10.4 Politically Exposed Person (PEP)</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('pep_considered', 'Are any of the individuals considered as being a PEP?')}}
                            {{Form::select('pep_considered',["" => "Select","Yes"=>"Yes","No" =>"No"],old('pep_considered'),['class'=>'form-control form-control-sm'. ($errors->has('pep_considered') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('pep_considered') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('pep_due_diligence', 'Are enhanced due diligence procedures required to be completed?')}}
                            {{Form::select('pep_due_diligence',["" => "Select","Yes"=>"Yes","No" =>"No"],old('pep_due_diligence'),['class'=>'form-control form-control-sm'. ($errors->has('pep_due_diligence') ? ' is-invalid' : ''),'id'=>'pep_due_diligence'])}}
                            @foreach($errors->get('pep_due_diligence') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr id="pep_approved" style="display: none;">
                        <td>
                            {{Form::label('pep_approved', 'If yes, has Michael Bellew, Director & MLRO, approved that we accept the client in these circumstances?')}}
                            {{Form::select('pep_approved',["" => "Select","Yes"=>"Yes","No" =>"No"],old('pep_approved'),['class'=>'form-control form-control-sm'. ($errors->has('pep_approved') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('pep_approved') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 11</strong><br />
                <strong>Client Criteria Form</strong><br />
                <strong>Determining the integrity of the Prospective</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('ccr_business_understanding', 'We confirm that we have a full understanding of the client\'s business.')}}
                            {{Form::select('ccr_business_understanding',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_business_understanding'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_business_understanding') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ccr_business_understanding') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_services_understanding', 'We confirm that we have a complete understanding of the services to be provided to the client.')}}
                            {{Form::select('ccr_services_understanding',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_services_understanding'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_services_understanding') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ccr_services_understanding') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_concerns_ownership', 'Have we any concerns identifying ownership, key management or those charged with governance?')}}
                            {{Form::select('ccr_concerns_ownership',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_concerns_ownership'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_concerns_ownership') ? ' is-invalid' : ''),'id'=>'ccr_concerns_ownership'])}}
                            @foreach($errors->get('ccr_concerns_ownership') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_identify_concerns" style="display: none;">
                            {{Form::label('ccr_identify_concerns', 'If yes, please identify these concerns.')}}
                            {{ Form::textarea('ccr_identify_concerns', old('ccr_identify_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_confirm_integrity', 'We confirm that the firm does not have information that would lead it to conclude that the client lacks integrity.')}}
                            {{Form::select('ccr_confirm_integrity',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_confirm_integrity'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_confirm_integrity') ? ' is-invalid' : ''),'id'=>'ccr_confirm_integrity'])}}
                            @foreach($errors->get('ccr_confirm_integrity') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_confirm_integrity_no" style="display: none"><strong class="alert-danger">*If no we cannot act for this client.</strong> </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_management_concerns', 'Do we have concerns regarding management\'s competence?')}}
                            {{Form::select('ccr_management_concerns',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_management_concerns'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_management_concerns') ? ' is-invalid' : ''),'id'=>'ccr_management_concerns'])}}
                            @foreach($errors->get('ccr_management_concerns') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_identify_management_concerns" style="display: none">
                            {{Form::label('ccr_identify_management_concerns', 'If yes, please identify these concerns.')}}
                            {{ Form::textarea('ccr_identify_management_concerns', old('ccr_identify_management_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_financial_concerns', 'Do we have concerns with the financial condition of the company / individual / partnership?')}}
                            {{Form::select('ccr_financial_concerns',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_financial_concerns'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_financial_concerns') ? ' is-invalid' : ''),'id'=>'ccr_financial_concerns'])}}
                            @foreach($errors->get('ccr_financial_concerns') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_identify_financial_concerns" style="display: none">
                            {{Form::label('ccr_identify_financial_concerns', 'If yes, please identify these concerns.')}}
                            {{ Form::textarea('ccr_identify_financial_concerns', old('ccr_identify_financial_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_legal_environment', 'Does the entity operate in a specialist legal or regulatory environment?')}}
                            {{Form::select('ccr_legal_environment',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_legal_environment'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_legal_environment') ? ' is-invalid' : ''),'id'=>'ccr_legal_environment'])}}
                            @foreach($errors->get('ccr_financial_concerns') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_confirm_legal_environment" style="display: none;">
                            {{Form::label('ccr_confirm_legal_environment', 'If yes, please confirm exact nature of this environment.')}}
                            {{ Form::textarea('ccr_confirm_legal_environment', old('ccr_confirm_legal_environment'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_accountant', 'Is there any reason that the existing accountant / auditor may not wish to continue in the engagement?')}}
                            {{Form::select('ccr_accountant',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_accountant'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_accountant') ? ' is-invalid' : ''),'id'=>'ccr_accountant'])}}
                            @foreach($errors->get('ccr_accountant') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_accountant_concerns" style="display: none">
                            {{Form::label('ccr_accountant_concerns', 'If yes, do we have any concerns in this respect?')}}
                            {{ Form::textarea('ccr_accountant_concerns', old('ccr_accountant_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_accountant_frequency', 'Has there been a frequency in changing accountants / auditors?')}}
                            {{Form::select('ccr_accountant_frequency',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_accountant_frequency'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_accountant_frequency') ? ' is-invalid' : ''),'id'=>'ccr_accountant_frequency'])}}
                            @foreach($errors->get('ccr_accountant_frequency') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_accountant_frequency_concerns" style="display: none">
                            {{Form::label('ccr_accountant_frequency_concerns', 'If yes, do we have any concerns in this respect?')}}
                            {{ Form::textarea('ccr_accountant_frequency_concerns', old('ccr_accountant_frequency_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_firms', 'Have any other firms refused to act for this client?')}}
                            {{Form::select('ccr_firms',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_firms'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_firms') ? ' is-invalid' : ''),'id'=>'ccr_firms'])}}
                            @foreach($errors->get('ccr_firms') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_firms_concerns" style="display: none">
                            {{Form::label('ccr_firms_concerns', 'If yes, do we have any concerns in this respect?')}}
                            {{ Form::textarea('ccr_firms_concerns', old('ccr_firms_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_risk', 'Does this client pose any risk to existing client relationships?')}}
                            {{Form::select('ccr_risk',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_risk'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_risk') ? ' is-invalid' : ''),'id'=>'ccr_risk'])}}
                            @foreach($errors->get('ccr_risk') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_risk_concerns" style="display: none">
                            {{Form::label('ccr_risk_concerns', 'If yes, please give details.')}}
                            {{ Form::textarea('ccr_risk_concerns', old('ccr_risk_concerns'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                </table>
                <strong>Determining the Competency of the  Firm to Perform the Engagement</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('ccr_confirm_engagement_director', 'We confirm that the engagement director has experience in the entity\'s business type')}}
                            {{Form::select('ccr_confirm_engagement_director',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_confirm_engagement_director'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_confirm_engagement_director') ? ' is-invalid' : ''),'id'=>'ccr_confirm_engagement_director'])}}
                            @foreach($errors->get('ccr_confirm_engagement_director') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_confirm_engagement_director_no" style="display: none"><strong class="alert-danger">*If no we cannot act for this client.</strong> </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_confirm_engagement_manager', 'We confirm that the engagement manager has experience in the entity\'s business type.')}}
                            {{Form::select('ccr_confirm_engagement_manager',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_confirm_engagement_manager'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_confirm_engagement_manager') ? ' is-invalid' : ''),'id'=>'ccr_confirm_engagement_manager'])}}
                            @foreach($errors->get('ccr_confirm_engagement_manager') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_confirm_engagement_manager_no" style="display: none"><strong class="alert-danger">*If no we cannot act for this client.</strong> </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_exposure_regulated_business', 'Does this client have any exposure in regulated business?')}}
                            {{Form::select('ccr_exposure_regulated_business',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_exposure_regulated_business'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_exposure_regulated_business') ? ' is-invalid' : ''),'id'=>'ccr_exposure_regulated_business'])}}
                            @foreach($errors->get('ccr_exposure_regulated_business') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_exposure_regulated_business_confirm" style="display: none">
                            {{Form::label('ccr_exposure_regulated_business_confirm', 'If yes, confirm exact nature of this regulated business.')}}
                            {{ Form::textarea('ccr_exposure_regulated_business_confirm', old('ccr_exposure_regulated_business_confirm'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_availability_concerns', 'Are there any concerns regarding the availability of professional staff to enable completion of the engagement to both a quality standard and in a timely manner?')}}
                            {{Form::select('ccr_availability_concerns',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_availability_concerns'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_availability_concerns') ? ' is-invalid' : ''),'id'=>'ccr_availability_concerns'])}}
                            @foreach($errors->get('ccr_availability_concerns') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_availability_concerns_no" style="display: none"><strong class="alert-danger">*If yes we cannot act for this client.</strong> </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_partner_concerns', 'Are there any concerns regarding the partner and staff having sufficient knowledge and experience for the engagement?')}}
                            {{Form::select('ccr_partner_concerns',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_partner_concerns'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_partner_concerns') ? ' is-invalid' : ''),'id'=>'ccr_partner_concerns'])}}
                            @foreach($errors->get('ccr_partner_concerns') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_partner_concerns_no" style="display: none"><strong class="alert-danger">*If yes we cannot act for this client.</strong> </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_staffing_engagement', 'Are there any matters related to staffing the engagement which would indicate that the engagement should not be accepted or why such acceptance needs to be considered by a second partner?')}}
                            {{Form::select('ccr_staffing_engagement',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_staffing_engagement'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_staffing_engagement') ? ' is-invalid' : ''),'id'=>'ccr_staffing_engagement'])}}
                            @foreach($errors->get('ccr_staffing_engagement') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_staffing_engagement_no" style="display: none"><strong class="alert-danger">*If yes we cannot act for this client.</strong> </td>
                    </tr>
                </table>
                <strong>Compliance with Ethical Requirements</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('ccr_client_connection', 'Has the firm any existing connection with the new client?')}}
                            {{Form::select('ccr_client_connection',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_client_connection'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_client_connection') ? ' is-invalid' : ''),'id'=>'ccr_client_connection'])}}
                            @foreach($errors->get('ccr_client_connection') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_client_connection_details" style="display: none">
                            {{Form::label('ccr_client_connection_details', 'If yes, please give details.')}}
                            {{ Form::textarea('ccr_client_connection_details', old('ccr_client_connection_details'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_already_service', 'Do we already provide any services to the client?')}}
                            {{Form::select('ccr_already_service',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_already_service'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_already_service') ? ' is-invalid' : ''),'id'=>'ccr_already_service'])}}
                            @foreach($errors->get('ccr_already_service') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_already_service_details" style="display: none">
                            {{Form::label('ccr_already_service_details', 'If yes, please give details.')}}
                            {{ Form::textarea('ccr_already_service_details', old('ccr_already_service_details'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_staff_connection', 'Has any member of staff any connection with the new client?')}}
                            {{Form::select('ccr_staff_connection',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_staff_connection'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_staff_connection') ? ' is-invalid' : ''),'id'=>'ccr_staff_connection'])}}
                            @foreach($errors->get('ccr_staff_connection') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_staff_connection_details" style="display: none">
                            {{Form::label('ccr_staff_connection_details', 'If yes, please give details.')}}
                            {{ Form::textarea('ccr_staff_connection_details', old('ccr_staff_connection_details'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_previously_acted', 'Have we previously acted for this client in any respect?')}}
                            {{Form::select('ccr_previously_acted',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_previously_acted'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_previously_acted') ? ' is-invalid' : ''),'id'=>'ccr_previously_acted'])}}
                            @foreach($errors->get('ccr_previously_acted') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_previously_acted_details" style="display: none">
                            {{Form::label('ccr_previously_acted_details', 'If yes, please give details.')}}
                            {{ Form::textarea('ccr_previously_acted_details', old('ccr_previously_acted_details'), ['class'=>'form-control form-control-sm','rows' => 5, 'cols' => 100]) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{Form::label('ccr_potential_risk', 'Is there any potential conflict of risk?')}}
                            {{Form::select('ccr_potential_risk',["" => "Select","Yes"=>"Yes","No" =>"No"],old('ccr_potential_risk'),['class'=>'form-control form-control-sm'. ($errors->has('ccr_potential_risk') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('ccr_potential_risk') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="ccr_potential_risk_no" style="display: none"><strong class="alert-danger">*If yes we cannot act for this client.</strong> </td>
                    </tr>
                </table>
                {{--<hr />--}}
                {{--<strong>Section 12</strong><br />
                <strong>Client Authorisation</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('client_authorization', 'The client has signed a letter of authorisation to authorise UHY FDW to act on the instructions of the named directors / partners / members of staff / individuals and this document has been saved to file.')}}
                            {{Form::select('client_authorization',["" => "Select","Yes"=>"Yes","No" =>"No"],old('client_authorization'),['class'=>'form-control form-control-sm'. ($errors->has('client_authorization') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('client_authorization') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <hr />
                <strong>Section 13</strong><br />
                <strong>Process for New Clients Spreadsheet</strong>
                <table class="table table-borderless" style="width:700px">
                    <tr>
                        <td>
                            {{Form::label('client_process_completed', 'Process for New Clients spreadsheet completed.')}}
                            {{Form::select('client_process_completed',["" => "Select","Yes"=>"Yes","No" =>"No"],old('client_process_completed'),['class'=>'form-control form-control-sm'. ($errors->has('client_process_completed') ? ' is-invalid' : '')])}}
                            @foreach($errors->get('client_process_completed') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>--}}


                <div class="blackboard-fab mr-3 mb-3">
                    <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-save"></i> Save</button>
                </div>


            </div>
        </div>
        {{Form::close()}}
    </div>
@endsection
@section("extra-css")
    <style>
        tbody{
            width:100%;
        }
        td label{
            font-weight:normal !important;
        }
        td select{
            width:auto !important;
        }

        hr{
            border-top: 3px dashed #138496;
        }
    </style>
@endsection
@section("extra-js")
    <script>
        function manadept(row){
            let manval = $('#manadept'+row).val();
            if(manval === '') {
                $('.main' + row).show();
                $('.auditdd' + row).hide();
                $('.cosecdd' + row).hide();
                $('.taxdd' + row).hide();
            }

            if(manval === 'Tax'){
                $('.main' + row).hide();
                $('.auditdd' + row).hide();
                $('.cosecdd' + row).hide();
                $('.taxdd' + row).show();
            }

            if(manval === 'Audit/Accounts'){
                $('.main' + row).hide();
                $('.auditdd' + row).show();
                $('.cosecdd' + row).hide();
                $('.taxdd' + row).hide();
            }

            if(manval === 'CoSec'){
                $('.main' + row).hide();
                $('.auditdd' + row).hide();
                $('.cosecdd' + row).show();
                $('.taxdd' + row).hide();
            }
        }

        $(document).ready(function(){


            $(".p_same_as_correspondence").on("click",function(){
                if($(".p_same_as_correspondence").is(":checked")) {
                    $('.p_business_address1').val($('.p_billing_address1').val());
                    $('.p_business_address2').val($('.p_billing_address2').val());
                    $('.p_business_address3').val($('.p_billing_address3').val());
                    $('.p_business_town').val($('.p_billing_town').val());
                    $('.p_business_pcode').val($('.p_billing_pcode').val());
                    $('.p_business_county').val($('.p_billing_county').val());
                    $('.p_business_country').val($('.p_billing_country').val());
                } else {
                    $('.p_business_address1').val("");
                    $('.p_business_address2').val("");
                    $('.p_business_address3').val("");
                    $('.p_business_town').val("");
                    $('.p_business_pcode').val("");
                    $('.p_business_county').val("");
                    $('.p_business_country').val("");
                }
            })

            $(".st_same_as_personal").on("click",function(){
                if($(".st_same_as_personal").is(":checked")) {
                    $('.st_billing_address1').val($('.st_personal_address1').val());
                    $('.st_billing_address2').val($('.st_personal_address2').val());
                    $('.st_billing_address3').val($('.st_personal_address3').val());
                    $('.st_billing_town').val($('.st_personal_town').val());
                    $('.st_billing_pcode').val($('.st_personal_pcode').val());
                    $('.st_billing_county').val($('.st_personal_county').val());
                    $('.st_billing_country').val($('.st_personal_country').val());
                } else {
                    $('.st_billing_address1').val("");
                    $('.st_billing_address2').val("");
                    $('.st_billing_address3').val("");
                    $('.st_billing_town').val("");
                    $('.st_billing_pcode').val("");
                    $('.st_billing_county').val("");
                    $('.st_billing_country').val("");
                }
            })

            $(".st_same_as_personal2").on("click",function(){
                if($(".st_same_as_personal2").is(":checked")) {
                    $('.st_business_address1').val($('.st_personal_address1').val());
                    $('.st_business_address2').val($('.st_personal_address2').val());
                    $('.st_business_address3').val($('.st_personal_address3').val());
                    $('.st_business_town').val($('.st_personal_town').val());
                    $('.st_business_pcode').val($('.st_personal_pcode').val());
                    $('.st_business_county').val($('.st_personal_county').val());
                    $('.st_business_country').val($('.st_personal_country').val());
                } else {
                    $('.st_business_address1').val("");
                    $('.st_business_address2').val("");
                    $('.st_business_address3').val("");
                    $('.st_business_town').val("");
                    $('.st_business_pcode').val("");
                    $('.st_business_county').val("");
                    $('.st_business_country').val("");
                }
            })

            $(".st_same_as_correspondence").on("click",function(){
                if($(".st_same_as_correspondence").is(":checked")) {
                    $('.st_business_address1').val($('.st_billing_address1').val());
                    $('.st_business_address2').val($('.st_billing_address2').val());
                    $('.st_business_address3').val($('.st_billing_address3').val());
                    $('.st_business_town').val($('.st_billing_town').val());
                    $('.st_business_pcode').val($('.st_billing_pcode').val());
                    $('.st_business_county').val($('.st_billing_county').val());
                    $('.st_business_country').val($('.st_billing_country').val());
                } else {
                    $('.st_business_address1').val("");
                    $('.st_business_address2').val("");
                    $('.st_business_address3').val("");
                    $('.st_business_town').val("");
                    $('.st_business_pcode').val("");
                    $('.st_business_county').val("");
                    $('.st_business_country').val("");
                }
            })


            $(".same_as_correspondence").on("click",function(){
                if($(".same_as_correspondence").is(":checked")) {
                    $('.business_address1').val($('.billing_address1').val());
                    $('.business_address2').val($('.billing_address2').val());
                    $('.business_address3').val($('.billing_address3').val());
                    $('.business_town').val($('.billing_town').val());
                    $('.business_pcode').val($('.billing_pcode').val());
                    $('.business_county').val($('.billing_county').val());
                    $('.business_country').val($('.billing_country').val());
                } else {
                    $('.business_address1').val("");
                    $('.business_address2').val("");
                    $('.business_address3').val("");
                    $('.business_town').val("");
                    $('.business_pcode').val("");
                    $('.business_county').val("");
                    $('.business_country').val("");
                }
            })

            $(".same_as_correspondence2").on("click",function(){
                if($(".same_as_correspondence2").is(":checked")) {
                    $('.registered_address1').val($('.billing_address1').val());
                    $('.registered_address2').val($('.billing_address2').val());
                    $('.registered_address3').val($('.billing_address3').val());
                    $('.registered_town').val($('.billing_town').val());
                    $('.registered_pcode').val($('.billing_pcode').val());
                    $('.registered_county').val($('.billing_county').val());
                    $('.registered_country').val($('.billing_country').val());
                } else {
                    $('.registered_address1').val("");
                    $('.registered_address2').val("");
                    $('.registered_address3').val("");
                    $('.registered_town').val("");
                    $('.registered_pcode').val("");
                    $('.registered_county').val("");
                    $('.registered_country').val("");
                }
            })

            $(".same_as_business").on("click",function(){
                if($(".same_as_business").is(":checked")) {
                    $('.registered_address1').val($('.business_address1').val());
                    $('.registered_address2').val($('.business_address2').val());
                    $('.registered_address3').val($('.business_address3').val());
                    $('.registered_town').val($('.business_town').val());
                    $('.registered_pcode').val($('.business_pcode').val());
                    $('.registered_county').val($('.business_county').val());
                    $('.registered_country').val($('.business_country').val());
                } else {
                    $('.registered_address1').val("");
                    $('.registered_address2').val("");
                    $('.registered_address3').val("");
                    $('.registered_town').val("");
                    $('.registered_pcode').val("");
                    $('.registered_county').val("");
                    $('.registered_country').val("");
                }
            })

            $("#ir_satisfied").on("change",function(){
                if($("#ir_satisfied").val() === "No"){
                    $("#ir_y_what_basis").hide();
                } else {
                    $("#ir_y_what_basis").show();
                }

                if($("#ir_satisfied").val().length === 0){
                    $("#ir_y_what_basis").hide();
                }
            })

            $("#pep_due_diligence").on("change",function(){
                if($("#pep_due_diligence").val() === "No"){
                    $("#pep_approved").hide();
                } else {
                    $("#pep_approved").show();
                }

                if($("#pep_due_diligence").val().length === 0){
                    $("#pep_approved").hide();
                }
            })

            $("#ccr_concerns_ownership").on("change",function(){
                if($("#ccr_concerns_ownership").val() === "No"){
                    $("#ccr_identify_concerns").hide();
                } else {
                    $("#ccr_identify_concerns").show();
                }

                if($("#ccr_concerns_ownership").val().length === 0){
                    $("#ccr_identify_concerns").hide();
                }
            })

            $("#ccr_management_concerns").on("change",function(){
                if($("#ccr_management_concerns").val() === "No"){
                    $("#ccr_identify_management_concerns").hide();
                } else {
                    $("#ccr_identify_management_concerns").show();
                }

                if($("#ccr_management_concerns").val().length === 0){
                    $("#ccr_identify_management_concerns").hide();
                }
            })


            $("#ccr_financial_concerns").on("change",function(){
                if($("#ccr_financial_concerns").val() === "No"){
                    $("#ccr_identify_financial_concerns").hide();
                } else {
                    $("#ccr_identify_financial_concerns").show();
                }

                if($("#ccr_financial_concerns").val().length === 0){
                    $("#ccr_identify_financial_concerns").hide();
                }
            })


            $("#ccr_legal_environment").on("change",function(){
                if($("#ccr_legal_environment").val() === "No"){
                    $("#ccr_confirm_legal_environment").hide();
                } else {
                    $("#ccr_confirm_legal_environment").show();
                }

                if($("#ccr_legal_environment").val().length === 0){
                    $("#ccr_confirm_legal_environment").hide();
                }
            })

            $("#ccr_accountant").on("change",function(){
                if($("#ccr_accountant").val() === "No"){
                    $("#ccr_accountant_concerns").hide();
                } else {
                    $("#ccr_accountant_concerns").show();
                }

                if($("#ccr_accountant").val().length === 0){
                    $("#ccr_accountant_concerns").hide();
                }
            })

            $("#ccr_accountant_frequency").on("change",function(){
                if($("#ccr_accountant_frequency").val() === "No"){
                    $("#ccr_accountant_frequency_concerns").hide();
                } else {
                    $("#ccr_accountant_frequency_concerns").show();
                }

                if($("#ccr_accountant_frequency").val().length === 0){
                    $("#ccr_accountant_frequency_concerns").hide();
                }
            })

            $("#ccr_firms").on("change",function(){
                if($("#ccr_firms").val() === "No"){
                    $("#ccr_firms_concerns").hide();
                } else {
                    $("#ccr_firms_concerns").show();
                }

                if($("#ccr_firms").val().length === 0){
                    $("#ccr_firms_concerns").hide();
                }
            })

            $("#ccr_risk").on("change",function(){
                if($("#ccr_risk").val() === "No"){
                    $("#ccr_risk_concerns").hide();
                } else {
                    $("#ccr_risk_concerns").show();
                }

                if($("#ccr_risk").val().length === 0){
                    $("#ccr_risk_concerns").hide();
                }
            })

            $("#ccr_confirm_engagement_manager").on("change",function(){
                if($("#ccr_confirm_engagement_manager").val() === "Yes"){
                    $("#ccr_confirm_engagement_manager_no").hide();
                } else {
                    $("#ccr_confirm_engagement_manager_no").show();
                }

                if($("#ccr_confirm_engagement_manager").val().length === 0){
                    $("#ccr_confirm_engagement_manager_no").hide();
                }
            })

            $("#ccr_confirm_engagement_director").on("change",function(){
                if($("#ccr_confirm_engagement_director").val() === "Yes"){
                    $("#ccr_confirm_engagement_director_no").hide();
                } else {
                    $("#ccr_confirm_engagement_director_no").show();
                }

                if($("#ccr_confirm_engagement_director").val().length === 0){
                    $("#ccr_confirm_engagement_director_no").hide();
                }
            })

            $("#ccr_exposure_regulated_business").on("change",function(){
                if($("#ccr_exposure_regulated_business").val() === "No"){
                    $("#ccr_exposure_regulated_business_confirm").hide();
                } else {
                    $("#ccr_exposure_regulated_business_confirm").show();
                }

                if($("#ccr_exposure_regulated_business").val().length === 0){
                    $("#ccr_exposure_regulated_business_confirm").hide();
                }
            })

            $("#ccr_availability_concerns").on("change",function(){
                if($("#ccr_availability_concerns").val() === "No"){
                    $("#ccr_availability_concerns_no").hide();
                } else {
                    $("#ccr_availability_concerns_no").show();
                }

                if($("#ccr_availability_concerns").val().length === 0){
                    $("#ccr_availability_concerns_no").hide();
                }
            })

            $("#ccr_partner_concerns").on("change",function(){
                if($("#ccr_partner_concerns").val() === "No"){
                    $("#ccr_partner_concerns_no").hide();
                } else {
                    $("#ccr_partner_concerns_no").show();
                }

                if($("#ccr_partner_concerns").val().length === 0){
                    $("#ccr_partner_concerns_no").hide();
                }
            })

            $("#ccr_staffing_engagement").on("change",function(){
                if($("#ccr_staffing_engagement").val() === "No"){
                    $("#ccr_staffing_engagement_no").hide();
                } else {
                    $("#ccr_staffing_engagement_no").show();
                }

                if($("#ccr_staffing_engagement").val().length === 0){
                    $("#ccr_staffing_engagement_no").hide();
                }
            })

            $("#ccr_already_service").on("change",function(){
                if($("#ccr_already_service").val() === "No"){
                    $("#ccr_already_service_details").hide();
                } else {
                    $("#ccr_already_service_details").show();
                }

                if($("#ccr_already_service").val().length === 0){
                    $("#ccr_already_service_details").hide();
                }
            })

            $("#ccr_client_connection").on("change",function(){
                if($("#ccr_client_connection").val() === "No"){
                    $("#ccr_client_connection_details").hide();
                } else {
                    $("#ccr_client_connection_details").show();
                }

                if($("#ccr_client_connection").val().length === 0){
                    $("#ccr_client_connection_details").hide();
                }
            })

            $("#ccr_staff_connection").on("change",function(){
                if($("#ccr_staff_connection").val() === "No"){
                    $("#ccr_staff_connection_details").hide();
                } else {
                    $("#ccr_staff_connection_details").show();
                }

                if($("#ccr_staff_connection").val().length === 0){
                    $("#ccr_staff_connection_details").hide();
                }
            })

            $("#ccr_previously_acted").on("change",function(){
                if($("#ccr_previously_acted").val() === "No"){
                    $("#ccr_previously_acted_details").hide();
                } else {
                    $("#ccr_previously_acted_details").show();
                }

                if($("#ccr_previously_acted").val().length === 0){
                    $("#ccr_previously_acted_details").hide();
                }
            })

            $("#ccr_potential_risk").on("change",function(){
                if($("#ccr_potential_risk").val() === "No"){
                    $("#ccr_potential_risk_no").hide();
                } else {
                    $("#ccr_potential_risk_no").show();
                }

                if($("#ccr_potential_risk").val().length === 0){
                    $("#ccr_potential_risk_no").hide();
                }
            })

            $("#ccr_confirm_integrity").on("change",function(){
                if($("#ccr_confirm_integrity").val() === "Yes"){
                    $("#ccr_confirm_integrity_no").hide();
                } else {
                    $("#ccr_confirm_integrity_no").show();
                }

                if($("#ccr_confirm_integrity").val().length === 0){
                    $("#ccr_confirm_integrity_no").hide();
                }
            })

            $("#sec3_industry").on("change",function(){
                if($("#sec3_industry").val() === "Other"){
                    $("#sec3_industry2").show();
                } else {
                    $("#sec3_industry2").hide();
                }

                if($("#sec3_industry").val().length === 0){
                    $("#sec3_industry2").hide();
                }
            })

            $("#sec3_country").on("change",function(){
                if($("#sec3_country").val() === "Other"){
                    $("#sec3_country2").show();
                } else {
                    $("#sec3_country2").hide();
                }

                if($("#sec3_country").val().length === 0){
                    $("#sec3_country2").hide();
                }
            })
        })

        $('#addshareholder').on('click',function() {
            let nextsh = $('#shareholders > div').length;
            let cur = nextsh+1;
            let shareholderhtml = '<div id="shareholders'+cur+'" class="shareholders">\n' +
                '                        <strong>Shareholder '+cur+'</strong>\n' +
                '                        <table class="table table-borderless" style="width:700px">\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Name</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][name]" class="form-control form-control-sm" placeholder="Name" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Position</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][position]" class="form-control form-control-sm" placeholder="Position" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Office Number</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][office_number]" class="form-control form-control-sm" placeholder="Office Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="sh['+nextsh+'][office_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Home Number</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][home_number]" class="form-control form-control-sm" placeholder="Home Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="sh['+nextsh+'][home_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Mobile Number</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][mobile_number]" class="form-control form-control-sm" placeholder="Mobile Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="sh['+nextsh+'][mobile_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>E-mail Address</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][email_address]" class="form-control form-control-sm" placeholder="E-mail Address" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="sh['+nextsh+'][email_address_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                        </table>\n' +
                '                        <strong>Residential Address</strong>\n' +
                '                        <table class="table table-borderless" style="width:700px">\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 1</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][address1]" class="form-control form-control-sm" placeholder="Address 1" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Address 2</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][address2]" class="form-control form-control-sm" placeholder="Address 2" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 3</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][address3]" class="form-control form-control-sm" placeholder="Address 3" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Town</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][town]" class="form-control form-control-sm" placeholder="Town" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>County</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][county]" class="form-control form-control-sm" placeholder="County" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Post Code</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][pcode]" class="form-control form-control-sm" placeholder="Post Code" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Country</label>\n' +
                '                                    <input type="text" name="sh['+nextsh+'][country]" class="form-control form-control-sm" placeholder="Country" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                        </table>\n' +
                '                        </div>'
            $('#shareholders').append(shareholderhtml);
        })

        $('#adddirector').on('click',function() {
            let nextdir = $('#directors > div').length;
            let cur = nextdir+1;
            let directorhtml = '<div id="directors'+cur+'" class="directors">\n' +
                '                        <strong>Director '+cur+'</strong>\n' +
                '                        <table class="table table-borderless" style="width:700px">\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Name</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][name]" class="form-control form-control-sm" placeholder="Name" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Position</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][position]" class="form-control form-control-sm" placeholder="Position" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Office Number</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][office_number]" class="form-control form-control-sm" placeholder="Office Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="dir['+nextdir+'][office_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Home Number</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][home_number]" class="form-control form-control-sm" placeholder="Home Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="dir['+nextdir+'][home_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Mobile Number</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][mobile_number]" class="form-control form-control-sm" placeholder="Mobile Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="dir['+nextdir+'][mobile_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>E-mail Address</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][email_address]" class="form-control form-control-sm" placeholder="E-mail Address" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="dir['+nextdir+'][email_address_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                        </table>\n' +
                '                        <strong>Residential Address</strong>\n' +
                '                        <table class="table table-borderless" style="width:700px">\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 1</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][address1]" class="form-control form-control-sm" placeholder="Address 1" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Address 2</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][address2]" class="form-control form-control-sm" placeholder="Address 2" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 3</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][address3]" class="form-control form-control-sm" placeholder="Address 3" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Town</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][town]" class="form-control form-control-sm" placeholder="Town" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>County</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][county]" class="form-control form-control-sm" placeholder="County" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Post Code</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][pcode]" class="form-control form-control-sm" placeholder="Post Code" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Country</label>\n' +
                '                                    <input type="text" name="dir['+nextdir+'][country]" class="form-control form-control-sm" placeholder="Country" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                        </table>\n' +
                '                        </div>'
            $('#directors').append(directorhtml);
        })

        $('#addpartner').on('click',function() {
            let nextpart = $('#partners > div').length;
            let cur = nextpart+1;
            let partnerhtml = '<div id="partners'+cur+'" class="partners">\n' +
                '                        <strong>Partner '+cur+'</strong>\n' +
                '                        <table class="table table-borderless" style="width:700px">\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Name</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][name]" class="form-control form-control-sm" placeholder="Name" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 1</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][address1]" class="form-control form-control-sm" placeholder="Address 1" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Address 2</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][address2]" class="form-control form-control-sm" placeholder="Address 2" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Address 3</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][address3]" class="form-control form-control-sm" placeholder="Address 3" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Town</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][town]" class="form-control form-control-sm" placeholder="Town" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>County</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][county]" class="form-control form-control-sm" placeholder="County" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '                                    <label>Post Code</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][pcode]" class="form-control form-control-sm" placeholder="Post Code" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Country</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][country]" class="form-control form-control-sm" placeholder="Country" />\n' +
                '                                </td>\n' +
                '                                <td>\n' +
                '\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Home Number</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][home_number]" class="form-control form-control-sm" placeholder="Home Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="part['+nextpart+'][home_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>Mobile Number</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][mobile_number]" class="form-control form-control-sm" placeholder="Mobile Number" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="part['+nextpart+'][mobile_number_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>E-mail Address 1</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][email_address1]" class="form-control form-control-sm" placeholder="E-mail Address" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="part['+nextpart+'][email_address1_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td>\n' +
                '                                    <label>E-mail Address 2</label>\n' +
                '                                    <input type="text" name="part['+nextpart+'][email_address2]" class="form-control form-control-sm" placeholder="E-mail Address" />\n' +
                '                                </td>\n' +
                '                                <td style="vertical-align: bottom;margin-bottom:4px;">\n' +
                '                                    N/A&nbsp;&nbsp;<input type="checkbox" name="part['+nextpart+'][email_address2_na]" />\n' +
                '                                </td>\n' +
                '                            </tr>\n' +
                '                        </table>\n' +
                '                        </div>'
            $('#partners').append(partnerhtml);
        })

        $('#addmanager').on('click',function() {
            let nextpart = $('#managers > tr').length;
            let cur = nextpart+1;
            let managerhtml = '<tr>\n' +
                '                        <td width="33.33%">\n' +
                '                            <select name="man['+nextpart+'][mandept]" id="manadept'+nextpart+'" class="form-control form-control-sm" onclick="manadept('+nextpart+')">\n' +
                '                                <option value="">None</option>\n' +
                '                                <option value="Audit/Accounts">Audit/Accounts</option>\n' +
                '                                <option value="Tax">Tax</option>\n' +
                '                                <option value="CoSec">Cosec</option>\n' +
                '                            </select>\n' +
                '                        </td>\n' +
                '                        <td>\n' +
                '                            <select name="man['+nextpart+'][manager]" class="main'+nextpart+' form-control form-control-sm">\n' +
                '\t\t\t\t\t\t\t\t<option value="">Select</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Martina Gribben">Martina Gribben</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Eric McQuillan">Eric McQuillan</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Michelle Martin">Michelle Martin</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Lauren Campion">Lauren Campion</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Pauline McKevitt">Pauline McKevitt</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Mairead Rooney">Mairead Rooney</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Jane Jackson">Jane Jackson</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Niall Donnelly">Niall Donnelly</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Richard Windrum">Richard Windrum</option>\n' +
                '\t\t\t\t\t\t\t</select>\n' +
                '\t\t\t\t\t\t\t<select name="man['+nextpart+'][audit_manager]" class="auditdd'+nextpart+' form-control form-control-sm" style="display:none;">\n' +
                '\t\t\t\t\t\t\t\t<option value="">Select</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Martina Gribben">Martina Gribben</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Eric McQuillan">Eric McQuillan</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Michelle Martin">Michelle Martin</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Lauren Campion">Lauren Campion</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Pauline McKevitt">Pauline McKevitt</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Matthew Whelan">Matthew Whelan</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Tracy Corkey">Tracy Corkey</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Leanne Black">Leanne Black</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Sylwia Krzysztofik">Sylwia Krzysztofik</option>\n' +
                '\t\t\t\t\t\t\t</select>\n' +
                '\t\t\t\t\t\t\t<select name="man['+nextpart+'][tax_manager]" class="taxdd'+nextpart+' form-control form-control-sm" style="display:none;">\n' +
                '\t\t\t\t\t\t\t\t<option value="">Select</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Mairead Rooney">Mairead Rooney</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Jane Jackson">Jane Jackson</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Niall Donnelly">Niall Donnelly</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Matthew Whelan">Matthew Whelan</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Fergal Maher">Fergal Maher</option>\n' +
                '\t\t\t\t\t\t\t</select>\n' +
                '\t\t\t\t\t\t\t<select name="man['+nextpart+'][cosec_manager]" class="cosecdd'+nextpart+' form-control form-control-sm" style="display:none;">\n' +
                '\t\t\t\t\t\t\t\t<option value="">Select</option>\n' +
                '\t\t\t\t\t\t\t\t<option value="Richard Windrum">Richard Windrum</option>\n' +
                '\t\t\t\t\t\t\t</select>\n' +
                '                        </td>\n' +
                '                        <td>\n' +
                '\n' +
                '                        </td>\n' +
                '                    </tr>'
            $('#managers').append(managerhtml);
        })


    </script>
    <script>
        $(document).ready(function(){
            let start;
            let i;

            function setFlashMessageInterval(){
                start = setInterval(function(){
                    clearInterval(start);
                    $('#autoSave').removeClass('alert').removeClass('alert-success').text('');
                }, 5000);
                i = 0;
            }

            function autoSave()
            {
                setFlashMessageInterval();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let myForm = document.getElementById('crfform');
                let myFormData = new FormData(myForm);

                {
                    $.ajax({
                        url:"/clients/" + {{$client->id}} + "/forms/crfautosave",
                        data:myFormData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success:function(data)
                        {
                            if(data != '')
                            {
                                $('#form_id').val(data.id);
                            }
                            $('#autoSave').addClass('alert').addClass('alert-success').text("Form was automatically saved.");
                            start;
                        }
                    });
                }
            }
            setInterval(function(){
                autoSave();
            }, 60000);
        });
    </script>
@endsection