@extends('clients.show')

@section('tab-content')
    <div class="col-lg-12">

        {{--@if($crf == 0)--}}
@foreach($existing_dynamic_forms as $existing_dynamic_form)
        @if(in_array($existing_dynamic_form['id'],$dynamic_forms))
                <span class="btn btn-outline-secondary btn-sm btn-existing-form"><i class="fa fa-plus"></i> New {{$existing_dynamic_form['name']}} Form</span>
        @else
                <a href="javascript:void(0)" onclick="newForm({{$client->id}},{{$existing_dynamic_form['id']}},{{$existing_dynamic_form['section']}})" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> New {{$existing_dynamic_form['name']}} Form</a>
        @endif
@endforeach
        {{--@endif--}}
        <a href="{{route('forms.uploadforms',['client'=>$client->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Upload Form</a>
        {{--<a href="{{route('forms.upload',['client'=>$client->id])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Upload Form</a>--}}

        <div class="table-responsive mt-3">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>Name</th>
                    <th>Form Type</th>
                    <th>Type</th>
                    {{--<th>Size</th>--}}
                    <th>Uploader</th>
                    <th>Signed</th>
                    <th>Signed Date</th>
                    <th>Added</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($list as $crf)
                    <tr>
                        @if(strpos($crf->form_type, "CRF Form") !== false)
                            @if(strpos($crf->file, "CRF_Form") !== false)
                                <td><a href="{{route('forms.generatecrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" download>{{$crf->name}}</a></td>
                            @else
                                <td><a href="{{route('crf_client',['q'=>$crf->file])}}" target="_blank">{{$crf->name}}</a></td>
                            @endif
                        @elseif(strpos($crf->form_type, "Other") !== false)
                        <td><a href="{{route('crf_client',['q'=>$crf->file])}}" target="_blank">{{$crf->name}}</a></td>
                        @else
                        <td><a href="{{route('forms.process',['client_id'=>$client->id,'form_id'=>$crf->id])}}" target="_blank">{{$crf->name}}</a></td>
                        @endif
                        <td>{{$crf->form_type}}</td>
                        <td>{{$crf->type()}}</td>
                        {{--<td>{{$crf->size()}}</td>--}}
                        <td><a href="{{route('profile',$crf->user)}}" title="{{$crf->user->name()}}"><img src="{{route('avatar',['q'=>$crf->user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline" alt="{{$crf->user->name()}}"/></a></td>
                        <td>{{($crf->signed && $crf->signed != null ? ($crf->signed == 1 ? 'Yes' : 'No') : '')}}</td>
                        <td>{{($crf->signed_date && $crf->signed_date != null ? \Illuminate\Support\Carbon::parse($crf->signed_date)->format('Y-m-d') : '')}}</td>
                        <td>{{$crf->created_at->diffForHumans()}}</td>
                        <td class="last" align="right">
                            @if(strpos($crf->form_type, "CRF Form") !== false)
                                @if(!isset($crf->signed) && $crf->signed != '1')
                                    {{Form::open(['url' => route('forms.signcrf',['clientid'=>$client->id,'formid'=>$crf->id]), 'method' => 'get','id'=>'crf_form'.$crf->id])}}
                                    <a href="javascript:void(0)" id="sign_form_{{$crf->id}}" class="btn btn-sm btn-dark" onclick="SignCRF({{$crf->id}},{{$client->id}})">Sign</a>&nbsp;
                                    <a href="{{route('forms.generatecrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" class="btn btn-sm btn-info">Generate</a>
                                    <a href="{{route('forms.editcrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" class="btn btn-success btn-sm">Edit</a>
                                    {{Form::close()}}
                                @else
                                    <a href="{{route('forms.generatecrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" class="btn btn-sm btn-info">Generate</a>
                                    <a href="{{route('forms.editcrf',['clientid'=>$client->id,'formid'=>$crf->crf_form_id])}}" class="btn btn-success btn-sm">Edit</a>
                                @endif
                            @elseif(strpos($crf->form_type, "Other") !== false)
                                    <a href="{{route('crf_client',['q'=>$crf->file])}}" class="btn btn-sm btn-info">Download</a>
                                    <a href="{{route('forms.editforms',['clientid'=>$client->id,'formid'=>$crf->id])}}" class="btn btn-success btn-sm">Edit</a>
                            @else
                                @if((!isset($crf->signed) && $crf->signed != '1') && $crf->forms["signature"] == 1)
                                    {{Form::open(['url' => route('forms.sign',['clientid'=>$client->id,'formid'=>$crf->id]), 'method' => 'get','id'=>'form_form'.$crf->id])}}
                                    <a href="javascript:void(0)" id="form_form_{{$crf->id}}" class="btn btn-sm btn-dark" onclick="SignForm({{$crf->id}},{{$client->id}})">Sign</a>
                                    {{--<a href="{{route('forms.view',['client_id'=>$client->id,'form_id'=>$crf->id])}}" target="_blank" class="btn btn-sm btn-primary">View</a>--}}
                                    <a href="{{route('forms.process',['client_id'=>$client->id,'form_id'=>$crf->id])}}" class="btn btn-sm btn-info">Generate</a>
                                    <a href="{{route('forms.dynamic_form',['client_id'=>$client->id,'form_id'=>$crf->id,'section_id'=>0])}}" class="btn btn-success btn-sm">Edit</a>
                                    {{Form::close()}}
                                @else
                                    {{--<a href="{{route('forms.view',['client_id'=>$client->id,'form_id'=>$crf->id])}}" class="btn btn-sm btn-primary">View</a>--}}
                                    <a href="{{route('forms.process',['client_id'=>$client->id,'form_id'=>$crf->id])}}" class="btn btn-sm btn-info">Generate</a>
                                    <a href="{{route('forms.dynamic_form',['client_id'=>$client->id,'form_id'=>$crf->id,'section_id'=>0])}}" class="btn btn-success btn-sm">Edit</a>
                                    @endif
                            @endif
                                {{--@if(strpos($crf->name, "CRF Form - Online") !== false)
                                    {{ Form::open(['id' => 'documentDelete','method' => 'DELETE','route' => ['crfforms.destroy','id'=>$crf->id,'crf_id' =>$crf->crf_form_id],'style'=>'display:inline']) }}
                                    <a href="#" class="delete deleteDoc btn btn-danger btn-sm">Delete</a>
                                    {{Form::close() }}
                                @else
                                @endif--}}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No forms match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="modal fade" id="modalSignCRF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <div class="row">
                                <div class="md-form col-sm-12">
                                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Kindly confirm by entering your password.</label>
                                    <input type="hidden" id="defaultForm-form_id" class="form-control form-control-sm validate crf_form_id">
                                    <input type="hidden" id="defaultForm-client_id" class="form-control form-control-sm validate crf_client_id">
                                    <input type="password" id="defaultForm-pass" class="form-control form-control-sm validate crf_pass">
                                </div>
                                <div class="md-form mb-4 col-sm-12">
                                    <div class="sign_error_message invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="row text-center col-sm-12">
                                <button class="btn btn-sm btn-default" id="signcrf">Sign</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="cancelsigncrf">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalSignForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body mx-3">
                            <div class="row">
                                <div class="md-form col-sm-12">
                                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Kindly confirm by entering your password.</label>
                                    <input type="hidden" id="defaultForm-form_id" class="form-control form-control-sm validate form_form_id">
                                    <input type="hidden" id="defaultForm-client_id" class="form-control form-control-sm validate form_client_id">
                                    <input type="password" id="defaultForm-password" class="form-control form-control-sm validate form_pass">
                                </div>
                                <div class="md-form mb-4 col-sm-12">
                                    <div class="sign_error_message invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="row text-center col-sm-12">
                                <button class="btn btn-sm btn-default" id="signform">Sign</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="cancelsignform">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $('#modalSignCRF').on('hidden.bs.modal', function () {

            $('#modalAddAction').find('.crf_form_id').val('');
            $('#modalAddAction').find('.crf_client_id').val('');
            $('#modalAddAction').find('.crf_pass').val('');
        })

        $('#modalNewForm').on('hidden.bs.modal', function () {

            $('#modalNewForm').find('.newform_client_id').val('');
            $('#modalNewForm').find('#newform_id').val('0');
        })

        function SignCRF(id,client_id){
            $('#modalSignCRF').modal('show');

            $("#modalSignCRF").find(".crf_form_id").val(id);
            $("#modalSignCRF").find(".crf_client_id").val(client_id);
        }

        function SignForm(id,client_id){
            $('#modalSignForm').modal('show');

            $("#modalSignForm").find(".form_form_id").val(id);
            $("#modalSignForm").find(".form_client_id").val(client_id);
        }

        function newForm(client_id,form_id,section_id){
            window.location.href = '/forms/'+ client_id +'/forms/'+ form_id +'/'+ section_id;
        }

        $('#signcrf').on('click',function (e) {
            var id = $("#modalSignCRF").find(".crf_form_id").val();
            var pwd = $("#modalSignCRF").find("#defaultForm-pass").val();

            if(pwd === '') {
                $("#modalSignCRF").find(".sign_error_message").html('The password you entered is incorrect.');
                $("#modalSignCRF").find(".invalid-feedback").css('display','block');
            } else {

                $.ajax({
                    url: '/check_password/' + pwd,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if (data.result !== 1) {
                            $("#modalSignCRF").find(".sign_error_message").html('The password you entered is incorrect.');
                            $("#modalSignCRF").find(".invalid-feedback").css('display','block');
                            return false

                        } else {
                            $("#modalSignCRF").find(".invalid-feedback").css('display','none');
                            $("#modalSignCRF").modal('hide');
                            $('#crf_form'+id).submit();

                        }
                    }
                });
            }


        })

        $('#signform').on('click',function (e) {
            var id = $("#modalSignForm").find(".form_form_id").val();
            var pwd = $("#modalSignForm").find("#defaultForm-password").val();

            if(pwd === '') {
                $("#modalSignForm").find(".sign_error_message").html('The password you entered is incorrect.');
                $("#modalSignForm").find(".invalid-feedback").css('display','block');
            } else {

                $.ajax({
                    url: '/check_password/' + pwd,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        if (data.result !== 1) {
                            $("#modalSignForm").find(".sign_error_message").html('The password you entered is incorrect.');
                            $("#modalSignForm").find(".invalid-feedback").css('display','block');
                            return false

                        } else {
                            $("#modalSignForm").find(".invalid-feedback").css('display','none');
                            $("#modalSignForm").modal('hide');
                            $('#form_form'+id).submit();

                        }
                    }
                });
            }


        })

        $("#cancelsigncrf").on('click',function (e){
            e.preventDefault();
            $("#modalSignCRF").modal('hide');

        })

        $("#cancelsignform").on('click',function (e){
            e.preventDefault();
            $("#modalSignForm").modal('hide');

        })
    </script>
@endsection