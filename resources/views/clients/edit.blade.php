@extends('adminlte.default')

@section('title') Update Client @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('clients.show',$client)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <hr />

    {{Form::open(['url' => route('clients.update', $client), 'method' => 'put'])}}

    <div class="form-group mt-3">
        {{Form::label('process', 'Process')}}
        {{Form::select('process',$processes,$client->process_id,['class'=>'form-control form-control-sm'. ($errors->has('process') ? ' is-invalid' : ''),'autofocus','id'=>'process'])}}
        @foreach($errors->get('process') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    {{--<div class="form-group">
        {{Form::label('first_name', 'First Name')}}
        {{Form::text('first_name',$client->first_name,['class'=>'form-control'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
        @foreach($errors->get('first_name') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('last_name', 'Last Name')}}
        {{Form::text('last_name',$client->last_name,['class'=>'form-control'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
        @foreach($errors->get('last_name') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>--}}
    <div class="form-group">
        {{Form::label('company', 'Client Name')}}
        {{Form::text('company',$client->company,['class'=>'form-control'. ($errors->has('company') ? ' is-invalid' : ''),'placeholder'=>'Company name'])}}
        @foreach($errors->get('company') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    {{--<div class="form-group">
        {{Form::label('email', 'Email')}}
        {{Form::email('email',$client->email,['class'=>'form-control'. ($errors->has('email') ? ' is-invalid' : ''), 'placeholder'=>'Email'])}}
        @foreach($errors->get('email') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('contact', 'Contact Number')}}
        {{Form::text('contact',$client->contact,['class'=>'form-control'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
        @foreach($errors->get('contact') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

    <div class="form-group">
        {{Form::label('referrer', 'Referrer')}}
        {{Form::select('referrer',$referrers,$client->referrer_id,['class'=>'form-control'. ($errors->has('referrer') ? ' is-invalid' : ''),'placeholder'=>'N/A'])}}
        @foreach($errors->get('referrer') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>--}}
        {{--<div class="form-group">
            {{Form::label('portfolio_name', 'Portfolio Name')}}
            {{Form::text('portfolio_name',$client->portfolio_name,['class'=>'form-control form-control-sm'. ($errors->has('portfolio_name') ? ' is-invalid' : ''),'placeholder'=>'Portfolio Name'])}}
            @foreach($errors->get('portfolio_name') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('portfolio_manager', 'Portfolio Manager')}}
            {{Form::text('portfolio_manager',$client->portfolio_manager,['class'=>'form-control form-control-sm'. ($errors->has('portfolio_manager') ? ' is-invalid' : ''),'placeholder'=>'Portfolio Manager'])}}
            @foreach($errors->get('portfolio_manager') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('project_name', 'Project Name')}}
            {{Form::text('project_name',$client->project_name,['class'=>'form-control form-control-sm'. ($errors->has('project_name') ? ' is-invalid' : ''),'placeholder'=>'Project Name'])}}
            @foreach($errors->get('project_name') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('date_last_updated', 'Date Last Updated')}}
            <input name="date_last_updated" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{Carbon\Carbon::parse($client->date_last_updated)->format('Y-m-d')}}" class="form-control form-control-sm {{($errors->has('date_last_updated') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
            @foreach($errors->get('date_last_updated') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('programme', 'Programme (if applicable)')}}
            {{Form::text('programme',$client->programme,['class'=>'form-control form-control-sm'. ($errors->has('programme') ? ' is-invalid' : ''),'placeholder'=>'Programme'])}}
            @foreach($errors->get('programme') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('programme_manager', 'Programme Manager (if applicable)')}}
            {{Form::text('programme_manager',$client->programme_manager,['class'=>'form-control form-control-sm'. ($errors->has('programme_manager') ? ' is-invalid' : ''),'placeholder'=>'Programme Manager'])}}
            @foreach($errors->get('programme_manager') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('project_manager', 'Project Manager')}}
            {{Form::text('project_manager',$client->project_manager,['class'=>'form-control form-control-sm'. ($errors->has('project_manager') ? ' is-invalid' : ''),'placeholder'=>'Project Manager'])}}
            @foreach($errors->get('project_manager') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('project_administrator', 'Project Manager')}}
            {{Form::text('project_administrator',$client->project_administrator,['class'=>'form-control form-control-sm'. ($errors->has('project_administrator') ? ' is-invalid' : ''),'placeholder'=>'Project Administrator'])}}
            @foreach($errors->get('project_administrator') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>

        <div class="form-group">
            {{Form::label('project_budget', 'Project Budget')}}
            {{Form::text('project_budget',$client->project_budget,['class'=>'form-control form-control-sm'. ($errors->has('project_budget') ? ' is-invalid' : ''),'placeholder'=>'Project Budget'])}}
            @foreach($errors->get('project_budget') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('project_executive', 'Project Executive')}}
            {{Form::text('project_executive',$client->project_executive,['class'=>'form-control form-control-sm'. ($errors->has('project_executive') ? ' is-invalid' : ''),'placeholder'=>'Project Executive'])}}
            @foreach($errors->get('project_executive') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('project_start_date', 'Project Start Date')}}
            <input name="project_start_date" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{Carbon\Carbon::parse($client->project_start_date)->format('Y-m-d')}}" class="form-control form-control-sm {{($errors->has('project_start_date') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
            @foreach($errors->get('project_start_date') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('proposed_end_date', 'Proposed End Date')}}
            <input name="proposed_end_date" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{Carbon\Carbon::parse($client->proposed_end_date)->format('Y-m-d')}}" class="form-control form-control-sm {{($errors->has('proposed_end_date') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
            @foreach($errors->get('proposed_end_date') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>--}}

    <div class="form-group">
        <button type="submit" class="btn btn-sm update-client">Update</button>
    </div>

    {{Form::close()}}
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function(){
            $('#process').on('change',function(){
                $.ajax({
                    dataType: 'json',
                    url: '/processes/step_count/'+$('#process').val(),
                    type:'GET'
                }).done(function(data) {
                    if(data === 0){
                        let process = $('#process').val();
                        $('#modalProcess').modal('show');
                        $('#modalProcess').find('.message').html('The process you selected currently has no steps.<br /><br />' +
                            'Click <a href="/processes/' + process + '/show">here</a> to add steps to this process now.');
                        $('body').find('.update-client').prop('disabled',true);
                    } else {
                        $('body').find('.update-client').prop('disabled',false);
                    }
                });
            })
        })
    </script>