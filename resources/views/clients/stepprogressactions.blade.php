@extends('clients.show')

@section('tab-content')
    <div class="col">

        <b>
            <div class="row mb-3 text-center">
                <div class="col"><i class="fa fa-circle" style="color: {{$client->process->getStageHex(0)}}"></i> Not-started</div>
                <div class="col"><i class="fa fa-circle" style="color: {{$client->process->getStageHex(1)}}"></i> Started</div>
                <div class="col"><i class="fa fa-circle" style="color: {{$client->process->getStageHex(2)}}"></i> Completed</div>
            </div>
        </b>

        {{Form::open(['url' => route('clients.storeactions',$client->id), 'method' => 'post','files'=>true])}}
        <input type="hidden" value="{{$step->process_id}}" name="process_id" id="process_id">
        <input type="hidden" value="{{$step->id}}" name="step_id" id="step_id">

        @foreach($process_progress as $key => $step)
            <div id="step_header_{{$step['id']}}" class="p-2">
                {{--<div id="step_header_{{$step['id']}}" class="p-2" style="background-color: {{$client_progress}}">--}}
                <h3 id="{{$step['order']}}" class="d-inline">
                    {{$step['name']}}
                </h3>
            </div>
            @foreach($step['activities'] as $activity)
                @if(isset($activities[$activity['id']]))
                <div id="list_{{$activity['id']}}" class="list-group-item activity" style="display:table;width:100%;">
                    <div style="display:table-cell;width:20px;/*vertical-align:middle*/"><i class="fa fa-circle" style="color: {{$client->process->getStageHex($activity['stage'])}}"></i> </div>
                    <div style="display:table-cell">

                        {{--<div id="list_{{$activity['id']}}" class="list-group-item" style="background-color: {{$client->process->getStageHex($activity['stage'])}}">--}}
                        {{-- <pre>
                             @php var_dump($activity); @endphp
                     </pre>--}}
                        @if($activity['type'] == 'dropdown')
                            @php

                                $arr = (array)$activity['dropdown_items'];
                                $arr2 = (array)$activity['dropdown_values'];

                            @endphp
                            <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                        @elseif($activity['type'] == 'notification')
                            @php

                                $narr = (array)$activity['notification_items'];
                                $narr2 = (array)$activity['notification_values'];

                            @endphp
                            <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($activity['id']))}}">
                        @elseif($activity['type'] == 'amount')
                            <input type="hidden" id="old_currency_{{$activity['id']}}" name="old_currency_{{$activity['id']}}" value="{{(isset($activity['currency']) ? $activity['currency'] : old($activity['id']))}}">
                            <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}">
                        @else
                            <input type="hidden" id="old_{{$activity['id']}}" name="old_{{$activity['id']}}" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}">
                        @endif
                        {{$activity['name']}}

                        <small class="text-muted"> [{{$activity['type_display']}}] @if($activity['kpi']==1) <i class="fa fa-asterisk" title="Activity is required for step completion" style="color:#FF0000"></i> @endif</small>

                        <div style="float: right; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;">

                        @if(isset($activity['comment']) && $activity['comment'] == 1)
                            <div style="float: right; display: inline-block;margin-top: -3px;margin-right:5px;padding-bottom: 3px;text-align: right;" class="form-inline">
                                {{--<span style="display: none;padding-right: 10px;" class="comment_block_{{$activity['id']}}">
                                <small class="text-muted">Comment:</small> <input type="text" name="{{$activity['id']}}_comment" class="form-control form-control-sm">&nbsp;&nbsp;
                                <small class="text-muted">Private</small> <input type="checkbox">
                                </span>--}}
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item dropdown">
                                        @if(isset($activity_comment[$activity['id']]))

                                            <a href="javascript:void(0)"  data-toggle="dropdown" style="display: inline-block;"><i class="far fa-comment-alt" id="comment_count_fa_{{ $activity["id"] }}" {{(isset($activity_comment[$activity['id']]) ? 'style=color:rgba(50,193,75,.7)' : '')}}></i>&nbsp;<span class="badge badge-pill badge-success" id="comment_count_{{ $activity["id"] }}">{{(isset($activity_comment[$activity['id']]) ? $activity_comment[$activity['id']] : '')}}</span><input type="hidden" id="old_comment_count_{{$activity["id"]}}" value="{{$activity_comment[$activity['id']]}}" /></a>
                                        @else

                                            <a href="javascript:void(0)"  data-toggle="dropdown" style="display: inline-block;"><i class="far fa-comment-alt" id="comment_count_fa_{{ $activity["id"] }}"></i>&nbsp;<span class="badge badge-pill badge-dark" id="comment_count_{{ $activity["id"] }}">0</span><input type="hidden" id="old_comment_count_{{$activity["id"]}}" value="0" /></a>
                                        @endif
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <a href="javascript:void(0)" class="dropdown-item" onclick="addComment({{ $client->id }},{{ $activity['id'] }})">
                                                Add Comment
                                            </a>
                                            {{--<div class="dropdown-divider"></div>--}}
                                            <a href="javascript:void(0)" class="dropdown-item" onclick="showComment({{$client->id}},{{$activity['id']}})">
                                                View Comments
                                            </a>

                                        </div>
                                    </li>
                                </ul>

                            </div>
                        @endif
                            @if(isset($activities) && array_key_exists($activity['id'],$activities))
                                @php
                                    $user_string = '';

                                    foreach ($activities[$activity["id"]]["user"] as $user){
                                        //foreach ($user as $value){
                                            $user_string = $user_string.$user.'<br />';
                                        //}
                                    }
                                @endphp

                                <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a href="javascript:void(0)" style="display: inline-block;" data-toggle="tooltip" data-html="true" title="@php echo $user_string; @endphp"><i class="far fa-user"></i> </a>
                                        </li>

                                    </ul>

                                </div>
                            @endif

                        </div>
                        <div class="clearfix"></div>

                        @if($activity['type']=='date')
                            <input name="{{$activity['id']}}" type="date" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control form-control-sm" placeholder="Insert date..."/>
                        @endif

                        @if($activity['type']=='text')
                            {{Form::text($activity['id'],(isset($activity['value']) ? $activity['value'] : old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...'])}}
                        @endif

                        @if($activity['type']=='textarea')
                            <div><textarea rows="5" name="{{$activity['id']}}" class="form-control form-control-sm text-area">{{(isset($activity['value']) ? $activity['value'] : '')}}</textarea></div>
                        @endif

                        @if($activity['type']=='amount')
                            <div class="input-group" style="width:99%">
                                {{Form::select('currency_'.$activity['id'],$currency_dropdown,($activity["currency"] != null ? $activity["currency"] : $default_currency),['class'=>'form-control form-control-sm','style'=>'float: left;flex:unset;width:auto;'])}}
                                {{Form::text($activity['id'],(isset($activity['value']) ? $activity['value'] : old($activity['id'])),['class'=>'form-control form-control-sm','placeholder'=>'Insert amount...','style'=>'float: left;width: initial'])}}
                            </div>
                        @endif

                        @if($activity['type']=='boolean')
                            {{Form::select($activity['id'],[1=>'Yes',0=>'No'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                        @endif

                        @if($activity['type']=='template_email')
                            <div class="row">
                                <div class="col-md-12 input-group">
                                    {{Form::select($activity['id'],$templates,(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                                    <div class="input-group-append" onclick="viewTemplate({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm">View Template</button>
                                    </div>
                                </div>
                                <div class="col-md-12 input-group form-group" style="margin-top: 10px !important; margin-bottom: 10px !important;">
                                    {{Form::select('template_email_'.$activity['id'],$template_email_options,null,['id'=>'template_email_'.$activity['id'],'onChange'=>'getSubject('.$activity['id'].')','class'=>'form-control form-control-sm'. ($errors->has('template_email_'.$activity['id']) ? ' is-invalid' : ''), 'placeholder'=>'Select Tempate Email...'])}}
                                    <div class="input-group-append" onclick="viewEmailTemplate({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm" data-toggle="modal" data-target="edit_email_template">View Email Template</button>
                                    </div>
                                    <div id="etemplate_message_{{$activity['id']}}"></div>
                                </div>
                                <div class="col-md-12 input-group" style="margin-bottom: 10px !important;">
                                    {{Form::text('subject_'.$activity['id'],old('subject_'.$activity['id']),['class'=>'form-control form-control-sm','style'=>'width:100%','placeholder'=>'Insert email subject...'])}}
                                    <div id="subject_message_{{$activity['id']}}"></div>
                                </div>
                                <div class="col-md-12 input-group">
                                    {{Form::text($activity['id'],(isset($client->email) ? $client->email : old($client->email)),['class'=>'form-control form-control-sm','placeholder'=>'Insert email...'])}}
                                    <div class="input-group-append" onclick="submitTemplate({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm">Send Template</button>
                                    </div>
                                </div>
                            </div>
                            <div id="message_{{$activity['id']}}"></div>
                        @endif

                        @if($activity['type']=='document_email')
                            <div class="row">
                                <div class="col-md-12 input-group form-group">
                                    {{Form::select($activity['id'],$documents,(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                                    <div class="input-group-append" onclick="viewDocument({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm">View Document</button>
                                    </div>
                                </div>
                                <div class="col-md-12 input-group form-group" style="margin-bottom: 10px !important;">
                                    {{Form::select('template_email_'.$activity['id'],$template_email_options,null,['id'=>'template_email_'.$activity['id'],'onChange'=>'getSubject('.$activity['id'].')','class'=>'form-control form-control-sm'. ($errors->has('documents_'.$activity['id']) ? ' is-invalid' : ''), 'placeholder'=>'Select Tempate Email...'])}}
                                    <div class="input-group-append" onclick="viewEmailTemplate({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm" data-toggle="modal" data-target="edit_email_template">View Email Template</button>
                                    </div>
                                    <div id="etemplate_message_{{$activity['id']}}"></div>
                                </div>
                                <div class="col-md-12 input-group" style="margin-bottom: 10px !important;">
                                    {{Form::text('subject_'.$activity['id'],old('subject_'.$activity['id']),['class'=>'form-control form-control-sm','style'=>'width:100%','placeholder'=>'Insert email subject...'])}}
                                    <div id="subject_message_{{$activity['id']}}"></div>
                                </div>
                                <div class="col-md-12 input-group">
                                    {{Form::text($activity['id'],(isset($client->email) ? $client->email : old($client->email)),['class'=>'form-control form-control-sm','placeholder'=>'Insert email...'])}}
                                    <div class="input-group-append" onclick="submitDocument({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm">Send Document</button>
                                    </div>
                                </div>
                            </div>
                            <div id="message_{{$activity['id']}}"></div>
                        @endif

                        @if($activity['type']=='multiple_attachment')
                            <div class="row">
                                <div class="col-md-12">
                                    <small class="form-text text-muted">
                                        Search and select multiple entries
                                    </small>
                                </div>
                                <div class="col-md-6 input-group form-group" style="margin-bottom: 0px !important;">
                                    {{Form::select('templates_'.$activity['id'],$templates,null,['id'=>'templates_'.$activity['id'],'class'=>'form-control form-control-sm chosen-select'. ($errors->has('templates_'.$activity['id']) ? ' is-invalid' : ''),'multiple'])}}
                                    @foreach($errors->get('templates_'.$activity['id']) as $error)
                                        <div class="invalid-feedback">
                                            {{ $error }}
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6 input-group form-group" style="margin-bottom: 0px !important;">
                                    {{Form::select('documents_'.$activity['id'],$document_options,null,['id'=>'documents_'.$activity['id'],'class'=>'form-control form-control-sm chosen-select'. ($errors->has('documents_'.$activity['id']) ? ' is-invalid' : ''),'multiple'])}}
                                    @foreach($errors->get('documents_'.$activity['id']) as $error)
                                        <div class="invalid-feedback">
                                            {{ $error }}
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-6">
                                    <small id="templates_help" class="form-text text-muted">Templates</small>
                                </div>
                                <div class="col-md-6">
                                    <small id="documents_help" class="form-text text-muted">Documents</small>
                                </div>

                                {{-- Todo: Add functionality for EmailTemplate(Pop up)--}}
                                <div class="col-md-12 input-group form-group" style="margin-top: 10px !important; margin-bottom: 10px !important;">
                                    {{Form::select('template_email_'.$activity['id'],$template_email_options,null,['id'=>'template_email_'.$activity['id'],'onChange'=>'getSubject('.$activity['id'].')','class'=>'form-control form-control-sm'. ($errors->has('documents_'.$activity['id']) ? ' is-invalid' : ''), 'placeholder'=>'Select Template Email...'])}}
                                    <div class="input-group-append" onclick="viewEmailTemplate({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm" data-toggle="modal" data-target="edit_email_template">View Email Template</button>
                                    </div>
                                    <div id="etemplate_message_{{$activity['id']}}" class="col-md-12"></div>
                                </div>
                                <div class="col-md-12 input-group" style="margin-bottom: 10px !important;">
                                    {{Form::text('subject_'.$activity['id'],old('subject_'.$activity['id']),['class'=>'col-md-12 form-control form-control-sm','style'=>'width:100%','placeholder'=>'Insert email subject...'])}}
                                    <div id="subject_message_{{$activity['id']}}"></div>
                                </div>
                                <div class="col-md-12 input-group">
                                    {{Form::text($activity['id'],(isset($client->email) ? $client->email : old($client->email)),['class'=>'form-control form-control-sm','placeholder'=>'Insert email...'])}}
                                    <div class="input-group-append" onclick="sendMultipleDocuments({{$activity['id']}})">
                                        <button type="button" class="btn btn-multiple btn-sm">Send Template</button>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <small id="documents_help" class="form-text text-muted"><i class="fa fa-info-circle"></i> Use a comma to seperate multiple email addresses.</small>
                                </div>
                            </div>
                            <div id="message_{{$activity['id']}}"></div>
                        @endif

                        @if($activity['type']=='document')
                            {{Form::file($activity['id'],['class'=>'form-control form-control-sm'. ($errors->has($activity['id']) ? ' is-invalid' : ''),'placeholder'=>'File'])}}
                            @foreach($errors->get($activity['id']) as $error)
                                <div class="invalid-feedback">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        @if($activity['type']=='dropdown')

                            {{-- Form::select($activity['id'],$activity['dropdown_items'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...']) --}}
                            <select multiple="multiple" id="{{$activity['id']}}" name="{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                                @php
                                    foreach((array) $arr as $key => $value){
                                        echo '<option value="'.$key.'" '.(in_array($key,$arr2) ? 'selected' : '').'>'.$value.'</option>';
                                    }
                                @endphp
                            </select>
                            <div>
                                <small class="form-text text-muted">
                                    Search and select multiple entries
                                </small>
                            </div>
                            {{--{{Form::select($activity['id'],$activity['dropdown_items'],null,['id'=>$activity['id'],'class'=>'form-control'. ($errors->has($activity['id']) ? ' is-invalid' : ''),'multiple'])}}--}}
                            @foreach($errors->get($activity['id']) as $error)
                                <div class="invalid-feedback">
                                    {{ $error }}
                                </div>
                            @endforeach

                        @endif

                        @if($activity['type']=='notification')
                            <div class="row">
                                <select multiple="multiple" id="notification_user_name_{{$activity['id']}}" name="notification_user_name_{{$activity["id"]}}[]" class="form-control form-control-sm chosen-select">
                                    @php
                                        foreach((array) $narr as $value){
                                            echo '<option value="'.$value["id"].'" '.(in_array($key,$narr2) ? 'selected' : '').'>'.$value["name"].'</option>';
                                        }
                                    @endphp
                                </select>
                                <div>
                                    <small class="form-text text-muted">
                                        Search and select multiple entries
                                    </small>
                                </div>
                                <div class="col-sm-12" style="margin-top: 10px !important;">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="sendNotification({{$activity['id']}})"><i class="fa fa-paper-plane"></i> Send notification</button>
                                </div>
                            </div>
                            <div id="message_{{$activity['id']}}"></div>
                        @endif
                    </div>

                </div>
                @endif
            @endforeach
        @endforeach

        @if(count($process_progress)>0)

            <div class="blackboard-complete-btn mr-3 mb-3">
                @if(isset($client->completed_at) && $client->completed_at != null)
                    <a href="javascript:void(0)" onclick="uncomplete({{ $client->id }},{{ $active->id }})" class="uncomplete-btn btn btn-info btn-lg" {{($active->id == $max_step ? 'style=display:block' : 'style=display:none')}}><i class="fa fa-hourglass-end"></i>Unconvert</a>
                    <a href="javascript:void(0)" onclick="complete({{ $client->id }},{{ $active->id }})" class="complete-btn btn btn-info btn-lg" {{($active->id == $max_step ? 'style=display:none' : 'style=display:block')}}><i class="fa fa-hourglass-end"></i>Convert</a>
                @else
                    <a href="javascript:void(0)" onclick="uncomplete({{ $client->id }},{{ $active->id }})" class="uncomplete-btn btn btn-info btn-lg" {{($active->id == $max_step ? 'style=display:none' : 'style=display:none')}}><i class="fa fa-hourglass-end"></i>Unconvert</a>
                    <a href="javascript:void(0)" onclick="complete({{ $client->id }},{{ $active->id }})" class="complete-btn btn btn-info btn-lg" {{($active->id == $max_step ? 'style=display:block' : 'style=display:none')}}><i class="fa fa-hourglass-end"></i>Convert</a>
                @endif
            </div>

            <div class="blackboard-fab mr-3 mb-3">
                <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-save"></i> Save</button>
            </div>
        @endif

        {{Form::close()}}

        <div class="modal fade" id="modalUnconvert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="clientid" id="unconvertclientid" />
                        <input type="hidden" name="activeid" id="unconvertactiveid" />
                        <div class="row step1">
                            <div class="md-form col-sm-12 pb-3 text-center">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Please select an action you would like to perform.</label>
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-center">
                                <button class="btn btn-sm btn-default" id="changeconvertdate">Change converted date</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="unconvert">Unconvert</button>
                            </div>
                        </div>
                        <div class="row step2" style="display: none">
                            <div class="md-form col-sm-12 mb-3">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Please enter the new convertion date.</label>
                                <input type="date" id="newconvertdate" class="form-control form-control-sm validate">
                            </div>
                            <div class="md-form mb-4 col-sm-12">
                                <button class="btn btn-sm btn-default" id="changeconvertdatesave">Save</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="changeconvertdatecancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalConvert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="clientid" id="convertclientid" />
                        <input type="hidden" name="activeid" id="convertactiveid" />
                        <div class="row">
                            <div class="md-form col-sm-12 mb-3 text-center">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Please enter the convert date.</label>
                                <input type="date" id="convertdate" class="form-control form-control-sm validate">
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-center">
                                <button class="btn btn-sm btn-default" id="convertdatesave">Save</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="convertdatecancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalAddComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="clientid" id="addcommentclientid" />
                        <input type="hidden" name="activityid" id="addcommentactivityid" />
                        <div class="row">
                            <div class="md-form col-sm-12 mb-3 text-left">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Add a Comment</label>
                                <textarea id="addcommentcomment" class="form-control form-control-sm my-editor2"></textarea>
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-left">
                                <input type="checkbox" name="privatec" id="addcommentprivatec" /> Private Comment
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-center">
                                <button class="btn btn-sm btn-default" id="addcommentsave">Save</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="addcommentcancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalEditComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="clientid" id="editcommentclientid" />
                        <input type="hidden" name="activityid" id="editcommentactivityid" />
                        <input type="hidden" name="activityid" id="editcommentcommentid" />
                        <div class="row">
                            <div class="md-form col-sm-12 mb-3 text-left">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Edit Comment</label>
                                <textarea id="editcommentcomment" class="form-control form-control-sm my-editor2"></textarea>
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-left">
                                <input type="checkbox" name="privatec" id="editcommentprivatec" /> Private Comment
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-center">
                                <button class="btn btn-sm btn-default" id="editcommentsave">Save</button>&nbsp;
                                <button class="btn btn-sm btn-default" id="editcommentcancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalShowComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width:800px !important;max-width:800px;">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <input type="hidden" name="clientid" id="showcommentclientid" />
                        <input type="hidden" name="activityid" id="showcommentactivityid" />
                        <div class="row">
                            <div class="md-form col-sm-12 mb-3 text-left">
                                <label data-error="wrong" data-success="right" for="defaultForm-pass">Comments:</label>
                                <div id="showcommentcomment"></div>
                            </div>
                            <div class="md-form mb-4 col-sm-12 text-center">
                                <button class="btn btn-sm btn-default" id="showcommentcancel">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <small class="text-muted"> Using process for: <b>{{$client->process->office->name}}</b> last updated: <b>{{$client->process->updated_at->diffForHumans()}}</b></small>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
            min-width: 500px;
        }

        .chosen-container, .chosen-container-multi{
            width:100% !important;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }
    </style>
@endsection
@section('extra-js')

    <script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
    <script>
        $(document).ready(function (){
                $('#changeconvertdate').on('click',function(){
                    $("#modalUnconvert .step1").css('display','none');
                    $("#modalUnconvert .step2").css('display','block');
                });

                $('#changeconvertdatecancel').on('click',function(){
                    $("#modalUnconvert .step2").css('display','none');
                    $("#modalUnconvert .step1").css('display','block');
                    $("#modalUnconvert").modal('hide');
                });

                $('#modalConvert #convertdatecancel').on('click',function(){
                    $("#modalConvert").modal('hide');
                });

                $('#modalUnconvert #unconvert').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var clientid = $("#modalUnconvert").find("#unconvertclientid").val();
                    var activeid = $("#modalUnconvert").find("#unconvertactiveid").val();
                    $.ajax({
                        url: '/clients/' + clientid + '/uncompleted/'+ activeid,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $("#modalUnconvert .step2").css('display', 'none');
                            $("#modalUnconvert .step1").css('display', 'block');
                            $("#modalUnconvert").modal('hide');

                            $(".uncomplete-btn").css('display','none');
                            $(".complete-btn").css('display','block');
                            $('.flash_msg').html('<div class="alert alert-success alert-dismissible blackboard-alert">\n' +
                                '                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>\n' +
                                '                    <strong>Success!</strong> Client successfully unconverted.\n' +
                                '                </div>');
                        }
                    });


                });

                $('#changeconvertdatesave').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var clientid = $("#modalUnconvert").find("#unconvertclientid").val();
                    var activeid = $("#modalUnconvert").find("#unconvertactiveid").val();
                    var newdate = $("#modalUnconvert").find("#newconvertdate").val();

                    if(newdate === ''){
                        $("#modalUnconvert").find("#newconvertdate").addClass('is-invalid');
                    } else {
                        $("#modalUnconvert").find("#newconvertdate").removeClass('is-invalid');

                        $.ajax({
                            url: '/clients/' + clientid + '/changecompleted/' + activeid + '/' + newdate,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $("#modalUnconvert .step2").css('display', 'none');
                                $("#modalUnconvert .step1").css('display', 'block');
                                $("#modalUnconvert").modal('hide');

                                $(".uncomplete-btn").css('display','block');
                                $(".complete-btn").css('display','none');
                                $('.flash_msg').html('<div class="alert alert-success alert-dismissible blackboard-alert">\n' +
                                    '                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>\n' +
                                    '                    <strong>Success!</strong> Converted date successfully changed.\n' +
                                    '                </div>');
                            }
                        });
                    }


                });

                $('#convertdatesave').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var clientid = $("#modalConvert").find("#convertclientid").val();
                    var activeid = $("#modalConvert").find("#convertactiveid").val();
                    var newdate = $("#modalConvert").find("#convertdate").val();

                    if(newdate === ''){
                        $("#modalConvert").find("#convertdate").addClass('is-invalid');
                    } else {
                        $("#modalConvert").find("#convertdate").removeClass('is-invalid');

                        $.ajax({
                            url: '/clients/' + clientid + '/completed/' + activeid + '/' + newdate,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                $("#modalConvert").modal('hide');

                                $(".uncomplete-btn").css('display','block');
                                $(".complete-btn").css('display','none');
                                $('.flash_msg').html('<div class="alert alert-success alert-dismissible blackboard-alert">\n' +
                                    '                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>\n' +
                                    '                    <strong>Success!</strong> Client successfully converted\n' +
                                    '                </div>');

                            }
                        });
                    }


                });

                $('#addcommentsave').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var clientid = $("#modalAddComment").find("#addcommentclientid").val();
                    var activityid = $("#modalAddComment").find("#addcommentactivityid").val();
                    var comment = $("#modalAddComment").find("#addcommentcomment").val();
                    var privatec = 0;

                    if($("#modalAddComment").find("#addcommentprivatec").is(':checked')){
                        privatec = 1;
                    }

                    $.ajax({
                        type: "POST",
                        url: '/clients/'+clientid+'/addcomment/'+activityid,
                        data: {clientid: clientid, activityid: activityid, comment: comment,privatec:privatec},
                        success: function( data ) {

                            $("#modalAddComment").modal('hide');
                            tinyMCE.activeEditor.setContent('');
                            $("#modalAddComment").find("#addcommentcomment").val();
                            $("#modalAddComment").find("#addcommentprivatec").prop('checked',false);
                            $("#modalAddComment").find("#addcommentclientid").val(clientid);
                            $("#modalAddComment").find("#addcommentactivityid").val(activityid);

                            let count = parseInt($("#old_comment_count_"+activityid).val());

                            if(count === 0){
                                $("#comment_count_"+activityid).removeClass('badge-dark');
                                $("#comment_count_"+activityid).addClass('badge-success');
                            }
                            count++;
                            $("#comment_count_"+activityid).html(count);
                            $("#old_comment_count_"+activityid).val(count);
                            $("#comment_count_fa_"+activityid).css('color','rgba(50, 193, 75, 0.7)');


                            //window.location.reload();
                        }
                    });
                });

                $('#addcommentcancel').on('click',function(){
                    $("#modalAddComment").modal('hide');
                });

                $('#showcommentcancel').on('click',function(){
                    $("#modalShowComment").find("#showcommentcomment").empty();
                    $("#modalShowComment").modal('hide');
                });

                $('#editcommentcancel').on('click',function(){
                    $("#modalEditComment").find("#editcommentcomment").empty();
                    $("#modalEditComment").modal('hide');
                });

                $('#modalEditComment #editcommentsave').on('click',function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    let clientid = $("#modalEditComment").find("#editcommentclientid").val();
                    let activityid = $("#modalEditComment").find("#editcommentactivityid").val();
                    let commentid = $("#modalEditComment").find("#editcommentcommentid").val();
                    let comment = $("#modalEditComment").find("#editcommentcomment").val();
                    let privatec = 0;
                    if($("#modalEditComment").find("#editcommentprivatec").is(':checked')) {
                        privatec = 1;
                    } else {
                        privatec = 0;
                    }
                    $.ajax({
                        type: "POST",
                        url: '/clients/updatecomment/'+commentid,
                        data: {clientid:clientid,activityid:activityid,commentid: commentid,comment:comment,privatec:privatec},
                        success: function( data ) {
                            $("#modalEditComment").modal('hide');
                            showComment(clientid,activityid);
                        }
                    });


                });

                $('.change-step').on("change",function(){
                    completeStep2($('.change-step').val());
                });
            }
        );

        function addComment(clientid,activityid) {

            $("#modalAddComment").modal('show');
            $("#modalAddComment").find("#addcommentclientid").val(clientid);
            $("#modalAddComment").find("#addcommentactivityid").val(activityid);

            tinymce.init(editor_config2);
        }

        function showComment(clientid,activityid) {

            $("#modalShowComment").modal('show');
            $("#modalShowComment").find("#showcommentcomment").empty();
            $("#modalShowComment").find("#showcommentclientid").val(clientid);
            $("#modalShowComment").find("#showcommentactivityid").val(activityid);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/clients/'+clientid+'/showcomment/'+activityid,
                data: {clientid: clientid, activityid: activityid},
                success: function( data ) {
                    let user = {{auth()->id()}};
                    let rows = '';

                    rows = rows + '<table cellpadding="10" cellspacing="0" border="0" width="100%">';
                    if(Object.keys(data).length === 0){
                        rows = rows + '<tr><td align="center">There are no comments for this activity.</td></tr>';
                    }
                    $.each( data, function( key, value ) {

                        if(user == value.user) {
                            rows = rows + '<tr class="comment_id_' + value.id + '"><td style="width: 30px;border-bottom:1px solid #ced4da;"><a href="/profile/' + value.user + '"><p style="margin-bottom: "><img src="/storage/avatar/?q=' + value.avatar + '" class="blackboard-avatar blackboard-avatar-inline blackboard-avatar-navbar-img" alt="Avatar"/></p></a></td><td style="border-bottom:1px solid #ced4da;">' + value.comment + '</td><td style="text-align:right;width:200px;border-bottom:1px solid #ced4da;"><p>' + value.date + '</p></td><td style="border-bottom:1px solid #ced4da;width:30px;"><p><a href="javascript:void(0)" onclick="editComment(' + value.client_id + ',' + value.activity_id + ',' + value.id + ',' + value.privatec + ')"><i class="fa fa-edit" style="color:#28a745"></i></a> </p></td><td style="border-bottom:1px solid #ced4da;width:30px;"><p><a href="javascript:void(0)" onclick="deleteComment(' + value.id + ',' + value.activity_id + ')"><i class="fa fa-trash-alt" style="color:#dc3545"></i> </a></p></td></tr>';
                        } else {
                            rows = rows + '<tr><td style="width: 30px;border-bottom:1px solid #ced4da;"><a href="/profile/' + value.user + '"><p style="margin-bottom: "><img src="/storage/avatar/?q=' + value.avatar + '" class="blackboard-avatar blackboard-avatar-inline blackboard-avatar-navbar-img" alt="Avatar"/></p></a></td><td style="border-bottom:1px solid #ced4da;">' + value.comment + '</td><td style="text-align:right;width:200px;border-bottom:1px solid #ced4da;"><p>' + value.date + '</p></td><td style="border-bottom:1px solid #ced4da;width:30px;"><p><a href="javascript:void(0)" style="cursor:not-allowed"><i class="fa fa-edit" style="color:#ddd"></i></a> </p></td><td style="border-bottom:1px solid #ced4da;width:30px;"><p><a href="javascript:void(0)" style="cursor: not-allowed"><i class="fa fa-trash-alt" style="color:#ddd"></i> </a></p></td></tr>';
                        }

                    });
                    rows = rows + '</table>';
                    $("#modalShowComment").find("#showcommentcomment").html(rows);
                    $("#showcommentcomment p").css('margin-bottom','0px');
                }
            });
        }

        function editComment(clientid,activityid,commentid,privatec) {
            $("#modalShowComment").modal('hide');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: '/clients/editcomment/'+commentid,
                data: {commentid: commentid},
                success: function( data ) {
                    $("#modalEditComment").modal('show');
                    $.each( data, function( key, value ) {
                        $("#modalEditComment").find("#editcommentcomment").val('');
                        $("#modalEditComment").find("#editcommentclientid").val(clientid);
                        $("#modalEditComment").find("#editcommentactivityid").val(activityid);
                        $("#modalEditComment").find("#editcommentcommentid").val(commentid);
                        $("#modalEditComment").find("#editcommentcomment").val(value.comment);

                        tinymce.init(editor_config2);

                        tinymce.get('editcommentcomment').setContent(value.comment);

                        if (privatec === 1) {
                            $("#modalEditComment").find("#editcommentprivatec").prop('checked', true);
                        } else {
                            $("#modalEditComment").find("#editcommentprivatec").prop('checked', false);
                        }
                    });
                }
            });

        }

        function deleteComment(commentid,activityid) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            if (!confirm("Are you sure you want to delete this record?")){
                return false;
            } else {
                $.ajax({
                    type: "POST",
                    url: '/clients/deletecomment/' + commentid,
                    data: {commentid: commentid},
                    success: function (data) {
                        $("#modalShowComment").find(".comment_id_" + commentid).remove();


                        let count = parseInt($("#old_comment_count_" + activityid).val());


                        count--;
                        if (count === 0) {
                            $("#modalShowComment").find("#showcommentcomment").html('<table cellpadding="10" cellspacing="0" border="0" width="100%"><tr><td align="center">There are no comments for this activity.</td></tr></table>');
                            $("#comment_count_" + activityid).addClass('badge-dark');
                            $("#comment_count_" + activityid).removeClass('badge-success');
                            $("#comment_count_fa_" + activityid).css('color', 'rgba(0,0,0,0.5)');
                        }
                        $("#comment_count_" + activityid).html(count);
                        $("#old_comment_count_" + activityid).val(count);

                    }
                });
            }
        }

        function uncomplete(clientid,activeid){
            $("#modalUnconvert").modal('show');
            $("#modalUnconvert").find("#unconvertclientid").val(clientid);
            $("#modalUnconvert").find("#unconvertactiveid").val(activeid);
            $("#modalUnconvert").find("#newconvertdate").val(activeid);
        }

        function complete(clientid,activeid){
            $("#modalConvert").modal('show');
            $("#modalConvert").find("#convertclientid").val(clientid);
            $("#modalConvert").find("#convertactiveid").val(activeid);
            $("#modalConvert").find("#convertdate").val();
        }

        function viewEmailTemplate(activity_id){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $('#template_email_'+activity_id).val();
            $.ajax({
                url: '/editemail/'+id,
                type:"GET",
                dataType:"json",
                success:function(data){

                    $("#edit_email_template").modal('show');
                    $("#edit_email_template").find("#email_id").val(data.id);
                    $("#edit_email_template").find("#activity_id").val(activity_id);
                    $("#edit_email_template").find("#email_title").val(data.name);
                    $("#edit_email_template").find("#email_content").val(data.email_content);

                    tinymce.init(editor_config);
                }
            });

        };

        function saveEmailTemplate(){
            var id = $("#edit_email_template").find("#email_id").val();
            var name = $("#edit_email_template").find("#email_title").val();
            var email_content = $("#edit_email_template").find("#email_content").val();
            $.ajax({
                url: '/updateemail/'+id,
                type:"POST",
                data:{name:name, email_content:email_content},
                success:function(data){
                    $("#edit_email_template").modal('hide');
                    $("#edit_email_template").find("#email_id").val();
                    $("#edit_email_template").find("#activity_id").val(activity_id);
                    $("#edit_email_template").find("#email_title").val();
                    $("#edit_email_template").find("#email_content").val();
                }
            });
        }

        function getSubject(activity){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var id = $('#template_email_'+activity).val();
            $.ajax({
                url: '/getsubject/'+id,
                type:"POST",
                data:{id:id},
                success:function(data){
                    $("input[name=subject_" + activity + "]").val(data.email_subject);
                }
            });
        }

        function viewTemplate(activity) {

            $('#message_' + activity).html('');

            if ($("select[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select template</span>');
                return;
            }

            var template = $("select[name=" + activity + "]").val();

            window.location.href = "{{ route('clients.viewtemplate', $client)}}/" + template;

        }



        function viewDocument(activity) {

            $('#message_' + activity).html('');

            if ($("select[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select document</span>');
                return;
            }

            var document = $("select[name=" + activity + "]").val();

            window.location.href = "{{ route('clients.viewdocument', $client)}}/" + document;

        }

        function submitTemplate(activity) {

            if ($("select[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select template</span>');
                return;
            }

            if ($("input[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please enter email comma separated to send to more than one recipient</span>');
                return;
            }

            if ($('#template_email_'+activity).val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select an email template.</span>');
                return;
            }

            if ($("input[name=subject_" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please enter a subject.</span>');
                return;
            }

            $('#message_' + activity).html('<span style="color: red;">Sending, please wait ...</span>');
            var data = {
                email: $("input[name=" + activity + "]").val(),
                template_file: $("select[name=" + activity + "]").val(),
                subject: $("input[name=subject_" + activity + "]").val(),
                template_email: $('#template_email_'+activity).val()
            };

            axios.post('{{route('clients.sendtemplate',$client)}}/' + activity, data)
                .then(function (data) {
                    $('#message_' + activity).html('<span style="color: green">Template sent successfully.</span>');
                })
                .catch(function () {
                    $('#message_' + activity).html('<span style="color: red">There was a problem with this request.</span>');
                });
        }

        function submitDocument(activity) {

            if ($("select[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select document</span>');
                return;
            }

            if ($("input[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please enter email comma separated to send to more than one recipient</span>');
                return;
            }

            if ($('#template_email_'+activity).val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select an email template.</span>');
                return;
            }

            if ($("input[name=subject_" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please enter a subject.</span>');
                return;
            }

            $('#message_' + activity).html('<span style="color: red;">Sending, please wait ...</span>');
            var data = {
                email: $("input[name=" + activity + "]").val(),
                document_file: $("select[name=" + activity + "]").val(),
                subject: $("input[name=subject_" + activity + "]").val(),
                template_email: $('#template_email_'+activity).val()
            };

            axios.post('{{route('clients.senddocument',$client)}}/' + activity, data)
                .then(function (data) {
                    $('#message_' + activity).html('<span style="color: green">Document sent successfully.</span>');
                })
                .catch(function () {
                    $('#message_' + activity).html('<span style="color: red">There was a problem with this request.</span>');
                });
        }

        function sendNotification(activity) {
            $('#message_' + activity).html('<span style="color: red;">Sending ...</span>');

            var data = {
                notification_user: $('#notification_user_name_'+activity).val()
            }

            axios.post('{{route('clients.sendnotification',$client)}}/' + activity, data)
                .then(function (data) {
                    $('#message_' + activity).html('<span style="color: green">Notifications sent successfully.</span>');
                })
                .catch(function () {
                    $('#message_' + activity).html('<span style="color: red">There was a problem with this request.</span>');
                });
        }

        function completeStep(step) {
            $('#step' + step).html('<span style="color: red;">Updating ...</span>');

            axios.post('{{route('clients.completestep',$client).'/'}}' + $('#process_id').val() + '/' + step)
                .then(function (data) {
                    var auto_completed_values = data["data"].activities_auto_completed;
                    for(i = 0; i< auto_completed_values.length; i++){
                        $("#step_header_"+step).css("background-color", '#3CFF463f');
                        $("#list_"+auto_completed_values[i]).css("background-color", '#3CFF463f');
                    }
                    $('#step_' + step).html('<span style="color: green">Done &nbsp;</span>');
                    //location.reload();
                    window.location.href = '{{ route('clients.progress', $client).'/'}}' + $('#process_id').val() + '{{'/'.$next_step}}';
                })
                .catch(function () {
                    $('#step_' + step).html('<span style="color: red">Error &nbsp;</span>');
                });
        }

        function completeStep2(step) {
            var r = confirm("Are you sure you want to move this client?");
            if (r == true) {
                axios.post('{{route('clients.completestep2',$client).'/'}}' + $('#process_id').val() + '/' + step)
                    .then(function (data) {
                        window.location.href = '{{ route('clients.progress', $client).'/'}}' + $('#process_id').val() + '/' + step;
                    })
                    .catch(function () {
                        $('#step_' + step).html('<span style="color: red">Error &nbsp;</span>');
                    });
            }

        }

        function sendMultipleDocuments(activity){


            if ($('#templates_'+activity).val() == "" && $('#documents_'+activity).val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please select at least one template or document</span>');
                return;
            } else {
                $('#message_' + activity).html('');
            }

            if ($("input[name=" + activity + "]").val() == "") {
                $('#message_' + activity).html('<span style="color: red;">Please enter email comma separated to send to more than one recipient</span>');
                return;
            } else {
                $('#message_' + activity).html('');
            }

            if ($('#template_email_'+activity).val() == "") {
                $('#etemplate_message_' + activity).html('<span style="color: red;">Please select an email template.</span>');
                return;
            } else {
                $('#etemplate_message_' + activity).html('');
            }

            if ($("input[name=subject_" + activity + "]").val() == "") {
                $('#subject_message_' + activity).html('<span style="color: red;">Please enter a subject.</span>');
                return;
            } else {
                $('#subject_message_' + activity).html('');
            }

            $('#message_' + activity).html('<span style="color: red;">Sending ...</span>');

            var data = {
                templates: $('#templates_'+activity).val(),
                documents: $('#documents_'+activity).val(),
                email: $("input[name=" + activity + "]").val(),
                subject: $("input[name=subject_" + activity + "]").val(),
                template_email: $('#template_email_'+activity).val()
            }


            axios.post('{{route('clients.senddocuments',$client)}}/' + activity, data)
                .then(function (data) {
                    console.log(data);
                    $('#message_' + activity).html('<span style="color: green">Documents sent successfully.</span>');
                })
                .catch(function () {
                    $('#message_' + activity).html('<span style="color: red">There was a problem with this request.</span>');
                });
        }


    </script>
    <script>
        var editor_config = {
            path_absolute : "/",
            branding: false,
            relative_urls: false,
            convert_urls : false,
            paste_data_images: true,
            selector: "textarea.my-editor",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",

            external_filemanager_path:"{{url('tinymce/filemanager')}}/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "{{url('tinymce')}}/filemanager/plugin.min.js"}
        };

        var editor_config2 = {
            path_absolute : "/",
            branding: false,
            relative_urls: false,
            convert_urls : false,
            paste_data_images: true,
            menubar: false,
            selector: "textarea.my-editor2",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",

            external_filemanager_path:"{{url('tinymce/filemanager')}}/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "{{url('tinymce')}}/filemanager/plugin.min.js"}
        };

    </script>
@endsection