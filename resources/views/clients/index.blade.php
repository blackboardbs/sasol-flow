@extends('adminlte.default')

@section('title') Clients @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('clients.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> Client</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <form class="form-inline mt-3" id="clientform">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','mine'=>'My','country'=>'Country wide','region'=>'Region wide','office'=>'Office wide','deleted'=>'Deleted'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; for &nbsp;
        {{Form::select('p',$processes ,old('p'),['class'=>'form-control form-control-sm','id'=>'p'])}}
        &nbsp; for &nbsp;
        {{Form::select('c',['all'=>'All completions','no'=>'Not converted','yes'=>'Converted','progress'=>'Progressing','noprogress'=>'Not progressing','approval'=>'Needs approval'],old('c'),['class'=>'form-control form-control-sm'])}}
        &nbsp; in &nbsp;
        {{Form::select('step',[] ,old('step'),['class'=>'form-control form-control-sm','id'=>'step'])}}
        &nbsp; with &nbsp;
        {{Form::select('rl',$relationship_lead ,old('rl'),['class'=>'form-control form-control-sm','id'=>'rl'])}}
        &nbsp; from &nbsp;
        {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
        &nbsp; to &nbsp;
        {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('query'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
        <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
        <a href="{{route('clients.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-hover table-sm">
            <thead class="btn-dark">
            <tr>
                <th>@sortablelink('company', 'Client Name')</th>{{--
                <th>@sortablelink('portfolio_manager', 'Portfolio Manager')</th>
                <th>@sortablelink('project_name', 'Project Name')</th>
                <th>@sortablelink('project_start_date', 'Project Start Date')</th>
                <th>@sortablelink('proposed_end_date', 'ProposedEnd Date')</th>--}}
                {{--<th>@sortablelink('referrer','Referrer')</th>
                <th>@sortablelink('director','Relationship Lead')</th>
                <th>@sortablelink('onboardingl','Onboarding Lead')</th>
                <th>@sortablelink('onboardingm','Onboarding Member In Charge')</th>
                <th>@sortablelink('email', 'Email')</th>
                <th>@sortablelink('contact', 'Contact')</th>--}}
                {{--<th>@sortablelink('process_id', 'Process')</th>--}}
                {{--<th>@sortablelink('created', 'Created')</th>--}}
                @if($np != 0)
                <th>@sortablelink('non_progressing', 'Non Progressing Date')</th>
                @endif
                {{--<th>@sortablelink('converted', 'Converted')</th>
                <th><abbr title="Days taken to complete a clients process.">@sortablelink('duration', 'Duration')</abbr></th>
                <th>@sortablelink('step', 'Step')</th>--}}
                {{--<th><abbr title="Is the client progressing or not."><i class="fa fa-tasks"></i></abbr></th>
                <th><abbr title="Does the client need approval."><i class="fa fa-tasks"></i></abbr></th>--}}
                <th>@sortablelink('introducer', 'User')</th>
            </tr>
            </thead>
            <tbody>
            @forelse($clients['results'] as $client)
                <tr>
                    <td><a href="{{route('clients.show',$client["id"])}}">{{($client["portfolio_name"] != '' ? $client["portfolio_name"] : $client["company"] )}}</a></td>{{--
                    <td>{{$client["portfolio_manager"]}}</td>
                    <td>{{$client["project_name"]}}</td>
                    <td>{{$client["project_start_date"]}}</td>
                    <td>{{$client["proposed_end_date"]}}</td>--}}
                    {{--<td>{{(isset($client["referrer"]) ? $client["referrer"] : '')}}</td>
                    <td>{{(isset($client["director"]) ? $client["director"] : '')}}</td>
                    <td>{{(isset($client["onboardingl"]) ? $client["onboardingl"] : '')}}</td>
                    <td>{{(isset($client["onboardingm"]) ? $client["onboardingm"] : '')}}</td>
                    <td>{{$client["email"]}}</td>
                    <td>{{$client["contact"]}}</td>--}}
                    {{--<td>{{$client["process"]}}</td>--}}
                    {{--<td>{{$client["created_at"]}}</td>--}}
                    @if($np != 0)
                        <td>{{$client["non_progressing"]}}</td>
                    @endif
                    {{--<td>{{!is_null($client["completed_at"]) ? \Carbon\Carbon::parse($client["completed_at"])->format('Y-m-d') : ''}}</td>
                    <td>{{$client["completed_days"]}}</td>
                    <td>{{$client["step"]}}</td>--}}
                    {{--<td>{!!($client["is_progressing"]) ? '' : '<i class="fa fa-exclamation-triangle text-danger"></i>'!!}</td>
                    <td>{!!(!$client["needs_approval"]) ? '' : '<i class="fa fa-asterisk text-danger"></i>'!!}</td>--}}
                    <td><a href="{{route('profile',$client["introducer"])}}"><img src="{{route('avatar',['q'=>$client["avatar"] ])}}" class="blackboard-avatar blackboard-avatar-inline blackboard-avatar-navbar-img" alt="Avatar"/></a></td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td></td>
                </tr>
            @endforelse
            </tbody>

        </table>
    </div>


        @include('clients.pagination', ['paginator' => $clients['results'], 'link_limit' => 5])
    </div>
@endsection
@section('extra-js')
    <script>
        /*$('select[name="s"], [name="step"], [name="p"]').change(function () {

            $('#clientform').submit();

        });*/

        $(document).ready(function() {
            if ($('select[name="p"]').val() > 0) {
                $(document).find('#step').empty();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: '/reports/getprocesssteps/' + $('select[name="p"]').val(),
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $(document).find('#step').append($("<option></option>").attr("value", '').text('All Steps'));
                        $.each(data, function (key, value) {
                            if(key === '{{ request()->get('step') }}') {
                                $(document).find('#step').append($("<option></option>").attr("value", key).text(value));
                                $('#step').val({{ request()->get('step') }}).attr("selected", "selected");
                            } else {
                                $(document).find('#step').append($("<option></option>").attr("value", key).text(value));
                            }
                        });

                    }
                });
            }

            $('#p').on('change',function()
            {
                if ($('select[name="p"]').val() > 0) {
                    $(document).find('#step').empty();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: '/reports/getprocesssteps/' + $('select[name="p"]').val(),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $(document).find('#step').append($("<option></option>").attr("value", '').text('All Steps'));
                            $.each(data, function (key, value) {
                                $(document).find('#step').append($("<option></option>").attr("value", key).text(value));
                            });

                        }
                    });
                    $('#step').val({{ request()->get('step') }}).attr("selected", "selected");
                }
            });
        });
    </script>
@endsection
