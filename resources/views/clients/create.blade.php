@extends('adminlte.default')

@section('title') Capture Client @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('clients.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row mt-3 d-block d-md-none d-lg-none">
        <div class="col-sm-12">
            <div class="w-50 p-1 float-left">
            <a href="{{route('clients.create')}}" class="btn {{active('clients.create','active')}} btn-outline-dark w-100 float-left"><i class="fa fa-plus"></i> Client</a>
            </div>
            <div class="w-50 p-1 float-right">
            <a href="{{route('referrers.create')}}" class="btn btn-outline-dark btn-referrer w-100 float-right"><i class="fa fa-plus"></i> Referrer</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    {{Form::open(['url' => route('clients.store'), 'method' => 'post'])}}

    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="form-group">
                {{Form::label('process', 'Process')}}
                {{Form::select('process',$processes,$last_process,['class'=>'form-control form-control-sm'. ($errors->has('process') ? ' is-invalid' : ''),'autofocus','id'=>'process'])}}
                @foreach($errors->get('process') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>

            {{--<div class="form-group">
                {{Form::label('first_name', 'First Name')}}
                {{Form::text('first_name',old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                @foreach($errors->get('first_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                {{Form::label('last_name', 'Last Name')}}
                {{Form::text('last_name',old('last_name'),['class'=>'form-control form-control-sm'. ($errors->has('last_name') ? ' is-invalid' : ''),'placeholder'=>'Last Name'])}}
                @foreach($errors->get('last_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>--}}

            <div class="form-group">
                {{Form::label('company', 'Client Name')}}
                {{Form::text('company',old('company'),['class'=>'form-control form-control-sm'. ($errors->has('company') ? ' is-invalid' : ''),'placeholder'=>'Company name'])}}
                @foreach($errors->get('company') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>

            {{--<div class="form-group">
                {{Form::label('email', 'Email')}}
                {{Form::email('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                @foreach($errors->get('email') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                {{Form::label('contact', 'Contact Number')}}
                {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
                @foreach($errors->get('contact') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>--}}
            {{--<div class="form-group">
                {{Form::label('portfolio_name', 'Portfolio Name')}}
                {{Form::text('portfolio_name',old('portfolio_name'),['class'=>'form-control form-control-sm'. ($errors->has('portfolio_name') ? ' is-invalid' : ''),'placeholder'=>'Portfolio Name'])}}
                @foreach($errors->get('portfolio_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('portfolio_manager', 'Portfolio Manager')}}
                {{Form::text('portfolio_manager',old('portfolio_manager'),['class'=>'form-control form-control-sm'. ($errors->has('portfolio_manager') ? ' is-invalid' : ''),'placeholder'=>'Portfolio Manager'])}}
                @foreach($errors->get('portfolio_manager') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('project_name', 'Project Name')}}
                {{Form::text('project_name',old('project_name'),['class'=>'form-control form-control-sm'. ($errors->has('project_name') ? ' is-invalid' : ''),'placeholder'=>'Project Name'])}}
                @foreach($errors->get('project_name') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('date_last_updated', 'Date Last Updated')}}
                <input name="date_last_updated" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{old('date_last_updated')}}" class="form-control form-control-sm {{($errors->has('date_last_updated') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
                @foreach($errors->get('date_last_updated') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('programme', 'Programme (if applicable)')}}
                {{Form::text('programme',old('programme'),['class'=>'form-control form-control-sm'. ($errors->has('programme') ? ' is-invalid' : ''),'placeholder'=>'Programme'])}}
                @foreach($errors->get('programme') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('programme_manager', 'Programme Manager (if applicable)')}}
                {{Form::text('programme_manager',old('programme_manager'),['class'=>'form-control form-control-sm'. ($errors->has('programme_manager') ? ' is-invalid' : ''),'placeholder'=>'Programme Manager'])}}
                @foreach($errors->get('programme_manager') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('project_manager', 'Project Manager')}}
                {{Form::text('project_manager',old('project_manager'),['class'=>'form-control form-control-sm'. ($errors->has('project_manager') ? ' is-invalid' : ''),'placeholder'=>'Project Manager'])}}
                @foreach($errors->get('project_manager') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('project_administrator', 'Project Administrator')}}
                {{Form::text('project_administrator',old('project_administrator'),['class'=>'form-control form-control-sm'. ($errors->has('project_administrator') ? ' is-invalid' : ''),'placeholder'=>'Project Administrator'])}}
                @foreach($errors->get('project_administrator') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>

            <div class="form-group">
                {{Form::label('project_budget', 'Project Budget')}}
                {{Form::text('project_budget',old('project_budget'),['class'=>'form-control form-control-sm'. ($errors->has('project_budget') ? ' is-invalid' : ''),'placeholder'=>'Project Budget'])}}
                @foreach($errors->get('project_budget') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('project_executive', 'Project Executive')}}
                {{Form::text('project_executive',old('project_executive'),['class'=>'form-control form-control-sm'. ($errors->has('project_executive') ? ' is-invalid' : ''),'placeholder'=>'Project Executive'])}}
                @foreach($errors->get('project_executive') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('project_start_date', 'Project Start Date')}}
                <input name="project_start_date" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{old('project_start_date')}}" class="form-control form-control-sm {{($errors->has('project_start_date') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
                @foreach($errors->get('project_start_date') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>
            <div class="form-group">
                {{Form::label('proposed_end_date', 'Proposed End Date')}}
                <input name="proposed_end_date" type="date" min="1900-01-01" max="{{ Carbon\Carbon::now()->addYears(10)->format('Y-m-d') }}" value="{{old('proposed_end_date')}}" class="form-control form-control-sm {{($errors->has('proposed_end_date') ? ' is-invalid' : '')}}" placeholder="Insert date..." />
                @foreach($errors->get('proposed_end_date') as $error)
                    <div class="invalid-feedback">
                        {{$error}}
                    </div>
                @endforeach
            </div>--}}

        </div>
    </div>


    <div class="form-group">
        <button type="submit" class="btn btn-sm add-client">Save</button>
    </div>

    {{Form::close()}}
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>
        $(function(){
            $('#process').on('change',function(){
                $.ajax({
                    dataType: 'json',
                    url: '/processes/step_count/'+$('#process').val(),
                    type:'GET'
                }).done(function(data) {
                    if(data === 0){
                        let process = $('#process').val();
                        $('#modalProcess').modal('show');
                        $('#modalProcess').find('.message').html('The process you selected currently has no steps.<br /><br />' +
                            'Click <a href="/processes/' + process + '/show">here</a> to add steps to this process now.');
                        $('body').find('.add-client').prop('disabled',true);
                    } else {
                        $('body').find('.add-client').prop('disabled',false);
                    }
                });
            })
        })
    </script>
@endsection