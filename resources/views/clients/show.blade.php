@extends('adminlte.default')

@section('title') {{($client->company == '' || $client->company == 'N/A' ? $client->first_name.' '.$client->last_name : $client->company )}} @endsection

@section('header')
    <div class="container-fluid container-title form-inline">
        <div class="col-sm-4">
            <h3 class="form-inline">@yield('title')</h3>
        </div>
        <div class="col-sm-4">
        @if(isset($view_process_dropdown) && isset($client))
                <input type="hidden" value="{{$client->id}}" id="client_id" />
                <label class="float-left">View Process</label> <select class="form-control form-control-sm float-left ml-3" id="viewprocess">
                @forelse($view_process_dropdown as $processdd)
                    <option value="{{$processdd['id']}}" {{--{{(isset($client->process_id) && $client->process_id == $processdd['id'] ? 'selected' : '')}}--}}>{{$processdd['name']}}</option>
                @empty
                    <option value="">There are no processes assigned for this client.</option>
                @endforelse
            </select>
        @else
            @forelse(auth()->user()->office()->processes->first()->steps as $step)
                <div class="col-lg blackboard-step-{{$step->id}}">
                    {{$step->name}}
                </div>
            @empty
                <p>There are no processes assigned for this client.</p>
            @endforelse
        @endif
        </div>
        <div class="col-sm-4">
        <a href="{{(isset($path) && $path == 1 ? $path_route : route('clients.index'))}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
        </div>
    </div>
@endsection

@section('content')
<div class="container-fluid">
    @include('clients.process')

    <ul class="nav nav-tabs nav-fill mt-3">
        <li class="nav-item">
            <a class="nav-link {{active('clients.show','active')}}" href="{{route('clients.show',$client)}}">Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{(\Request::is('clients/*/progress/*') ? 'active' : '')}}" href="{{route('clients.progress',$client)}}/{{$client->process_id}}/{{$client->step_id}}">Progress</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{active('clients.documents','active')}}" href="{{route('clients.documents',$client)}}">Documents</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{active('clients.actions','active')}}" href="{{route('clients.actions',$client)}}">Actions</a>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="#">Events <span class="badge badge-danger">Soon</span></a>
        </li>--}}
        <li class="nav-item">
            <a class="nav-link {{active('clients.forms','active')}} {{(\Request::is('forms/*/create_dynamic_form/*') ? 'active' : '')}}" href="{{route('clients.forms',$client)}}">Forms</a>
        </li>
    </ul>

    <div class="row m-0 pt-3 pb-5 border border-top-0">
        @yield('tab-content')
    </div>
</div>
@endsection