@extends('layouts.proud')

@section('proud-form')
    <form method="POST" action="{{url()->current()}}">
        {{ csrf_field() }}

        <p class="text-center text-muted mb-5">
            <i>To complete the onboarding process, your introducer, <b>{{$client->introducer->name()}}</b>, has requested additional information regarding your application.</i>
        </p>

        {{$activity['name']}}

        <div class="form-group">
            @if($activity['type']=='date')
                <input name="{{$activity['id']}}" type="date" value="{{(isset($activity['value']) ? $activity['value'] : old($activity['id']))}}" class="form-control" placeholder="Insert date..."/>
            @endif

            @if($activity['type']=='text')
                {{Form::text($activity['id'],(isset($activity['value']) ? $activity['value'] : old($activity['id'])),['class'=>'form-control','placeholder'=>'Insert text...'])}}
            @endif

            @if($activity['type']=='boolean')
                {{Form::select($activity['id'],[1=>'Yes',0=>'No'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...'])}}
            @endif

            @if($activity['type']=='template_email')
                <div class="row">
                    <div class="col-md-5 input-group">
                        {{Form::select($activity['id'],$templates,(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...'])}}
                    </div>
                    <div class="col-md-7 input-group">
                        {{Form::text($activity['id'],(isset($client->email) ? $client->email : old($client->email)),['class'=>'form-control','placeholder'=>'Insert email...'])}}
                        <div class="input-group-append" onclick="viewTemplate({{$activity['id']}})">
                            <button type="button" class="btn">View Template</button>
                        </div>
                        &nbsp;&nbsp;
                        <div class="input-group-append" onclick="submitTemplate({{$activity['id']}})">
                            <button type="button" class="btn">Send Template</button>
                        </div>
                    </div>
                </div>
                <div id="message_{{$activity['id']}}"></div>
            @endif

            @if($activity['type']=='document_email')
                <div class="input-group">
                    {{Form::select($activity['id'],$documents,(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...'])}}
                    <div class="input-group-append">
                        <button type="button" class="btn">Send Document</button>
                    </div>
                </div>
            @endif

            @if($activity['type']=='document')
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            @endif

            @if($activity['type']=='dropdown')
                {{Form::select($activity['id'],$activity['dropdown_items'],(isset($activity['value']) ? $activity['value'] : ''),['class'=>'form-control','placeholder'=>'Please select...'])}}
            @endif

            @if($activity['type']=='notification')
                <br>
                <button type="button" class="btn btn-primary btn-sm" onclick="sendNotification({{$activity['id']}})"><i class="fa fa-paper-plane"></i> Send notification</button>
                <div id="message_{{$activity['id']}}"></div>
            @endif

            @foreach($errors->get('email') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        <div class="form-group mt-5">
            <button type="submit" class="btn btn-block">
                Submit
            </button>
        </div>
    </form>
@endsection

@section('proud-footer')
    <div class="card-footer text-center p-3">
        Expired link? <a href="{{route('register')}}">Resend</a>
    </div>
@endsection
