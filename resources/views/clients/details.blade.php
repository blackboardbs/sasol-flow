@extends('clients.show')

@section('tab-content')
    <div class="col-lg-9">
        <div class="card mt-3">
            <div class="card-header">
                Comments
            </div>
            <div class="card-body">
                <table class="table table-borderless">
                    @forelse($client->comments as $comment)
                        <tr>
                            <td>
                                <a href="{{route('profile',$comment->user_id)}}">
                                    <img src="{{route('avatar',['q'=>$comment->user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline" alt="{{$comment->user->name()}}"/>{{$comment->user->name()}}
                                </a>
                                : {{$comment->comment}}
                                <small class="float-right text-muted"><i class="fa fa-calendar"></i> {{substr($comment->created_at,0,10)}}&nbsp;&nbsp;<i class="fa fa-clock-o"></i> {{substr($comment->created_at,11,19)}}</small>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center">
                                <small><i>No comments added yet.</i></small>
                            </td>
                        </tr>
                    @endforelse
                    <tr>
                        <td>
                            {{Form::open(['url' => route('clients.storecomment', $client), 'method' => 'post'])}}
                            <div class="input-group">
                                {{Form::text('comment',old('comment'),['class'=>'form-control form-control-sm','placeholder'=>'Type a comment'])}}
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-info btn-sm"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                            {{Form::close()}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <ul>
                    <dt>
                        Client Name
                    </dt>
                    <dd>
                        @if($client->company)
                            {{$client->company}}
                        @else
                            <small><i>No client name captured</i></small>
                        @endif
                    </dd>
                    {{--<dt>
                        Portfolio Manager
                    </dt>
                    <dd>
                        @if($client->portfolio_manager)
                            {{$client->portfolio_manager}}
                        @else
                            <small><i>No portfolio manager captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Name
                    </dt>
                    <dd>
                        @if($client->project_name)
                            {{$client->project_name}}
                        @else
                            <small><i>No project name captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Date Last Updated
                    </dt>
                    <dd>
                        @if($client->date_last_updated)
                            {{\Illuminate\Support\Carbon::parse($client->date_last_updated)->format('Y-m-d')}}
                        @else
                            <small><i>No last updated date captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Programme
                    </dt>
                    <dd>
                        @if($client->programme)
                            {{$client->programme}}
                        @else
                            <small><i>No programme captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Programme Manager
                    </dt>
                    <dd>
                        @if($client->programme_manager)
                            {{$client->programme_manager}}
                        @else
                            <small><i>No programme manager captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Manager
                    </dt>
                    <dd>
                        @if($client->project_manager)
                            {{$client->project_manager}}
                        @else
                            <small><i>No project manager captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Administrator
                    </dt>
                    <dd>
                        @if($client->project_administrator)
                            {{$client->project_administrator}}
                        @else
                            <small><i>No project administrator captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Budget
                    </dt>
                    <dd>
                        @if($client->project_budget)
                            {{$client->project_budget}}
                        @else
                            <small><i>No project budget captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Executive
                    </dt>
                    <dd>
                        @if($client->project_executive)
                            {{$client->project_executive}}
                        @else
                            <small><i>No project executive captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Project Start Date
                    </dt>
                    <dd>
                        @if($client->project_start_date)
                            {{\Illuminate\Support\Carbon::parse($client->project_start_date)->format('Y-m-d')}}
                        @else
                            <small><i>No project start date captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Proposed End Date
                    </dt>
                    <dd>
                        @if($client->proposed_end_date)
                            {{\Illuminate\Support\Carbon::parse($client->proposed_end_date)->format('Y-m-d')}}
                        @else
                            <small><i>No proposed end date captured</i></small>
                        @endif
                    </dd>--}}
                    <dt>
                        Process
                    </dt>
                    <dd>
                        @if($client->process_id)
                            {{$client->process->name}}
                        @else
                            <small><i>No process captured</i></small>
                        @endif
                    </dd>
                    <dt>
                        Converted Date
                    </dt>
                    <dd>
                        @if($client->completed_at)
                            {{\Illuminate\Support\Carbon::parse($client->completed_at)->format('Y-m-d')}}
                        @else
                            <small><i>Not converted</i></small>
                        @endif
                    </dd>
                    <dt>
                        Office
                    </dt>
                    <dd>
                        {{$client->office->area->region->division->name}} / {{$client->office->area->region->name}} / {{$client->office->area->name}} / {{$client->office->name}}
                    </dd>
                    <dt>
                        Created
                    </dt>
                    <dd>
                        <p>
                            {{$client->created_at->diffForHumans()}} <span class="text-muted"><i> <i class="fa fa-clock-o"></i> {{\Illuminate\Support\Carbon::parse($client->created_at)->format('Y-m-d')}}</i></span>
                        </p>
                    </dd>
                    @if(isset($client->not_progressing_date) && $client->not_progressing_date != null)
                    <dt>
                        Moved to Not Progressing
                    </dt>
                    <dd>
                        <p>
                            {{\Illuminate\Support\Carbon::parse($client->not_progressing_date)->diffForHumans()}} <span class="text-muted"><i> <i class="fa fa-clock-o"></i> {{\Illuminate\Support\Carbon::parse($client->not_progressing_date)->format('Y-m-d')}}</i></span>
                        </p>
                    </dd>
                    @endif
                </ul>

                <div id="actual-times" class="mt-3" style="width: 100%"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="card-header">
                Actions
            </div>
            <div class="card-body">
                @if(auth()->user()->can('maintain_client'))
                    {{Form::open(['url' => route('clients.follow',$client), 'method' => 'put'])}}
                    @if($client->users->where('id',auth()->id())->count()>0)
                        {{Form::hidden('follow',0)}}
                        <button type="submit" class="btn btn-block btn-sm btn-primary"><i class="fa fa-star"></i> Unfollow</button>
                    @else
                        {{Form::hidden('follow',1)}}
                        <button type="submit" class="btn btn-block btn-sm btn-outline-primary"><i class="fa fa-star-o"></i> Follow</button>
                    @endif
                    {{Form::close()}}

                    <br>

                    {{Form::open(['url' => route('clients.progressing',$client), 'method' => 'post'])}}
                    @if($client->user_id == auth()->id() || auth()->user()->can('admin'))
                        <a href="{{route('clients.edit',$client)}}" class="btn btn-sm btn-block btn-outline-primary"><i class="fa fa-pencil"></i> Edit</a>

                        <br>
                        @if($client->trashed())
                            <a href="{{route('clients.restore',$client)}}" class="btn btn-sm btn-block btn-outline-success"><i class="fa fa-undo"></i> Restore</a>
                        @else
                            <a href="{{route('clients.delete',$client)}}" class="deleteclient btn btn-sm btn-block btn-outline-danger"><i class="fa fa-trash"></i> Delete</a>
                        @endif

                        <br>

                        <button type="submit" class="btn btn-block btn-sm {{($client->is_progressing) ? 'btn-success' : 'btn-danger'}}" title="{{($client->is_progressing) ? 'Set status to not progressing' : 'Set status to progressing again'}}"><i class="fa fa-exclamation-triangle"></i> {{($client->is_progressing) ? 'Progressing' : 'Not progressing'}}</button>
                    @else
                        <button type="button" class="btn btn-block btn-sm btn-outline-primary disabled" disabled title="You do not have permission to do that"><i class="fa fa-pencil"></i> Edit</button>

                        <br>

                        <button type="button" class="btn btn-block btn-sm {{($client->is_progressing) ? 'btn-success' : 'btn-danger'}} disabled" disabled title="You do not have permission to do that"><i class="fa fa-exclamation-triangle"></i> {{($client->is_progressing) ? 'Progressing' : 'Not progressing'}}</button>
                    @endif
                    {{Form::close()}}

                    @if($client->needs_approval)
                        <hr>

                        {{Form::open(['url' => route('clients.approval',$client), 'method' => 'post'])}}
                        {{Form::hidden('status',true)}}
                        <button type="submit" class="btn btn-block btn-sm btn-outline-success"><i class="fa fa-check"></i> Approve lead</button>
                        {{Form::close()}}

                        <br>

                        {{Form::open(['url' => route('clients.approval',$client), 'method' => 'post'])}}
                        {{Form::hidden('status',false)}}
                        <button type="submit" class="btn btn-block btn-sm btn-outline-danger"><i class="fa fa-times"></i> Decline lead</button>
                        {{Form::close()}}
                    @endif
                <br />
                    {{Form::open(['url' => route('messages.client',['client_id'=>$client]), 'method' => 'get'])}}
                    {{Form::hidden('status',true)}}
                    <button type="submit" class="btn btn-block btn-sm btn-success"><i class="fa fa-comment"></i> Send Message</button>
                    {{Form::close()}}

                @else
                    <button type="button" class="btn btn-block btn-sm btn-outline-primary disabled" disabled title="You do not have permission to do that"><i class="fa fa-star-o"></i> Follow</button>

                    <br>

                    <button type="button" class="btn btn-block btn-sm btn-outline-primary disabled" disabled title="You do not have permission to do that"><i class="fa fa-pencil"></i> Edit</button>
                @endif
                    {{--@foreach($process_progress as $key => $step)
                        @foreach($step['activities'] as $activity)
                            {{$activity["name"]}}<br />
                        @endforeach
                    @endforeach--}}

            </div>
        </div>

        <div class="card mt-3">
            <div class="card-header">
                Assigned users
            </div>
            <div class="card-body">
                <dt>
                    Registrar
                </dt>
                <dd>
                    <a href="{{route('profile',$client->introducer_id)}}"><img src="{{route('avatar',['q'=>$client->introducer->avatar])}}" class="blackboard-avatar blackboard-avatar-inline"/> {{$client->introducer->name()}}</a>
                </dd>
                {{--<dt>
                    Referrer
                </dt>
                <dd>
                    @if(!$referrer)
                        <small><i>No referrer assigned</i></small>
                    @else
                        {{$referrer}}
                    @endif
                </dd>
                <dt>
                    Relationship Lead
                </dt>
                <dd>
                    @if(!$director)
                        <small><i>No director assigned</i></small>
                    @else
                        {{$director}}
                    @endif
                </dd>
                <dt>
                    Onboarding Lead
                </dt>
                <dd>
                    @if(!$onboardingl)
                        <small><i>No lead assigned</i></small>
                    @else
                        {{$onboardingl}}
                    @endif
                </dd>
                <dt>
                    Onboarding Team Member In Charge
                </dt>
                <dd>
                    @if(!$onboardingm)
                        <small><i>No member assigned</i></small>
                    @else
                        {{$onboardingm}}
                    @endif
                </dd>--}}
                <dt>
                    Following users
                </dt>
                @forelse($client->users as $user)
                    <dd>
                        <a href="{{route('profile',$user->id)}}"><img src="{{route('avatar',['q'=>$user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline"/> {{$user->name()}}</a>
                    </dd>
                @empty
                    <dd>
                        <small><i>No users tagged yet</i></small>
                    </dd>
                @endforelse
            </div>
        </div>
    </div>
@endsection

@section('extra-js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
    <script src="https://rawgit.com/highcharts/rounded-corners/master/rounded-corners.js"></script>

    <script>
        Highcharts.theme = {};

        Highcharts.chart('actual-times', {
            colors: ['#86bffd'],
            title: {
                text: ''
            },
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    formatter: function (x) {
                        return (this.value) + " seconds";
                    }
                },
            },
            xAxis: {
                crosshair: true,
                categories: [
                    @foreach($client_actual_times as $name => $step)
                        '{{$name}}',
                    @endforeach
                ]
            },
            tooltip: {
                formatter: function () {
                    return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' seconds</b>';
                }
            },
            series: [{
                data: [
                    @foreach($client_actual_times as $name => $step)
                    {{$step}},
                    @endforeach
                ]
            }],
            plotOptions: {
                series: {
                    borderRadiusTopLeft: '3px',
                    borderRadiusTopRight: '3px'
                }
            }
        });


        $(".deleteclient").click(function (e) {
            e.preventDefault();
            var conf = confirm("Are you sure you want to delete this client?");
            if(conf)
                window.location = $(this).attr("href");
        });



    </script>
@endsection
