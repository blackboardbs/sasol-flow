<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{!! asset('storage/favicon.ico') !!}" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{env('APP_NAME')}}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{!! asset('fontawesome/css/all.css') !!}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset('adminlte/dist/css/adminlte.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/flow.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/progress-wizard.min.css') !!}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/jquery-ui.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/perfect-scrollbar.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap/bootstrap-multiselect.css') !!}">
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    {{--<link href="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">
    <style>
        .ui-tooltip, .arrow:after {
            background: black !important;
            border: 0px solid transparent !important;
        }
        .ui-tooltip {
            padding: 5px 10px;
            color: white !important;
            font-size: 11px !important;
            font-weight:normal !important;
            border-radius: 5px;
            text-transform: uppercase;
            box-shadow: 0 0 7px black;
        }
        .arrow {
            width: 60px;
            height: 14px;
            overflow: hidden;
            position: absolute;
            left: 50%;
            margin-left: 0px;
            bottom: -10px;
        }
        .arrow.top {
            top: -16px;
            bottom: auto;
        }
        .arrow.left {
            left: 20%;
        }
        .arrow:after {
            content: "";
            position: absolute;
            left: 20px;
            top: -20px;
            width: 25px;
            height: 25px;
            box-shadow: 6px 5px 9px -9px black;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .arrow.top:after {
            bottom: -20px;
            top: auto;
        }

        /* =Tooltip Style -------------------- */

        /* Tooltip Wrapper */
        .has-tooltip {
            position: relative;
        }
        .has-tooltip .tooltip2 {
            opacity: 0;
            visibility: hidden;
            -webkit-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            -moz-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            -o-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            transition: visibility 0s ease 0.5s,opacity .3s ease-in;
        }
        .has-tooltip:hover .tooltip2 {
            opacity: 1;
            visibility: visible;
        }

        /* Tooltip Body */
        .tooltip2 {
            background-color: #222;
            bottom: 130%;
            color: #fff;
            font-size: 14px;
            left: -100%;
            margin-left: 0px;
            padding: 6px;
            position: absolute;
            text-align: left;
            text-decoration: none;
            text-shadow: none;
            width:auto;
            min-width: 600px;
            max-width: 1000px;
            overflow: auto;
            z-index: 4;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -o-border-radius: 3px;
            border-radius: 3px;
        }

        /* Tooltip Caret */
        .tooltip2:after {
            border-top: 5px solid #222;
            border-left: 4px solid transparent;
            border-right: 4px solid transparent;
            bottom: -5px;
            content: " ";
            font-size: 0px;
            left: 25px;
            line-height: 0%;
            margin-left: -4px;
            position: absolute;
            width: 0px;
            z-index: 1;
        }

        .tooltip2 ol,.tooltip2 ul,.tooltip2 li{
            text-align: left;
            /*margin: 0px;
            padding:0px;*/
        }

        .wrapper, body, html {
            min-height: 100%;
            overflow-x: unset;
        }
    </style>
    @yield('extra-css')
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="overlay">
    <div class="spinner"></div>
    <br/>
    Loading...
</div>
<div id="overlay2" style="display: none;">

</div>
<div id="app" class="wrapper">
    @if(auth()->check() && auth()->user()->trial == 1)
        <attooh-trial-notification></attooh-trial-notification>
        <p id="countdown" style="text-align: center; position: absolute;top: 0;left: 0;z-index: 99999;"></p>
    @endif

    @include('flow.sidebar')
<!-- Content Wrapper. Contains page content -->
    @include('flow.header')
    <div class="content-wrapper main-wrapper">
        <!-- SETUP WIZARD -->

        <!-- Only display setup wizard if role is financial advisor -->
        @role('financialadvisor2')
        <blackboard-wizard></blackboard-wizard>
        @endrole

        @yield('content')
    </div>
    <!-- /.content-wrapper -->

        <div class="modal fade" id="edit_email_template">
            <div class="modal-dialog" style="width:800px !important;max-width:800px;">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <h5 class="modal-title">View Email Template</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="email_id" id="email_id" >
                                <input type="hidden" class="form-control" name="activity_id" id="activity_id" >
                            </div>
                            <div class="form-group mt-3">
                                {{Form::label('name', 'Name')}}
                                {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name','id'=>'email_title'])}}

                            </div>

                            <div class="form-group">
                                {{Form::label('Email Body')}}
                                {{ Form::textarea('email_content', null, ['class'=>'form-control my-editor','size' => '30x10','id'=>'email_content']) }}

                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                                <button type="button" onclick="saveEmailTemplate()" class="btn btn-sm btn-primary">Use Template</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="billing" style="z-index: 9999;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);">
            <div class="modal-dialog" style="width:400px !important;max-width:800px;">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="box-body" style="text-align: center;">
                            <strong>Access denied.</strong> Please contact the account owner.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmModal">
            <div class="modal-dialog" style="width:450px !important;max-width:450px;">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <h5 class="modal-title">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="form-group text-center">
                            {{Form::label('popup', '',['id'=>'confirmMessage'])}}

                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-sm btn-default" id="confirmOk">Yes</button>
                            <button type="button" class="btn btn-sm btn-default" id="confirmCancel">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="notifyModal">
            <div class="modal-dialog" style="width:450px !important;max-width:450px;">
                <div class="modal-content">
                    <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="form-group text-center">
                            {{Form::label('popup', '',['id'=>'notifyMessage'])}}

                        </div>
                        <div class="form-group text-center">
                            <button type="button" class="btn btn-sm btn-default" id="notifyOk">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="{!! asset('js/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/jquery/jquery-ui.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/popper.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/bootstrap/bootstrap.min.js') !!}" type="text/javascript"></script>

<!-- AdminLTE -->
<script src="{!! asset('adminlte/dist/js/adminlte.js') !!}" type="text/javascript"></script>
<script src="{!! asset('adminlte/dist/js/jscolor.js') !!}" type="text/javascript"></script>
<script src="{!! asset('adminlte/dist/js/custom.js') !!}" type="text/javascript"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet"/>

<script src="{!! asset('js/moment.min.js') !!}" type="text/javascript"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=361nrfmxzoobhsuqvaj3hyc2zmknskzl4ysnhn78pjosbik2"></script>
<script src="{!! asset('js/tinymce/vue-tinymce.js') !!}" type="text/javascript"></script>

@if(!strpos($_SERVER['REQUEST_URI'],'custom_report'))
@auth
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    <!-- SortableJS -->
    <script src="https://unpkg.com/sortablejs@1.4.2"></script>
    <!-- VueSortable -->
    <script src="https://unpkg.com/vue-sortable@0.1.3"></script>
@endauth
@endif

<script src="{!! asset('chosen/chosen.jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('chosen/docsupport/init.js') !!}" type="text/javascript" charset="utf-8"></script>
<script src="{!! asset('js/autocomplete.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/bootstrap/bootstrap-multiselect.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/perfect-scrollbar.js') !!}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    
    

    @if(!strpos($_SERVER['REQUEST_URI'],'progress') && !strpos($_SERVER['REQUEST_URI'],'overview') && !strpos($_SERVER['REQUEST_URI'],'basket'))
    <script>
    $(document).ajaxStart(function() {
        $('#overlay').fadeIn();
    });
    $(document).ajaxStop(function() {
        $('#overlay').fadeOut();
        $(".client-content").show();
    });
    </script>
    @endif

    @if((\Request::route()->getName() != 'clients.processes'))
    <script>
    $(window).on('load', function () {
    if($('.task-table').is(':visible')) {
        /*console.log('azure');
        isAzureSignedIn('/clients');*/
    }
    setTimeout(removeLoader, 500); //wait for page load PLUS two seconds.
    });

    function removeLoader() {
    $("#overlay").fadeOut(200, function () {
        /*$("#overlay2").fadeIn();
        $("#billing").modal({backdrop: 'static', keyboard: false},'show');*/
        $(".client-content").show();
        $(".client-capture-content").show();
    });
    }
    </script>
    @endif
    @if(Session::has('flash_success'))
    <script>
    toastr.success('<strong>Success!</strong> {{Session::get('flash_success')}}');

    toastr.options.timeOut = 1000;
    {{Session::forget('flash_success')}}
    </script>
@endif

    <script src="{!! asset('js/flow.js') !!}" type="text/javascript"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
<script src="https://rawgit.com/highcharts/rounded-corners/master/rounded-corners.js"></script>
@yield('extra-js')
<script>
    $(function() {

        var numItems = $('.progress-indicator').find('.card').length;
        if (numItems < parseInt(6)) {
            $('.progress-indicator').addClass('justify-content-center');
        }
        if (numItems === parseInt(6)) {
            $('.progress-indicator').addClass('justify-content-center');
        }
        if (numItems > parseInt(6)) {
            let offs = $('#step_' + $('#active_step_id').val()).offset().left;
            $('#scrolling-wrapper').animate({scrollLeft: offs - 300}, 0);
        }
    })
</script>
</body>
</html>
