@extends('flow.default')

@section('title') Dashboard @endsection

@section('header')
    <div class="container-fluid container-title w-100">
        <h3> @yield('title') </h3>
        <div style="float: right;">
            <form method="get" class="form-inline mt-0">
            <input type="hidden" name="r" value="{{(isset($_GET['r']) && $_GET['r'] != '' ? $_GET['r'] : '')}}">
            <input type="hidden" name="f" value="{{(isset($_GET['f']) && $_GET['f'] != '' ? $_GET['f'] : '')}}">
            <input type="hidden" name="t" value="{{(isset($_GET['t']) && $_GET['t'] != '' ? $_GET['t'] : '')}}">
            <input type="hidden" name="p" value="{{(isset($_GET['p']) && $_GET['p'] != '' ? $_GET['p'] : '')}}">
                {{-- @if(count($supervisor_list) > 0)
                <div class="form-group mr-1" style="display: inline-block">
                    <select name="supervisor" id="supervisor" class="form-control form-control-sm">
                        <option value="">Select Supervisor</option>
                        <option value="all">All</option>
                        @foreach($supervisor_list as $fn)
                            <option value="{{$fn->id}}" {{(isset($_GET['supervisor']) && $_GET['supervisor'] == $fn->id || $fn->id == auth()->id() ? 'selected="selected"' : '')}}>{{$fn->full_name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif
                @if(count($users_list) > 0)
                <div class="form-group mr-1" style="display: inline-block">
                    <select name="cam" id="cam" class="form-control form-control-sm">
                        <option value="">Select Salesperson</option>
                        <option value="all">All</option>
                        @foreach($users_list as $fn)
                            <option value="{{$fn->id}}" {{(isset($_GET['cam']) && $_GET['cam'] == $fn->id || $fn->id == auth()->id() ? 'selected="selected"' : '')}}>{{$fn->full_name}}</option>
                        @endforeach
                    </select>
                </div>
                @endif --}}
            </form>
        </div>
    </div>
@endsection

@section('content')
    <div class="content-container page-content">
        <div class="row col-md-12 h-100">
            <div class="container-fluid container-title">
                @yield('header')
            </div>
            <div class="container-fluid container-content dashboard" style="overflow-y: auto;overflow-x: hidden">
                @php
                    $style = array('0'=>'bg-danger-gradient','1'=>'bg-warning-gradient','2'=>'bg-success-gradient','3'=>'bg-info-gradient','4'=>'bg-primary-gradient','5'=>'bg-secondary-gradient','6'=>'bg-danger-gradient','7'=>'bg-warning-gradient','8'=>'bg-success-gradient','9'=>'bg-info-gradient','10'=>'bg-primary-gradient');
                @endphp
                <div class="row mt-3 dashboard-region">
                    @foreach($client_step_counts[0] as $x => $y)
                    <div class="col-lg">
                        <div class="card text-white blackboard-region" style="background:rgb(10, 129,188)">
                            <div class="card-body" style="padding: 0.75rem;">
                                <h4><i class="fa fa-chart-line"></i> {{(!empty($y) ? $y : '0')}}</h4>
                                <p class="d-inline-block w-100" style="font-size: .9rem;flex-wrap: nowrap;white-space: nowrap;">{{ ucfirst($x)}}</p>
                                <span class="float-right d-inline-block"><a href="{{route('clients.index',['p'=>44,'step'=>'converted'])}}&c=no" class="btn btn-sm btn-outline-light"><i class="fa fa-share"></i> View</a></span>
                            </div>
                        </div>
                        
                    </div>
                    @endforeach
                </div>

                    <div class="row pt-3">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    Average days from Prospecting to Converted
                                    <div class="float-right">
                                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary btn-graph active" id="type-1-column">
                                                <input type="radio" name="blackboard-dashboard-1-type"><i class="far fa-chart-bar"></i>
                                            </label>
                                            <label class="btn btn-secondary btn-graph" id="type-1-bar">
                                                <input type="radio" name="blackboard-dashboard-1-type"><i class="fa fa-align-left"></i>
                                            </label>
                                            <label class="btn btn-secondary btn-graph" id="type-1-line">
                                                <input type="radio" name="blackboard-dashboard-1-type"><i class="fa fa-chart-line"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-1 pt-2 pb-2">
                                    <div id="blackboard-dashboard-1" class="m-0" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    Converted Opportunities
                                    <div class="float-right">
                                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary btn-graph active" id="type-2-column">
                                                <input type="radio" name="blackboard-dashboard-2-type"><i class="far fa-chart-bar"></i>
                                            </label>
                                            <label class="btn btn-secondary btn-graph" id="type-2-bar">
                                                <input type="radio" name="blackboard-dashboard-2-type"><i class="fa fa-align-left"></i>
                                            </label>
                                            <label class="btn btn-secondary btn-graph" id="type-2-line">
                                                <input type="radio" name="blackboard-dashboard-2-type"><i class="fa fa-chart-line"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-1 pt-2 pb-2">
                                    <div id="blackboard-dashboard-2" class="m-0" style="height: 250px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('extra-js')
            <script>

                $(document).ready(function() {
                    $('#step_id').on('change', function () {
                        $(function() {

                            let ajaxCategories = [];
                            let ajaxValues = [];
                            let params = {
                                'process_id': '{!! request()->input('p') !!}',
                                'step_id': $('#step_id').val()
                            }

                            // Fire an ajax call
                            $.getJSON('graphs/getoutstandingactivitiesajax', params, function(data){
                                // Break apart the json returned into categories and series values
                                $.each(data, function(name, values) {
                                    ajaxCategories.push(name);
                                    ajaxValues.push(values.user);
                                });

                                // Update the categories
                                blackboardDashboard4.update({
                                    xAxis: {
                                        type: 'category',
                                        categories: ajaxCategories
                                    },
                                });

                                // Set the series
                                blackboardDashboard4.xAxis[0].setCategories(ajaxCategories);
                                blackboardDashboard4.series[0].setData(ajaxValues);
                            });
                        });
                    });

                    $('#dashboard_process').on('change',function (){
                        this.form.submit();
                    });

                    Highcharts.theme = {
                        colors: ['#00589e', '#00589e'],
                        title: {
                            text: ''
                        },
                        chart: {
                            type: 'column'
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: ''
                            }
                        },
                        xAxis: {
                            crosshair: true
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                    };

                    Highcharts.setOptions(Highcharts.theme);

                    let blackboardDashboard1 = Highcharts.chart('blackboard-dashboard-1', {
                        yAxis: {
                            labels: {
                                formatter: function (x) {
                                    return (this.value) + " days";
                                }
                            },
                            plotLines: [{
                                color: '#f6918c',
                                width: 2,
                                value: {{$config->onboard_days}},
                                dashStyle: 'shortdash',
                                zIndex: 5
                            }]
                        },

                        xAxis: {
                            categories: ['Min', 'Avg', 'Max'],
                        },
                        tooltip: {
                            formatter: function () {
                                return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' days</b>';
                            }
                        },
                        series: [{
                            data: [{{$client_onboard_times["minimum"]}}, {{$client_onboard_times["average"]}}, {{$client_onboard_times["maximum"]}}]
                        }],
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px',
                                point: {
                                    events: {
                                        click: function () {
                                            switch (this.category) {
                                                case 'Min':
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'a']) !!}';
                                                    break;
                                                case 'Avg':
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'a']) !!}';
                                                    break;
                                                case 'Max':
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'f'=>request()->input('f'),'t'=>request()->input('t'),'c'=>'yes','si'=>'completed_days','so'=>'d']) !!}';
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        },
                    });


                    let blackboardDashboard2 = Highcharts.chart('blackboard-dashboard-2', {
                        yAxis: {
                            labels: {
                                formatter: function (x) {
                                    return (this.value) + " clients";
                                }
                            },
                            plotLines: [{
                                color: '#dc3545',
                                width: 2,
                                value: {{$config->onboards_per_day}},
                                dashStyle: 'shortdash',
                                zIndex: 5
                            }]
                        },
                        xAxis: {
                            categories: [
                                @foreach($client_onboards as $key => $client)
                                    '{{$key}}',
                                @endforeach
                            ]
                        },
                        tooltip: {
                            formatter: function () {
                                return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' clients</b>';
                            }
                        },
                        series: [{
                            data: [
                                @foreach($client_onboards as $client)
                                @if($client > 0)
                                {{$client}},
                                @endif
                                @endforeach
                            ]
                        }],
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px',
                                point: {
                                    events: {
                                        click: function () {
                                            let start = '';
                                            switch ('{{request()->input('r')}}') {
                                                case 'day':
                                                    start = moment(this.category, 'DD MMMM YYYY');
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.format('YYYY-MM-DD');
                                                    break;
                                                case 'week':
                                                    start = moment(moment(this.category).format('WW YYYY'), 'WW YYYY');
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(6, 'days').format('YYYY-MM-DD');
                                                    break;
                                                case 'month':
                                                    start = moment(this.category, 'MMMM YYYY');
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(1, 'months').format('YYYY-MM-DD');
                                                    break;
                                                case 'year':
                                                    start = moment(this.category, 'YYYY');
                                                    location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p'),'c'=>'yes']) !!}&f=' + start.format('YYYY-MM-DD') + '&t=' + start.add(1, 'years').format('YYYY-MM-DD');
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });

                    let blackboardDashboard3 = Highcharts.chart('blackboard-dashboard-3', {
                        yAxis: {
                            labels: {
                                formatter: function (x) {
                                    return (this.value) + " days";
                                }
                            },
                        },
                        xAxis: {
                            categories: [
                                @foreach($process_average_times as $name => $step)
                                    '{{$name}}',
                                @endforeach
                            ]
                        },
                        tooltip: {
                            formatter: function () {
                                return '<small class="text-muted">' + this.x + '</small><br><b>' + this.y + ' days</b>';
                            }
                        },
                        series: [{
                            data: [
                                @foreach($process_average_times as $name => $step)
                                {{$step}},
                                @endforeach
                            ]
                        }],
                        plotOptions: {
                            series: {
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px'
                            }
                        }
                    });


                    let blackboardDashboard4 = Highcharts.chart('blackboard-dashboard-4', {
                        yAxis: [
                            {
                                title: {},
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            {
                                title: {},
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                },
                                opposite: true
                            }
                        ],
                        xAxis: {
                            categories: [
                                @foreach($process_outstanding_activities as $name => $amount)
                                    '{{$name}}',
                                @endforeach
                            ]
                        },
                        series: [
                            {
                                name: 'Outstanding Activities',
                                data: [
                                    @foreach($process_outstanding_activities as $amount)
                                    @if($amount['user'] > 0)
                                    {{$amount['user']}},
                                    @endif
                                    @endforeach
                                ]
                            },
                        ],
                        tooltip: {
                            shared: true
                        },
                        plotOptions: {
                            series: {
                                cursor: 'pointer',
                                borderRadiusTopLeft: '3px',
                                borderRadiusTopRight: '3px',
                                point: {
                                    events: {
                                        click: function () {
                                            location.href = '{!! route('clients.index',['l'=>request()->input('l'),'p'=>request()->input('p')]) !!}&oa='+this.category+'&s=all&c=all&step={!! $outstanding_step_id !!}';
                                        }
                                    }
                                }
                            }
                        }
                    });

                    $('#type-1-bar').click(function () {
                        blackboardDashboard1.update({
                            chart: {
                                type: 'bar'
                            }
                        });
                    });

                    $('#type-1-line').click(function () {
                        blackboardDashboard1.update({
                            chart: {
                                type: 'line'
                            }
                        });
                    });

                    $('#type-1-column').click(function () {
                        blackboardDashboard1.update({
                            chart: {
                                type: 'column'
                            }
                        });
                    });

                    $('#type-2-bar').click(function () {
                        blackboardDashboard2.update({
                            chart: {
                                type: 'bar'
                            }
                        });
                    });

                    $('#type-2-line').click(function () {
                        blackboardDashboard2.update({
                            chart: {
                                type: 'line'
                            }
                        });
                    });

                    $('#type-2-column').click(function () {
                        blackboardDashboard2.update({
                            chart: {
                                type: 'column'
                            }
                        });
                    });

                    $('#type-3-bar').click(function () {
                        blackboardDashboard3.update({
                            chart: {
                                type: 'bar'
                            }
                        });
                    });

                    $('#type-3-line').click(function () {
                        blackboardDashboard3.update({
                            chart: {
                                type: 'line'
                            }
                        });
                    });

                    $('#type-3-column').click(function () {
                        blackboardDashboard3.update({
                            chart: {
                                type: 'column'
                            }
                        });
                    });

                    $('#type-4-bar').click(function () {
                        blackboardDashboard4.update({
                            chart: {
                                type: 'bar'
                            }
                        });
                    });

                    $('#type-4-line').click(function () {
                        blackboardDashboard4.update({
                            chart: {
                                type: 'line'
                            }
                        });
                    });

                    $('#type-4-column').click(function () {
                        blackboardDashboard4.update({
                            chart: {
                                type: 'column'
                            }
                        });
                    });

                    $('#completed_clients').on('change', function () {
                        $(function() {

                            let ajaxCategories = [];
                            let ajaxValues = [];
                            let params = {
                                'process_id': '{!! request()->input('p') !!}',
                                'months': $('#completed_clients').val()
                            }

                            // Fire an ajax call
                            $.getJSON('graphs/getcompletedclientsajax', params, function(data){
                                // Break apart the json returned into categories and series values
                                $.each(data, function(name, value) {
                                    ajaxCategories.push(name);
                                    ajaxValues.push(value*1);
                                });

                                //console.log(ajaxCategories);
                                console.log(ajaxValues);

                                // Update the categories
                                blackboardDashboard2.update({
                                    xAxis: {
                                        type: 'category',
                                        categories: ajaxCategories
                                    },
                                });

                                // Set the series
                                blackboardDashboard2.xAxis[0].setCategories(ajaxCategories,false);
                                blackboardDashboard2.series[0].setData(ajaxValues, true);

                                blackboardDashboard2.redraw();
                            });
                        });
                    });
                })
            </script>
@endsection