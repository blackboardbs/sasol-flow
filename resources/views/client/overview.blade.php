@extends('client.show')

@section('tab-content')
    <div class="row col-md-12 h-100 m-0 p-0">
        <div class="col-md-6 h-100 pt-0 pb-0 pl-0">
            <div class="card-group overview-addnote">
            <div class="card h-100">
                <h5 class="card-title">Add a new log</h5>
                <div class="card-body pt-0">
                    {{Form::open(['url' => route('clients.storecomment', $client), 'method' => 'post','id'=>'add_comment'])}}
                    <table class="table table-borderless">
                    <tr>
                        <td style="height:30px;line-height:3rem;">Date</td>
                        <td><input type="date" name="logdate" id="logdate" class="form-control form-control-sm" value="{{Carbon\Carbon::parse(now())->format("Y-m-d")}}"></td>
                    </tr>
                    <tr>
                        <td style="height:30px;line-height:3rem;">Contact</td>
                        <td>{{Form::text('logcontact',$client->first_name.' '.$client->last_name,['class'=>'form-control form-control-sm','placeholder'=>'Add client name','id'=>'logcontact'])}}</td>
                    </tr>
                    <tr>
                        <td style="height:30px;line-height:3rem;">Call type</td>
                        <td><select name="logtype" id="logtype" class="form-control form-control-sm">
                            <option value="">Select call type</option>
                            <option value="Prospecting">Prospecting</option>
                            <option value="Negotiation">Negotiation</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td style="height:30px;line-height:3rem;">Salesperson</td>
                        <td><select name="logcams" id="logcams" class="form-control form-control-sm">
                            <option value="">Select Salesperson</option>
                            @foreach($userslist as $fn)
                                <option value="{{$fn->id}}" {{(old('logcams') == $fn->id || $fn->id == $client->introducer_id ? 'selected="selected"' : '')}}>{{$fn->full_name}}</option>
                            @endforeach
                        </select></td>
                    </tr>
                    <tr>
                        <td style="height:30px;line-height:3rem;">Call Successful?</td>
                        <td><div role="radiogroup" class="mt-0">
                            <input type="radio" value="1" name="logsuccess" id="logsuccess-enabled">
                            <label for="logsuccess-enabled">Yes</label><!-- remove whitespace
                            --><input type="radio" value="0" name="logsuccess" id="logsuccess-disabled" checked><!-- remove whitespace
                            --><label for="logsuccess-disabled">No</label>

                            <span class="selection-indicator"></span>
                        </div></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height:30px;line-height:3rem;">Comment</td>
                    </tr>
                    <tr>
                        <td colspan="2">{{Form::textarea('logcomment',old('logcomment'),['cols'=>'10','rows'=>'3','class'=>'form-control form-control-sm','placeholder'=>'Type your comment here','id'=>'logcomment'])}}</td>
</tr>
<tr>
                        <td colspan="2">
                        {{Form::file('logfile',old('logfile'),['class'=>'form-control form-control-sm w-100','id'=>'logfile','style'=>'width:100%;'])}}
</td></tr></table>
                    <input type="submit" class="btn btn-success overview-note-button float-right" value="Save note">
                    {{Form::close()}}
                    

                </div>
            </div>
            </div>
            <div class="card-group overview-openapplications">
            <div class="card h-100">
                <h5 class="card-title d-inline-block float-left">Processes in progress<a href="javascript:void(0)" onclick="startNewApplication({{$client->id}},{{$client->process_id}})" class="float-right d-inline-block" style="font-size: 14px;line-height: 24px;"><i class="fa fa-plus"></i> Start process</a></h5>
                <div class="card-body overflow-auto open-applications grid-items">
                    <div class="spinner"></div>
                </div>
            </div>
            </div>
        </div>

        <div class="col-md-6 h-100 pt-0 pb-0 pr-0">
            <div class="card h-100 overflow-auto">
                <h5 class="card-title">Logs</h5>
                <div class="card-body client-notes pt-0">
                    <div style="height: 100%;margin:auto;width:100%">
                    <div class="spinner"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    <script>

        function deletecomment(id){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/clients/deleteclientcomment/' + id,
                type: "POST",
                data: {comment: id},
                success: function (data) {
                    toastr.success('<strong>Success!</strong> ' + data);

                    toastr.options.timeOut = 1000;

                    $.ajax({
                        url: '/clients/' + {{ $client->id }} + '/getcomments',
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            let row = '';

                            $.each(data.data, function(key,value) {
                                row = row + '<div class="card">' +
                                    '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                    '<div class="card-block">' +
                                    '<blockquote class="card-blockquote">' +
                                    '<div class="blockquote-header">' + value.title + '</div>' +
                                    '<div class="blockquote-body">' + value.comment + '</div>' +
                                    '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                    '</blockquote>' +
                                    '</div>' +
                                    '</div>';
                            });

                            $('.client-notes').html(row);
                        }
                    });
                }
            });
        }
        $(function (){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: '/clients/' + {{ $client->id }} + '/getcomments',
                type: "GET",
                dataType: "json",
                success: function (data) {
                    let row = '';

                    if(data.data.length > 0) {
                        $.each(data.data, function(key,value) {
                            row = row + '<div class="card">' +
                                            '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                            '<div class="card-block">' +
                                            '<blockquote class="card-blockquote">' +
                                            '<div class="blockquote-header"></div>' +
                                            '<div class="blockquote-body"> ' + 
                                            '<table class="table table-borderless mb-0">' +
                                            '<tr>' + 
                                            '<td style="width:70px">Date</td><td>' + value.date + '</td>' +
                                            '<td rowspan="6" style="width:80px;font-size:2rem;">';
                                            
                                            if(value.file){
                                                row = row + '<a href="/storage/document?q='+ value.file +'"><i class="far fa-file"></i></a>';
                                            }
                                            
                                            row = row + '</td>'+
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Contact</td><td>' + value.contact + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Call type</td><td>' + value.call_type + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>CAM</td><td>' + value.cam_name + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Success</td><td>' + value.calsuccess + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Comment</td><td>' + value.comment + '<td>'  +
                                            '</tr>' +
                                            '</table>' +
                                            '</div>' +
                                            '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                            '</blockquote>' +
                                            '</div>' +
                                            '</div>';
                        });
                    } else {
                        row = row + '<div class="alert alert-info">There are currently no Logs for this client.</div>';
                    }

                    $('.client-notes').html(row);
                }
            });

            $.ajax({
                url: '/clients/' + {{ $client->id }} + '/current_applications',
                type: "GET",
                dataType: "json",
                success: function (data) {

                    let row = '';

                    if(data.length > 0) {
                        $.each(data, function (key, value) {
                            row = row + '<div class="d-table" style="width: 100%;border: 1px solid #ecf1f4;margin-bottom:0.75rem;">' +
                                '<div class="grid-icon">' +
                                '<i class="far fa-file-alt"></i>' +
                                '</div>' +
                                '<div class="grid-text">' +
                                '<span class="grid-heading">' + value.name + '</span>' +
                                'Date started:' +
                                '</div>' +
                                '<div class="grid-btn">' +
                                '<a href="/clients/' + {{ $client->id }} +'/progress/' + value.process_id + '/' + value.step_id + '/0" class="btn btn-outline-primary btn-block">Continue</a>' +
                                '</div>' +
                                '</div>';
                        });
                    } else {
                        row = row + '<div class="alert alert-info">There are currently no Applications in progress for this client.</div>';
                    }

                    $('.open-applications').html(row);
                }
            });

            $('#add_comment').submit(function (e) {
                e.preventDefault();

                let err = 0;
                let logdate = $('#logdate').val();
                let logcontact = $('#logcontact').val();
                let logtype = $('#logtype').val();
                let logcams = $('#logcams').val();
                let logcomment = $('#logcomment').val();

                if(logdate.length === 0){
                    err++;
                    $('#logdate').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#logdate').removeClass('is-invalid').addClass('is-valid');
                }

                if(logcontact.length < 2){
                    err++;
                    $('#logcontact').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#logcontact').removeClass('is-invalid').addClass('is-valid');
                }

                if(logtype.length === 0){
                    err++;
                    $('#logtype').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#logtype').removeClass('is-invalid').addClass('is-valid');
                }

                if(logcams.length === 0){
                    err++;
                    $('#logcams').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#logcams').removeClass('is-invalid').addClass('is-valid');
                }

                if(logcomment.length === 0){
                    err++;
                    $('#logcomment').addClass('is-invalid').removeClass('is-valid');
                } else {
                    $('#logcomment').removeClass('is-invalid').addClass('is-valid');
                }

                var form = $('#add_comment')[0];
                var formData = new FormData(form);

                if(err === 0) {
                    $.ajax({
                        url: '/clients/' + {{ $client->id }} +'/storecomment',
                        type: "POST",
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function (data) {
                            toastr.success('<strong>Success!</strong> ' + data);

                            toastr.options.timeOut = 1000;

                            $('#logcontact').removeClass('is-valid').val('');
                            $('#logtype').removeClass('is-valid').val('');
                            $('#logcomment').removeClass('is-valid').val('');
                            $('#logcams').removeClass('is-valid');
                            $('#logdate').removeClass('is-valid');

                            $.ajax({
                                url: '/clients/' + {{ $client->id }} + '/getcomments',
                                type: "GET",
                                dataType: "json",
                                success: function (data) {
                                    let row = '';

                                    $.each(data.data, function(key,value) {
                                        row = row + '<div class="card">' +
                                            '<span class="pull-right btn-danger clickable close-icon" onclick="deletecomment('+value.id+')" data-effect="fadeOut"><i class="fa fa-times"></i></span>' +
                                            '<div class="card-block">' +
                                            '<blockquote class="card-blockquote">' +
                                            '<div class="blockquote-header"></div>' +
                                            '<div class="blockquote-body"> ' + 
                                            '<table class="table table-borderless mb-0">' +
                                            '<tr>' + 
                                            '<td style="width:70px">Date</td><td>' + value.date + '</td>' +
                                            '<td rowspan="6" style="width:80px;font-size:2rem;">';
                                            
                                            if(value.file){
                                                row = row + '<a href="/storage/document?q='+ value.file +'"><i class="far fa-file"></i></a>';
                                            }
                                            
                                            row = row + '</td>'+
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Contact</td><td>' + value.contact + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Call type</td><td>' + value.call_type + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>CAM</td><td>' + value.cam_name + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Success</td><td>' + value.calsuccess + '</td>' +
                                            '</tr>' +
                                            '<tr>' + 
                                            '<td>Comment</td><td>' + value.comment + '<td>'  +
                                            '</tr>' +
                                            '</table>' +
                                            '</div>' +
                                            '<div class="blockquote-footer">'+value.cdate+'<a href="/profile/' + value.user_id + '" class="float-right">' + value.user_name + '</a></div>' +
                                            '</blockquote>' +
                                            '</div>' +
                                            '</div>';
                                    });

                                    $('.client-notes').html(row);
                                }
                            });
                        }
                    });
                }
            });
        })
    </script>
@endsection
