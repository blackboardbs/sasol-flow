@extends('flow.default')

@section('title') Opportunities @endsection

@section('header')
    <div class="container-fluid container-title float-left clear" style="margin-top: 1%;margin-bottom: 7px;">
        <h3 style="padding-bottom: 5px;">@yield('title')</h3>
        <div class="nav-btn-group" style="top:30%;padding-bottom: 5px;">
            <form autocomplete="off">
                <div class="form-row" style="flex-wrap: nowrap">
                <div class="form-group mr-1" style="display: inline-block">
                        <select name="p" id="p" class="form-control form-control-sm">
                            <option value="20" {{(isset($_GET['p']) && $_GET['p'] == '20' ? 'selected="selected"' : '')}}>20</option>
                            <option value="30" {{(isset($_GET['p']) && $_GET['p'] == '30' ? 'selected="selected"' : '')}}>30</option>
                            <option value="40" {{(isset($_GET['p']) && $_GET['p'] == '40' ? 'selected="selected"' : '')}}>40</option>
                            <option value="50" {{(isset($_GET['p']) && $_GET['p'] == '50' ? 'selected="selected"' : '')}}>50</option>
                            <option value="60" {{(isset($_GET['p']) && $_GET['p'] == '60' ? 'selected="selected"' : '')}}>60</option>
                            <option value="70" {{(isset($_GET['p']) && $_GET['p'] == '70' ? 'selected="selected"' : '')}}>70</option>
                            <option value="80" {{(isset($_GET['p']) && $_GET['p'] == '80' ? 'selected="selected"' : '')}}>80</option>
                            <option value="90" {{(isset($_GET['p']) && $_GET['p'] == '90' ? 'selected="selected"' : '')}}>90</option>
                            <option value="100" {{(isset($_GET['p']) && $_GET['p'] == '100' ? 'selected="selected"' : '')}}>100</option>
                        </select>
                    </div>
                    @if(count($supervisor_list) > 0)
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="supervisor" id="supervisor" class="form-control form-control-sm">
                            <option value="">Select Supervisor</option>
                            <option value="all">All</option>
                            @foreach($supervisor_list as $fn)
                                <option value="{{$fn->id}}" {{(isset($_GET['supervisor']) && $_GET['supervisor'] == $fn->id || $fn->id == auth()->id() ? 'selected="selected"' : '')}}>{{$fn->full_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    @if(count($users_list) > 0)
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="cam" id="cam" class="form-control form-control-sm">
                            <option value="">Select Salesperson</option>
                            <option value="all">All</option>
                            @foreach($users_list as $fn)
                                <option value="{{$fn->id}}" {{(isset($_GET['cam']) && $_GET['cam'] == $fn->id || $fn->id == auth()->id() ? 'selected="selected"' : '')}}>{{$fn->full_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    @if(count($status_list) > 0)
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="status" id="status" class="form-control form-control-sm">
                            <option value="">Select Status</option>
                            <option value="all">All</option>
                            @foreach($status_list as $key => $value)
                                <option value="{{$key}}" {{(isset($_GET['status']) && $_GET['status'] == $key ? 'selected="selected"' : '')}}>{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    @if($crm == 4)
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="fund_consultant" id="fund_consultant" class="form-control form-control-sm">
                            <option value="">Fund Consultant</option>
                            <option value="all">All</option>
                            <option value="blank">Blank</option>
                            @foreach($fund_consultants as $fn)
                                <option value="{{$fn->name}}" {{(isset($_GET['fund_consultant']) && $_GET['fund_consultant'] == $fn->name || $fn->name == auth()->user()->first_name.' '.auth()->user()->last_name ? 'selected="selected"' : '')}}>{{$fn->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mr-1" style="display: inline-block">
                        <select name="parent_branch" id="parent_branch" class="form-control form-control-sm">
                            <option value="">Parent or Branch</option>
                            <option value="Branch" {{(isset($_GET['parent_branch']) && $_GET['parent_branch'] == 'Branch' ? 'selected="selected"' : '')}}>Branch</option>
                            <option value="Parent" {{(isset($_GET['parent_branch']) && $_GET['parent_branch'] == 'Parent' ? 'selected="selected"' : '')}}>Parent</option>
                        </select>
                    </div>
                    @endif
                    <div class="form-group" style="display: inline-block">
                        <div class="input-group input-group-sm">
                            {{Form::search('q',old('query'),['class'=>'form-control form-control-sm search','placeholder'=>'Search...'])}}
                            <div class="input-group-append">
                                    <button type="submit" class="btn btn-sm btn-default" style="line-height: 1.35rem !important;"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('content')

        <div class="content-container page-content">
            <div class="row col-md-12 h-100 pr-0">
                <div class="container-fluid index-container-content h-100">
                    <div class="table-responsive d-inline-block pl-0" style="margin-right:1.7%;height: 25%;width:49%;">
                        <table class="table table-borderless table-sm table-fixed billboard-table">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Opportunity Billboard</h4></th>
                                <th class="last" nowrap style="box-shadow: none;vertical-align: top;">
                                    <a href="javascript:void(0)" onclick="composeBillboardMessage()" class="btn btn-sm btn-primary submit-btn" style="line-height: 1rem !important;">Add a message</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($messages as $message)
                                <tr class="message-{{$message["id"]}}">
                                    <td class="billboard" colspan="100%">{{--<a href="javascript:void(0)" style="display: block;" onclick="showBillboardMessage({{$message->id}})">{{$message->message}}</a>--}}
                                        <span class="pull-right clickable close-icon" onclick="completeBillboardMessage({{$message["id"]}})" data-effect="fadeOut"><input type="checkbox" /></span>
                                        <div class="card-block">
                                            <blockquote class="card-blockquote">
                                                <div class="blockquote-body" onclick="showBillboardMessage({{$message["id"]}})">@if($message["client_id"] != null) <strong>{{$message["client_name"]}}</strong> - {{$message["message"]}} @else  {{$message["message"]}} @endif</div>
                                            </blockquote>
                                        </div>
                                    </td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no messages to display.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                    <div class="table-responsive d-inline-block pl-0" style="height: 25%;width:49%;">
                            <table class="table table-borderless table-sm table-fixed task-table">
                            <thead>
                            <tr>
                                <th nowrap style="box-shadow: none;"><h4>Tasks</h4></th>
                                <th class="last" nowrap style="box-shadow: none;vertical-align: top;">
                                    <a href="javascript:void(0)" onclick="composeUserTask()" class="btn btn-sm btn-primary submit-btn" style="line-height: 1rem !important;">Add a task</a>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user_tasks as $user_task)
                                <tr class="task-{{$user_task["id"]}}">
                                    <td class="usertask" colspan="100%">{{--<a href="javascript:void(0)" style="display: block;" onclick="showBillboardMessage({{$message->id}})">{{$message->message}}</a>--}}
                                        <span class="pull-right clickable close-icon" onclick="completeUserTask({{$user_task["id"]}})" data-effect="fadeOut"><input type="checkbox" /></span>
                                        <div class="card-block">
                                            <blockquote class="card-blockquote">
                                                <div class="blockquote-body" onclick="showUserTask({{$user_task["id"]}})"><strong>{{\Carbon\Carbon::parse($user_task["task_date"])->format('Y-m-d')}}</strong> - {{$user_task["task_type"]}}<br /><small class="text-muted">{{$user_task["client_name"]}}</small></div>
                                            </blockquote>
                                        </div>
                                    </td>
                                </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no tasks to display.</small></td>
                                </tr>
                            @endforelse
                            </tbody>

                        </table>
                    </div>
                    @yield('header')
                    <div class="table-responsive w-100 float-left client-index" style="height: calc(66% - 60px);position:relative;margin-bottom:10px;">
                        @if($crm == 4)
                            <table class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 80px);margin-bottom:0px;">
                            <thead>
                            <tr>
                                <th nowrap>@sortablelink('company', 'Client Name')</th>
                                <th nowrap>@sortablelink('email', 'Fund Consultant')</th>
                                <th nowrap>@sortablelink('cell', 'Parent or Branch')</th>
                                <th nowrap>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($clients as $client)
                                    <tr>
                                        <td><a href="{{route('clients.overview',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["client_name"] ) && $client["client_name"] != ' ' && $client["client_name"] != null ? $client["client_name"]  : $client["company"])}}</a></td>
                                        <td>{{!is_null($client["fund_consultant"]) ? $client["fund_consultant"] : ''}}</td>
                                        <td>{{!is_null($client["parent_branch"]) ? $client["parent_branch"] : ''}}</td>
                                        <td class="last">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-primary" onclick="startNewApplication({{$client['id']}},{{$client['process_id']}})" title="Start a new application"><i class="fas fa-folder-plus"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showOpenApplications({{$client['id']}})" title="Open applications"><i class="fas fa-folder-open"></i> </a>
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-html="true" class="btn btn-sm btn-secondary" onclick="showClosedApplications({{$client['id']}})" title="Closed applications"><i class="fas fa-folder"></i> </a>
                                        </td>
                                    </tr >
                            @empty
                                <tr>
                                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        @else
                            <table class="table table-bordered table-hover table-sm table-fixed" style="max-height: calc(100% - 80px);margin-bottom:0px;">
                                <thead>
                                <tr>
                                    <th nowrap style="vertical-align:middle">Customer Name</th>
                                    <th nowrap style="vertical-align:middle">Contact Name</th>
                                    <th nowrap style="vertical-align:middle">Contact Number</th>
                                    <th nowrap style="vertical-align:middle">Contact Email</th>
                                    <th nowrap style="vertical-align:middle">Salesperson</th>
                                    <th nowrap style="vertical-align:middle">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($clients as $client)
                                    <tr>
                                    <td><a href="{{route('clients.overview',[$client["opportunityId"],44,234])}}">{{(isset($client["opporatorName"] ) && $client["opporatorName"] != '' ? $client["opporatorName"]  : 'Not Captured')}}</a></td>
                                        <td>{{$client["contactFirstName"]}} {{$client["contactLastName"]}}</td>
                                        <td>{{!is_null($client["contactNumber"]) ? $client["contactNumber"] : ''}}</td>
                                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                                        <td>{{$client["salesPerson"]}}</td>
                                        <td>{{$client["customerStatus"]}}</td>
                                    </tr >
                                @empty
                                    <tr>
                                        <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                                    </tr>
                                @endforelse
                                </tbody>

                            </table>

                        @endif

                    </div>
              <div style="position: relative;float:left;bottom: 0px;left: 0px;height:40px;width: 70%;text-align: right;">{{ $clients->appends($_GET)->links() }}</div>
                    <div style="position: relative;float:left;bottom: 0px;left: 0px;height:40px;width: 30%;text-align: right;"><strong>{{$clients->count()}}</strong> of <strong>{{$clients->total()}}</strong> Clients shown</div>
                 </div>

                  @include('client.modals.index')
            </div>
         </div>

@endsection
@section('extra-js')

@endsection
