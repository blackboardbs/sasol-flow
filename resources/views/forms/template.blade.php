@extends('adminlte.default')

@section('title') Edit Form @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('forms.show',$form)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{Form::open(['url' => route('forms.update',$form), 'method' => 'post','class'=>'mt-3 mb-3','files'=>true,'autocomplete' => 'off'])}}
        {{Form::hidden('form_id',$form->id,['class'=>'form-control','id'=>'form_id'])}}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name',$form->name,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name','disabled'])}}
            @foreach($errors->get('name') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('signature', 'Does this form require a signature?')}}
            {{Form::select('signature',[""=>"Please Select","0"=>"No","1"=>"Yes"],$form->signature,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'disabled'])}}
            @foreach($errors->get('signature') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>
        <div class="form-group">
            {{Form::label('template_file', 'Template File')}}
            {{Form::file('template_file',old('template_file'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '')])}}
            @foreach($errors->get('name') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        <div class="alert alert-primary mt-3">
            <div class="form-group form-inline">
                {{Form::label('section_id', 'Section Filter:')}}&nbsp;&nbsp;
                {{Form::select('section_id',$form_sections,null,['id'=>'section_id', 'class'=>'form-control form-control-sm col-sm-4 form-inline'])}}
            </div>
            <table class="table table-responsive">
                <thead>
                <tr>
                    <th>Form Section</th>
                    <th>Form Field</th>
                    <th>Variable</th>
                </tr>
                </thead>
                <tbody id="section_input" style="height: 300px;overflow-y: auto;display:block;overflow-x: hidden;">
                <tr>
                    @forelse($formfields as $sections)
                        @forelse($sections["form_section_input"] as $section)
                            @php
                                $var = strtolower(str_replace(' ','_',$sections->name)).'.'.$section["order"];
                            @endphp
                            <td width="16.66%">{{$sections->name}}</td><td width="16.66%"><i>{{$section["name"]}}</i></td><td width="16.66%">$&#123;{{$var}}&#125;</td>
                            @if(($loop->iteration)%1 == 0 && $loop->iteration != 0)
                </tr><tr>
                    @endif
                    @empty
                        <td colspan="3"><div class="alert alert-danger" style="margin-right: 1rem;">There are no inputs for this form section.</div></td>
                </tr>
                @endforelse
                @empty
                    <td colspan="3"><div class="alert alert-danger" style="margin-right: 1rem;">There are no sections for this form.</div></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        {{--{{Form::label('fields', 'Fields')}}
        <blackboard-forms-editor></blackboard-forms-editor>--}}

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
        </div>

        {{-- todo notifications --}}

        {{Form::close()}}
    </div>
@endsection
@section('extra-css')
    <style>
        table {
            flex-flow: column;
            height: 100%;
            width: 100%;
        }
        table thead {
            /* head takes the height it requires,
            and it's not scaled when table is resized */
            flex: 0 0 auto;
            width: calc(100% - 0.9em);
        }
        table tbody {
            /* body takes all the remaining available space */
            flex: 1 1 auto;
            display: block;
            overflow-y: scroll;
        }
        table tbody tr {
            width: 100%;
        }
        table thead, table tbody tr {
            display: table;
            table-layout: fixed;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $("#section_id").on('change',function (){
            let form_id = $("#form_id").val();
            let section_id = $("#section_id").val();

            $.ajax({
                url: '/forms_section/' + form_id + '/get_inputs/' + section_id,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    let rows = '<tr>';
                    let count = 0;
                    $.each(data, function (key, value) {
                        count++;
                        rows = rows + '<td width="16.66%">' + value.form_section + '</td>';
                        rows = rows + '<td width="16.66%">' + value.form_input + '</td>';
                        rows = rows + '<td width="16.66%">$&#123;' + value.form_variable + '&#125;</td>';
                        if (count === 1) {
                            rows = rows + '</tr><tr>';
                            count = 0;
                        }
                    });

                    $("#section_input").html(rows);
                }
            });
        })
    </script>
@endsection