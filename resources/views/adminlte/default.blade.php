<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Sasol</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- IonIcons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset('adminlte/dist/css/adminlte.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/blackboard.css') !!}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.css">

    <link rel="stylesheet" href="{!! asset('css/progress-wizard.min.css') !!}">
    @yield('extra-css')
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini">
<div id="app" class="wrapper">
@include('adminlte.header')
@include('adminlte.sidebar')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('header')
        <div class="row flash_msg">
            @if(Session::has('flash_success'))
                <div class="alert alert-success alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Success!</strong> {!! Session::get('flash_success')!!}
                </div>
                {{Session::forget('flash_success')}}
            @endif

            @if(Session::has('flash_info'))
                <div class="alert alert-info alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Notice.</strong> {{Session::get('flash_info')}}
                </div>
            @endif

            @if(Session::has('flash_danger'))
                <div class="alert alert-danger alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Error!</strong> {{Session::get('flash_danger')}}
                </div>
            @endif
        </div>
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside onmouseover="showScroller(this)" onmouseout="hideScroller(this)" class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-block-down">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="https://www.blackboardbs.com">www.blackboardbs.com</a>.</strong> All rights reserved.
    </footer>

    <!-- Bootsrap Modal -->

    <div class="modal fade" id="edit_email_template">
        <div class="modal-dialog" style="width:800px !important;max-width:800px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="email_id" id="email_id" >
                            <input type="hidden" class="form-control" name="activity_id" id="activity_id" >
                        </div>
                        <div class="form-group mt-3">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name','id'=>'email_title'])}}

                        </div>

                        <div class="form-group">
                            {{Form::label('Email Body')}}
                            {{ Form::textarea('email_content', null, ['class'=>'form-control my-editor','size' => '30x10','id'=>'email_content']) }}

                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                            <button type="button" onclick="saveEmailTemplate()" class="btn btn-sm btn-primary">Use Template</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="popup">
        <div class="modal-dialog" style="width:450px !important;max-width:450px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group text-center">
                        {{Form::label('popup', 'Please bring your Blackboard account up to date.')}}

                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Got it</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>


<!-- AdminLTE -->
<script src="{!! asset('adminlte/dist/js/adminlte.js') !!}"></script>
<script src="{!! asset('adminlte/dist/js/jscolor.js') !!}"></script>
<!-- OPTIONAL SCRIPTS -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="{{ mix('js/app.js') }}"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/no-data-to-display.js"></script>
<script src="https://rawgit.com/highcharts/rounded-corners/master/rounded-corners.js"></script>


<script>
    function getAuthToken() {
        $.ajax({
            url: 'https://team.blackboardbs.com/login',
            type: 'GET',
            cache: false,
            success: function (response) {
                var csfr = $(response).filter('meta[name="csrf-token"]').prop('content');
                var token = $(response).find('input[name="_token"]').val();
                $('input[name="_token"]').val(token);
                $('input[name="csrf-token"]').val(csfr)
                alert('su');
            },
            error: function () {
                //
            }
        });
    }

    function doLogin() {
        getAuthToken();
        var _token = $('input[name="_token"]').val();

        var myDate = new Date();
        myDate.setMonth(myDate.getMonth() + 12);
        var cookieName = 'XSRF-TOKEN';

        document.cookie = cookieName + "=" + _token + ";expires=" + myDate
            + ";domain=.blackboard.com;path=/";

        $.ajax({
            headers: {
                'XSRF-TOKEN': _token
            },
            url: 'https://team.blackboardbs.com/login',
            type: 'POST',
            data: {"_token": _token, "email": "jaco@blackboardbs.com", "password": "secret"},
            cache: false,
            success: function (response) {

            },
            error: function () {
                //
            },
            xhrFields: {withCredentials: true}
        });
    }

    $(function () {
        $(document).find(".execute").on('click',function(e){
            e.preventDefault();
            /*doLogin();*/
            window.location.href = "https://team.blackboardbs.com/task/kanban/52";

        })

        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<script>
    $(document).ready(function(){
        var active_elem = $(".nav-sidebar").find(".active");

        $(active_elem).parent('li').parent('ul').parent('li').addClass('menu-open');
        //$(active_elem).parent('li').parent('ul').addClass('test');
    });
</script>
<script src="{!! asset('adminlte/dist/js/custom.js') !!}"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=361nrfmxzoobhsuqvaj3hyc2zmknskzl4ysnhn78pjosbik2"></script>
<script src="{{asset('chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('chosen/docsupport/init.js')}}" type="text/javascript" charset="utf-8"></script>
<script>
    $(".delete").on("click", function(e){
        e.preventDefault();
        if (!confirm("Are you sure you want to delete this record?")){
            return false;
        } else {
            $(this).parents('form:first').submit();
        }

    });

    /*$(".deleteDoc").on('click', function(e){
        $(this).parents('form:first').submit();
    });*/
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.js"></script>
    <script>
    $(function(){
        $('[data-toggle="tooltip"]').tooltip({html:true});

        $('.chosen-select').chosen();
        $('.chosen-select').css('width', '90%');

        $(".step-dropdown").change(function(){

             var url = $('option:selected',this).data('path');
             window.location.href = url;
        });
    })

    $("#viewprocess").on('change', function () {
        let client_id = $('#client_id').val();
        let process_id = $('#viewprocess').val();
        let step_id = 0;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '/clients/getfirststep/' + client_id + '/' + process_id,
            success: function( data ) {
                window.location.href = '/clients/' + client_id + '/progress/' + process_id + '/' + data;
            }
        });
    })

    @if(auth()->id() == '1000')
    $(document).ready(function() {

        if(localStorage.getItem('popState') != 'shown'){
            $("#popup").delay(2000).modal('show');
            localStorage.setItem('popState','shown')
        }

    });
    @endif
</script>

@yield('extra-js')

</body>
</html>
