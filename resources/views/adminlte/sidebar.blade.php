  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">

      <span class="logo-lg"><img src="{!! asset('assets/blackboard_logo_gold.jpg') !!}" alt="Blackboard Logo" style="width:100%;opacity: .8"></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar blackboard-scrollbar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel pb-3 mb-3 pt-3 d-flex">
        <div class="image">
          <img src="{{route('avatar',['q'=>Auth::user()->avatar])}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{route('profile',\Illuminate\Support\Facades\Auth::id())}}" class="d-block">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{route('clients.create')}}" class="nav-link {{ (\Request::route()->getName() == 'clients.create') ? 'active' : '' }}">
              <i class="nav-icon fa fa-plus"></i>
              <p>
                Capture Client
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('recents')}}" class="nav-link {{ (\Request::route()->getName() == 'recents') ? 'active' : '' }}">
              <i class="nav-icon far fa-clock"></i>
              <p>
                Recents
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('dashboard')}}" class="nav-link {{ (\Request::route()->getName() == 'dashboard') ? 'active' : '' }}">
              <i class="nav-icon fa fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @permission('admin')
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link {{ (\Request::route()->getName() == 'reports.index') || (\Request::route()->getName() == 'custom_report.index') ? 'active' : '' }}">
              <i class="nav-icon far fa-chart-bar"></i>
              <p>
                Reports
                <i class="right fa fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('reports.assigned_actions')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.assigned_actions') ? 'active' : '' }}">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Action Report
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('reports.index')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.index') ? 'active' : '' }}">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Activity Reports
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('custom_report.index')}}" class="nav-link {{ (\Request::route()->getName() == 'custom_report.index') ? 'active' : '' }}">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Custom Reports
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('reports.audit_report')}}" class="nav-link {{ (\Request::route()->getName() == 'reports.audit_report') ? 'active' : '' }}">
                  <i class="nav-icon fas fa-circle"></i>
                  <p>
                    Audit Report
                  </p>
                </a>
              </li>
            </ul>
          </li>
          @endpermission
          @permission('maintain_client')
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link {{ (\Request::route()->getName() == 'clients.index') ? 'active' : '' }}">
              <i class="nav-icon far fa-address-card"></i>
              <p>
                Clients
                <i class="right fa fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              @auth
                {{--<li class="nav-item">
                  <a href="{{route('clients.index')}}" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>All</p>
                  </a>
                </li>--}}
                @if($sidebar_process_statuses)
                  @foreach($sidebar_process_statuses as $sidebar_process)
                    {{--<li class="nav-item">
                      <a href="{{route('clients.index')}}?step={{$key}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>{{$value}}</p>
                      </a>
                    </li>--}}
                    <li class="nav-item has-treeview">
                      @if($sidebar_process["steps"] != null && count($sidebar_process["steps"]) > 0)
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-circle"></i>
                          <p>
                            {{$sidebar_process["name"]}}
                            <i class="right fa fa-angle-right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          @auth
                            @foreach($sidebar_process["steps"] as $key => $value)
                              <li class="nav-item">
                                <a href="{{route('clients.index')}}?step={{$key}}&p={{$sidebar_process["id"]}}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>{{$value}}</p>
                                </a>
                              </li>
                            @endforeach
                          @if($sidebar_process["default"] == 1)
                              <li class="nav-item">
                                <a href="{{route('clients.index')}}?step=1000&p={{$sidebar_process["id"]}}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Converted</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{route('clients.index')}}?step=1001&p={{$sidebar_process["id"]}}" class="nav-link">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Not Progressing</p>
                                </a>
                              </li>
                            @endif
                          @endauth
                        </ul>
                      @else
                        <a href="{{route('processes.show',$sidebar_process["id"])}}" class="nav-link">
                          <i class="nav-icon fas fa-circle"></i>
                          <p>
                            {{$sidebar_process["name"]}}
                          </p>
                        </a>
                      @endif
                    </li>
                  @endforeach
                @endif
              @endauth
            </ul>
          </li>
          @endpermission
          @permission('maintain_document')
          <li class="nav-item has-treeview">
            <a href="{{route('documents.index')}}" class="nav-link {{ (\Request::route()->getName() == 'documents.index') ? 'active' : '' }}">
              <i class="nav-icon far fa-file-alt"></i>
              <p>
                Documents
              </p>
            </a>
          </li>
          @endpermission
          @permission('maintain_template')
          <li class="nav-item has-treeview">
            <a href="{{route('templates.index')}}" class="nav-link {{ (\Request::route()->getName() == 'templates.index') ? 'active' : '' }}">
              <i class="nav-icon far fa-file"></i>
              <p>
                Templates
              </p>
            </a>
          </li>
          @endpermission
          @permission('admin')
          <li class="header">ADMIN</li>
          @permission('process_editor')
          <li class="nav-item has-treeview">
            <a href="{{route('processes.index')}}" class="nav-link {{ (\Request::route()->getName() == 'processes.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-tasks"></i>
              <p>
                Processes
              </p>
            </a>
          </li>
          @endpermission
          <li class="nav-item has-treeview">
            <a href="{{route('forms.index')}}" class="nav-link {{ (\Request::route()->getName() == 'forms.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Forms
              </p>
            </a>
          </li>
          @permission('actions')
          <li class="nav-item has-treeview">
            <a href="{{route('action.index')}}" class="nav-link {{ (\Request::route()->getName() == 'action.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-project-diagram"></i>
              <p>
                Actions
              </p>
            </a>
          </li>
          @endpermission
          <li class="nav-item has-treeview">
            <a href="{{ route('users.index')}}" class="nav-link {{ (\Request::route()->getName() == 'users.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('locations.index')}}" class="nav-link {{ (\Request::route()->getName() == 'locations.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-globe"></i>
              <p>
                Locations
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('roles.index') }}" class="nav-link {{ (\Request::route()->getName() == 'roles.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Roles
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{route('emailtemplates.index')}}" class="nav-link {{ (\Request::route()->getName() == 'emailtemplates.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Email Templates
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('configs.index')}}" class="nav-link {{ (\Request::route()->getName() == 'configs.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-cogs"></i>
              <p>
                Configs
              </p>
            </a>
          </li>
          @endpermission
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
