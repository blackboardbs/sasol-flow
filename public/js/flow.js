$(document).ready(function(){


    var active_elem = $(".nav-sidebar").find(".active");

    $(active_elem).parent('li').parent('ul').parent('li').addClass('menu-open');
    //$(active_elem).parent('li').parent('ul').addClass('test');

    $(".delete").on("click", function(e){
        e.preventDefault();
        if (!confirm("Are you sure you want to delete this record?")){
            return false;
        } else {
            $(this).parents('form:first').submit();
        }

    });

    $('#admin-menu').on('click',function(e){
        //alert();
        $(".nav-sidebar").find('li.admin-menu').toggle();
    })

    //show overlay

    $('[data-toggle="tooltip"]').tooltip({
        items: "[data-original-title]",
        content: function() {
            var element = $( this );
            if ( element.is( "[data-original-title]" ) ) {
                return element.attr( "title" );
            }
        },
        position: {
            my: "center bottom-10", // the "anchor point" in the tooltip element
            at: "center top", // the position of that anchor point relative to selected element
            using: function( position, feedback ) {
                $( this ).css( position );
                $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
            }
        }
    });



    $(".step-dropdown").change(function(){

        var url = $('option:selected',this).data('path');
        window.location.href = url;
    });

    $("#viewprocess").on('change', function () {
        let client_id = $('#client_id').val();
        let process_id = $('#viewprocess').val();
        let step_id = 0;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: '/clients/getfirststep/' + client_id + '/' + process_id,
            success: function( data ) {
                window.location.href = '/clients/' + client_id + '/progress/' + process_id + '/' + data;
            }
        });
    });

    $('.confirmExtraEmail').on('keypress',function(e){
        let email = $('#confirmEmailModal').find('.confirmExtraEmail').val();
        if(e.which === 13) {
            if (validateEmail(email)) {
                $('#confirmEmailModal').find('.confirmExtraEmail').removeClass('is-invalid');
                $('#confirmEmailModal').find('#confirmEmails').append('<li>' + email + '</li>');
                $('#confirmEmailModal').find('.all-emails').append('<input type="hidden" name="extra-emails[]" value="' + email + '">');
                $('#confirmEmailModal').find('.confirmExtraEmail').val('');
            } else {
                $('#confirmEmailModal').find('.confirmExtraEmail').addClass('is-invalid');
            }
        }
    });

    $('#changeprocesscancel').on('click',function(){
        $('#modalChangeProcess').modal('hide');
    });

    $('input[type=radio][name=clientf]').change(function() {
        $(this).closest('form').submit();
    });

    $('#getApplicationDoc').on('click',function(){

        let i = 0;
        let cnt = $("#modalAllProcesses").find('.signature_checkbox:checked').length;
        let row = '';

        $('#overlay').show();

        function next(clientid,processid){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });

            console.log('/api/signiflow/getsigniflowdocument/'+clientid+'/1/'+processid);

            $.ajax({
                url: '/api/signiflow/getsigniflowdocument/'+clientid+'/1/'+processid,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if(data.success == 0){
                        alert(data.error_message);
                    }

                    cnt = cnt - 1;
//console.log(data);
                    row = row + '<tr>';
                    // row = row + '<td>&bull;</td><td>'+data.message+'</td>';
                    //row = row + '<td>&bull;</td><td><a href="/storage/documents/processed_applications/'+clientid+'/'+data+'" style="padding-left:10px">'+data+'</a></td>';
                    row = row + '</tr>';
                    if(i === cnt) {
                        // run function here as its the last item in array
                        $('#overlay').hide();
                        /*$("#modalAllProcesses").modal('hide');*/
                        let process_id = $("#modalAllProcesses").find('#all_processes_process_id').val();
                        let step_id = $("#modalAllProcesses").find('#all_processes_step_id').val();

                        $("#modalAllProcesses").find('.modal-title').html('Sent for Signatures');
                        $("#modalAllProcesses").find('.instruction').html('All applications were successfullly submitted for signatures.<br />You can view them by navigating to the client documents tab by clicking <a href="/clients/'+clientid+'/documents/'+process_id+'/'+step_id+'/0"><strong>here</strong></a>.');
                        $("#modalAllProcesses").find('.btn-div').hide();
                        $("#modalAllProcesses").find('#all_processes').html(row);
                    }
                },
                error: function (data) {
                    alert('An error occurred while generating document(s): ' + data.statusText);
                    console.log('Error:', data);
                }
            });
        }

        /*function next(clientid,processid){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });

            $.ajax({
                url: '/clients/submit_for_signature/' + clientid + '/' + processid,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    cnt = cnt - 1;

                    row = row + '<tr>';
                    row = row + '<td>&bull;</td><td><a href="/storage/documents/processed_applications/'+clientid+'/'+data+'" style="padding-left:10px">'+data+'</a></td>';
                    row = row + '</tr>';
                    if(i === cnt) {
                        // run function here as its the last item in array
                        $('#overlay').hide();
                        //$("#modalAllProcesses").modal('hide');
                        let process_id = $("#modalAllProcesses").find('#all_processes_process_id').val();
                        let step_id = $("#modalAllProcesses").find('#all_processes_step_id').val();

                        $("#modalAllProcesses").find('.modal-title').html('Sent for Signatures');
                        $("#modalAllProcesses").find('.instruction').html('All applications were successfullly submitted for signatures.<br />You can view them by clicking on the filename below or by navigating to the client documents tab by clicking <a href="/clients/'+clientid+'/documents/'+process_id+'/'+step_id+'"><strong>here</strong></a>.');
                        $("#modalAllProcesses").find('.btn-div').hide();
                        $("#modalAllProcesses").find('#all_processes').html(row);
                    }
                },
                error: function (data) {

                }
            });
        }*/

        $("#modalAllProcesses").find('.signature_checkbox:checked').each(function () {
            let clientid = $(this).data('client');
            let processid = $(this).data('process');

            if(i === cnt) {
                // run function here as its the last item in array
            } else {
                // do the next ajax call
                next(clientid,processid);
                // alert(clientid + ' - ' + processid);
            }
        });


        /**/
    });

    $('#cam').on('change',function(){
        $('#overlay').show();
        $(".client-index").hide();
        $( this ).closest( "form" ).submit();
    });

    $('#status').on('change',function(){
        $('#overlay').show();
        $(".client-index").hide();
        $( this ).closest( "form" ).submit();
    });

    $('#supervisor').on('change',function(){
        $('#overlay').show();
        $(".client-index").hide();
        $( this ).closest( "form" ).submit();
    });

    $('#p').on('change',function(){
        $('#overlay').show();
        $(".client-index").hide();
        $( this ).closest( "form" ).submit();
    });

});

(function($) {

    $(".cata-sub-nav").on('scroll', function() {
        $val = $(this).scrollLeft();

        if($(this).scrollLeft() + $(this).innerWidth()>=$(this)[0].scrollWidth){
            $(".nav-next").hide();
        } else {
            $(".nav-next").show();
        }

        if($val == 0){
            $(".nav-prev").hide();
        } else {
            $(".nav-prev").show();
        }
    });
    var w = $('.sidebar').outerWidth(true);

    $(".nav-next").on("click", function(){

        $(".cata-sub-nav").animate( { scrollLeft: '+=460' }, 200);
    });
    $(".nav-prev").on("click", function(){
        $(".cata-sub-nav").animate( { scrollLeft: '-=460' }, 200);
    });
})(jQuery);

function validateEmail(sEmail) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(sEmail)) {
        return false;
    }else{
        return true;
    }
}

function confirmDialog(message, onConfirm){
    var fClose = function(){
        modal.modal("hide");
    };

    var fClose2 = function(){
        modal.modal("hide");
        if ($(document).find('#modalSendTemplate').hasClass('show')) {
            $('#modalSendTemplate').find('#sendtemplatecomposeemailsend').attr("disabled", false);
            $('#modalSendTemplate').find('.sendtemplatecancel').attr("disabled", false);
            $('#modalSendTemplate').find('#sendcomposemessage').html('');
            $('#modalSendTemplate').find('#sendtemplatetemplateemailsend').attr("disabled", false);
            $('#modalSendTemplate').modal('hide');
        }
        if ($(document).find('#modalSendDocument').hasClass('show')) {
            $('#modalSendDocument').find('#senddocumentcomposeemailsend').attr("disabled", false);
            $('#modalSendDocument').find('.senddocumentcancel').attr("disabled", false);
            $('#modalSendDocument').find('#sendcomposemessaged').html('');
            $('#modalSendDocument').find('#senddocumenttemplateemailsend').attr("disabled", false);
            $('#modalSendDocument').modal('hide');
        }
        if ($(document).find('#modalSendMA').hasClass('show')) {
            $('#modalSendMA').find('#sendmatemplateemailsend').attr("disabled", true);
            $('#modalSendMA').find('.sendmacancel').attr("disabled", true);
            $('#modalSendMA').find('#sendmamessage').html('');
            $('#modalSendMA').find('#sendmacomposeemailsend').attr("disabled", false);
            $('#modalSendMA').modal('hide');
        }
    };

    var modal = $("#confirmModal");
    modal.modal("show");
    $("#confirmMessage").empty().append(message);
    $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
    $("#confirmCancel").unbind().one("click", fClose2);
}

function confirmEmailDialog(message, client_id, email, onConfirm){
    var fClose = function(){
        modal.modal("hide");
    };

    var modal = $("#confirmEmailModal");
    modal.modal("show");

    if(email.length > 0){
        $("#confirmEmails").empty().append('<li>'+email+'</li>');
    }

    $('.all-emails').append('<input type="hidden" name="extra-emails[]" value="'+email+'" />');
    $("#confirmEmailClient").val(client_id);
    $("#confirmEmailMessage").empty().append(message);

    $("#confirmEmailOk").unbind().one('click', onConfirm).one('click', fClose);
    $("#confirmEmailCancel").unbind().one("click", fClose);
}

function notifyDialog(message){
    var fClose = function(){
        modal.modal("hide");
    };

    var modal = $("#notifyModal");
    modal.modal("show");
    $("#notifyMessage").empty().append(message);
    $("#notifyOk").unbind().one('click', fClose);
}

function completePrimary(client_id, url) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: {client_id: client_id},
        success: function (data) {
            if (data.message === 'Success') {
                let YOUR_MESSAGE_STRING_CONST = "Are you sure you want to complete the instruction for this Primary Client?";

                confirmDialog(YOUR_MESSAGE_STRING_CONST, function () {

                    $.ajax({
                        type: "POST",
                        url: '/client/complete/' + client_id,
                        data: {client_id: client_id},
                        success: function (data) {
                            if (data.message === 'Success') {

                                //$('.flash_msg').html('<div class="alert alert-success">Primary Client successfully completed</div>');
                            } else {
                                $('.flash_msg').html('<div class="alert alert-danger">An error occured while trying to complete the instruction.</div>');
                            }

                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }
                    });
                });
            } else {
                let YOUR_MESSAGE_STRING_CONST = "Not all required fields have been captured.";

                notifyDialog(YOUR_MESSAGE_STRING_CONST);
            }
        }
    });
}

function sendClientEmail(client_id, client_email) {

    let YOUR_MESSAGE_STRING_CONST;

    if(client_email.length > 0) {
        YOUR_MESSAGE_STRING_CONST = "Are you sure you want to send an email to the following recipients?";
    } else {
        YOUR_MESSAGE_STRING_CONST = "";
    }


    confirmEmailDialog(YOUR_MESSAGE_STRING_CONST, client_id, client_email, function () {
        var emails = $('input[name="extra-emails[]"]').map(function(){
            return this.value;
        }).get();

        var process_id = $('#process_id').val();
        var step_id = $('#step_id').val();

        $('#overlay').fadeIn();

        $.ajax({
            type: "POST",
            url: '/client/' + client_id +'/sendclientemail',
            data: {client_id: client_id,emails:emails,process_id:process_id,step_id:step_id},
            success: function (data) {
                if (data.message === 'Success') {
                    toastr.success('<strong>Success!</strong> '+data.success_msg);

                    toastr.options.timeOut = 1000;
                } else {
                    toastr.error('<strong>Error!</strong> An error occured while trying to send the email.');

                    toastr.options.timeOut = 1000;
                }
                $('.all-emails').empty().append('<ul id=\'confirmEmails\'>\n' +
                    '\n' +
                    '                        </ul>');
                /*setTimeout(function () {
                    window.location.reload();
                }, 2000);*/
                $('#overlay').fadeOut();
            }
        });
    });
}

function submitForSignatures(client_id, process_id, step_id){

    let clientid = client_id;
    let processid = process_id;
    let stepid = step_id;

    $("#modalAllProcesses").find('.modal-title').html('Send for Signatures');
    $("#modalAllProcesses").find('.instruction').html('Please select the applications you would like to submit for signatures.');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/clients/' + client_id + '/get_forms',
        type: "GET",
        dataType: "json",
        success: function (data) {
            $("#modalAllProcesses").modal('show');

            let row = '';
            $.each(data, function(key, value) {
                row = row + '<tr><td style="vertical-align: top;padding-bottom: 7px;"><input type="checkbox" class="signature_checkbox" name="signature_checkbox[]"  data-client="'+client_id+'" data-process="'+value.process_id+'" data-href="clients/submit_for_signature/'+ client_id + '/'+value.process_id+'" /></td><td style="padding-left: 1rem;padding-bottom: 7px;">' + value.name + '</td></tr>';
                /*row = row + '<li><a href="javascript:void(0)" onclick="getApplicationDoc('+client_id+','+value.process_id+')" data-client-id="'+client_id+'" data-href="clients/submit_for_signature/'+ client_id + '/'+value.process_id+'">' + value.name + '</a></li>';*/
            });

            $("#modalAllProcesses").find('#all_processes_process_id').val(processid);
            $("#modalAllProcesses").find('#all_processes_step_id').val(stepid);
            $("#modalAllProcesses").find('.btn-div').show();
            $("#modalAllProcesses").find('#all_processes').html(row);
        }
    });
}

function startNewApplication(client_id,process_id) {
    let clientid = client_id;
    let processid = process_id;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/clients/getnewprocesses/'+clientid,
        type:"GET",
        dataType:"json",
        success:function(data){
            /*$('#modalSendTemplate').modal('hide');*/
            $("#modalChangeProcess").modal('show');
            $("body").find('#move_to_process_new').empty();
            $("#modalChangeProcess").find('.client_id').val(client_id);
            $("#modalChangeProcess").find('.process_id').val(process_id);

            $.each(data, function(key, value) {
                $("#modalChangeProcess").find('#move_to_process_new').append($("<optgroup></optgroup>").attr("label",key).attr("id",key.replace(' ','').toLowerCase()));
                $.each(value, function(k, v) {
                    if(v.existing === '1'){

                    } else {
                        $("#modalChangeProcess").find('#' + key.replace(' ', '').toLowerCase()).append($("<option></option>").attr("value", v.id).text(v.name));
                    }
                });
            });

            $("body").find('#move_to_process_new').trigger('chosen:updated');

        }
    });
}

function startNewForm(client_id,process_id) {
    let clientid = client_id;
    let processid = process_id;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/clients/getnewform/'+clientid,
        type:"GET",
        dataType:"json",
        success:function(data){
            /*$('#modalSendTemplate').modal('hide');*/
            $("#modalChangeForm").modal('show');
            $("#modalChangeForm").find('#move_to_form_new').empty();
            $("#modalChangeForm").find('.client_id').val(client_id);
            $("#modalChangeForm").find('.process_id').val(process_id);

            $.each(data, function(key, value) {
                $("#modalChangeForm").find('#move_to_form_new').append($("<optgroup></optgroup>").attr("label",key).attr("id",key.replace(' ','').toLowerCase()));
                $.each(value, function(k, v) {
                    if(v.existing === '1'){

                    } else {
                        $("#modalChangeForm").find('#' + key.replace(' ', '').toLowerCase()).append($("<option></option>").attr("value", v.id).text(v.name));
                    }
                });
            });

            $("body").find('#move_to_form_new').trigger('chosen:updated');

        }
    });
}

function showOpenApplications(client_id){
    let clientid = client_id;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/clients/' + client_id + '/current_applications',
        type: "GET",
        dataType: "json",
        success: function (data) {
            $("#modalCurrentProcesses").modal('show');

            let row = '';
            $.each(data, function(key, value) {
                row = row + '<li><a href="clients/'+ client_id + '/progress/'+value.process_id+'/'+value.step_id+'">' + value.name + '</a></li>';
            });

            $("#modalCurrentProcesses").find('#current_processes').html(row);
        }
    });
}

function showClosedApplications(client_id){
    let clientid = client_id;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/clients/' + client_id + '/closed_applications',
        type: "GET",
        dataType: "json",
        success: function (data) {
            $("#modalClosedProcesses").modal('show');

            let row = '';
            $.each(data, function(key, value) {
                row = row + '<li><a href="clients/'+ client_id + '/progress/'+value.process_id+'/'+value.step_id+'">' + value.name + '</a></li>';
            });

            $("#modalClosedProcesses").find('#closed_processes').html(row);
        }
    });
}

function clientBasketGlobalSelectAll(){

    if($(".client-basket-select-all-all").prop('checked')){

        $(document).find(".client-basket .card").each(function () {
            let name = $(this).attr("data-name");

            $("#" + name).prop('checked', true);
            $(".select-this-" + name).prop('checked',true);
        });
    } else {
        $(".client-basket-select-all-all").prop('checked', false);
        $(".client-basket .card").each(function () {
            let name = $(this).attr("data-name");

                $("#" + name).prop('checked', false);
                $(".select-this-" + name).prop('checked',false);
        });
    }

}

function clientBasketSelectAll(section){
    if($(document).find("#" + section).prop('checked')){

        $("#" + section).prop('checked', true);
        $("#" + section).each(function(){

                $(".select-this-" + section).prop('checked',true);
        });
    } else {
        $("#" + section).prop('checked', false);
        $("#" + section).each(function(){

            $(".select-this-" + section).prop('checked',false);
        });
    }
    //e.stopPropagation();
    $(".client-basket .card").each(function(){
        const name = $(this).attr("data-name");

        let total = $(document).find('#' + name).length;
        let total_selected = $(document).find('#' + name + ':checked').length;

        if(total === total_selected){
            $(".client-basket-select-all-all").prop('checked',true);
        } else {
            $(".client-basket-select-all-all").prop('checked',false);
        }
    });
};

function clientBasketSelect(section){
    let total = $(document).find('.select-this-' + section).length;
    let total_selected = $(document).find('.select-this-' + section + ':checked').length;

    if(total === total_selected){
        $("#" + section).prop('checked',true);
        $(".client-basket-select-all-all").prop('checked', false);
        let total = $(document).find('#' + section).length;
        let total_selected = $(document).find('#' + section + ':checked').length;

        if(total === total_selected){
            $(".client-basket .card").each(function(){
                const name = $(this).attr("data-name");

                let total = $(document).find('#' + name).length;
                let total_selected = $(document).find('#' + name + ':checked').length;

                if(total === total_selected){
                    $(".client-basket-select-all-all").prop('checked',true);
                } else {
                    $(".client-basket-select-all-all").prop('checked',false);
                }
            });
        } else {
            $(".client-basket-select-all-all").prop('checked', false);
        }
    } else {
        $("#" + section).prop('checked',false);
        $(".client-basket-select-all-all").prop('checked', false);
    }
};

$(document).ready(function () {

    $('[data-toggle="collapse"]').on('click',function(e){
        if ( $(this).parents('.accordion').find('.collapse.show') ){
            var idx = $(this).index('[data-toggle="collapse"]');
            if (idx == $('.collapse.show').index('.collapse')) {
                // prevent collapse
                //e.stopPropagation();
            }
        }
    });

    $('.search').on('search',function(){
        $('#overlay').fadeIn();
        $(this).closest('form').submit();
    });

    //open move to process modal

    $('#modalChangeProcess').on('hidden.bs.modal', function () {
        $('#modalChangeProcess').find('#changeprocessradio_msg').html('');
        $('#modalChangeProcess').find('#move_to_process_new_msg').html('');
        $('#modalChangeProcess').find('.client_id').val('');
        $('#modalChangeProcess').find('.process_id').val('');
        $('#modalChangeProcess').find('#move_to_process_new').removeClass('is-invalid');
    });

    //move to process depending on radio selection
    $('#changeprocesssave').on('click', function () {
        let err = 0;

        if ($('#modalChangeProcess').find('#move_to_process_new').val() === '0') {
            err++;
            $('#modalChangeProcess').find('#move_to_process_new').addClass('is-invalid');
            $('#modalChangeProcess').find('#move_to_process_new_msg').html('<span style="color: red;">Please select a application.</span>');
        }


        let process_action = 'keep';
        //get value of radio button in modal
        if (err === 0) {
            $('#overlay').show();
            $('#modalChangeProcess').find('#changeprocessradio_msg').html('');
            $('#modalChangeProcess').find('#move_to_process_new_msg').html('');

            if (process_action === 'keep') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let client_id = $('#modalChangeProcess').find('.client_id').val();
                let process_id = $('#modalChangeProcess').find('.process_id').val();
                let new_process_id = $('#modalChangeProcess').find('#move_to_process_new').val();

                $.ajax({
                    url: '/clients/' + client_id + '/keep_process/' + process_id + '/' + new_process_id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $("#modalChangeProcess").modal('hide');

                        window.location.href = '/clients/' + client_id + '/progress/' + new_process_id + '/' + data.new_step_id + '/0';

                    }
                });
            }
        }
    })

    $('#changeformsave').on('click', function () {
        let err = 0;

        if ($('#modalChangeForm').find('#move_to_form_new').val() === '0') {
            err++;
            $('#modalChangeForm').find('#move_to_form_new').addClass('is-invalid');
            $('#modalChangeForm').find('#move_to_form_new_msg').html('<span style="color: red;">Please select a form.</span>');
        }


        let process_action = 'keep';
        //get value of radio button in modal
        if (err === 0) {
            $('#overlay').show();
            $('#modalChangeForm').find('#changeprocessradio_msg').html('');
            $('#modalChangeForm').find('#move_to_form_new_msg').html('');

            if (process_action === 'keep') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let client_id = $('#modalChangeForm').find('.client_id').val();
                let process_id = $('#modalChangeForm').find('.process_id').val();
                let new_process_id = $('#modalChangeForm').find('#move_to_form_new').val();

                $.ajax({
                    url: '/clients/' + client_id + '/keep_process/' + process_id + '/' + new_process_id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $("#modalChangeForm").modal('hide');

                        window.location.href = '/clients/' + client_id + '/progress/' + new_process_id + '/' + data.new_step_id + '/1';

                    }
                });
            }
        }
    })

    $('.submitModal').on('click',function () {
        //$(this).closest(".modal-footer").html('');
         $(this).closest(".modal-footer").prev(".modal-body").find(".clientdetailsform").submit();
    })
})


/*Client Details*/
function toggelClientBasket(){
        $('.client-basket').stop(true, true).toggle('slide', {
            direction: 'left',
        }, 750);

    $(".client-basket .card").each(function(){
        const name = $(this).attr("data-name");

        let total = $(document).find('.select-this-' + name).length;
        let total_selected = $(document).find('.select-this-' + name + ':checked').length;

        if(total === total_selected){
            $("#" + name).prop('checked',true);
            $(".client-basket .card").each(function(){
                const name = $(this).attr("data-name");

                let total = $(document).find('.select-this-' + name).length;
                let total_selected = $(document).find('.select-this-' + name + ':checked').length;

                if(total === total_selected){
                    $(".client-basket-select-all-all").prop('checked',true);
                } else {
                    $(".client-basket-select-all-all").prop('checked',false);
                }
            });
        } else {
            $("#" + name).prop('checked',false);
            $(".client-basket-select-all-all").prop('checked',false);
        }
    });
}

function composeMessage(client_id,process_id,step_id){

    let processid;
    let stepid;

    if($('#process_id').length){
        processid = $('#process_id').val();
    } else {
        processid = process_id;
    }

    if($('#step_id').length){
        stepid = $('#step_id').val();
    } else {
        stepid = step_id;
    }

    $.ajax({
        url: '/messages/create/' + client_id + '/' + processid + '/' + stepid,
        type: "GET",
        dataType: "json",
        success: function (data) {
            $('#modalSendMessage').modal('show');
            $('#modalSendMessage').find('.modal-title').html("Send Message");
            if(data.subject === 0){
                $('#modalSendMessage').find('#message_subject').hide();
            } else {
                $('#modalSendMessage').find('#message_subject').show();
            }

            tinymce.init(editor_config);
        }
    });
}

function composeMail(client_id){
    $('#modalSendMail').modal('show');
    tinymce.init(editor_config);
}

function sendMail(){
    
    $('#send_mail_form').submit();
    $('#modalSendMail').modal('hide');
}

function composeBillboardMessage(){
    $("#modalBillboardMessage").find(".billboard_client").val('');
    $("#modalBillboardMessage").find(".billboard_client").val('').trigger('chosen:updated');
    $("#modalBillboardMessage").find(".billboard_message").val('');
    $("#modalBillboardMessage").modal('show');
}

function showTaskOther() {
    if(($("#modalUserTask").data('bs.modal') || {})._isShown ){
        if($("#modalUserTask").find('.task_type').find("option:selected").text() === "Other"){
            $("#modalUserTask").find('.task_other_div').css('display','block');
            $("#modalUserTask").find('.task_other').focus();
        } else {
            $("#modalUserTask").find('.task_other_div').css('display','none ');
        }
    }
    if(($("#modalEditUserTask").data('bs.modal') || {})._isShown ){
        if($("#modalEditUserTask").find('.task_type').find("option:selected").text() === "Other"){
            $("#modalEditUserTask").find('.task_other_div').css('display','block');
            $("#modalEditUserTask").find('.task_other').focus();
        } else {
            $("#modalEditUserTask").find('.task_other_div').css('display','none ');
        }
    }
}
function composeUserTask(){
    // Check if the user is signed in to Azure


    $("#modalUserTask").find(".task_client").val('');
    $("#modalUserTask").find(".task_client").val('').trigger('chosen:updated');
    $("#modalUserTask").find(".task_message").val('');
    $("#modalUserTask").find(".task_other_div").css('display','none');
    $("#modalUserTask").modal('show');
}

function saveBillboardMessage(){
    let client = $("#modalBillboardMessage").find('.billboard_client').val();
    let message = $("#modalBillboardMessage").find('.billboard_message').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/billboard-message/save',
        type:"POST",
        data:{billboard_message:message,billboard_client:client},
        success:function(data){
            var rowCount = $('.billboard-table tr').length;

            $("#modalBillboardMessage").find(".billboard_client").val();
            $("#modalBillboardMessage").find(".billboard_message").val('');
            $("#modalBillboardMessage").modal('hide');

            toastr.success('<strong>Success!</strong> Message was successfully saved.');

            toastr.options.timeOut = 1000;

            if(rowCount > 1) {
                if(data.client === '') {
                    $('<tr class="message-' + data.message_id + '">' +
                        '<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')">' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>' +
                        '</tr>').prependTo(".billboard-table > tbody");
                } else {
                    $('<tr class="message-' + data.message_id + '">' +
                        '<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')"><strong>' + data.client + '</strong> - ' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>' +
                        '</tr>').prependTo(".billboard-table > tbody");
                }
            }

            if($(".billboard-table .text-center").is(':visible')){
                $(".billboard-table > tbody").html('');
                if(data.client === '') {
                    $('<tr class="message-' + data.message_id + '">' +
                        '<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')">' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>' +
                        '</tr>').prependTo(".billboard-table > tbody");
                } else {
                    $('<tr class="message-' + data.message_id + '">' +
                        '<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')"><strong>' + data.client + '</strong> - ' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>' +
                        '</tr>').prependTo(".billboard-table > tbody");
                }
            }
        }
    });
}

function saveUserTask(){
    let client = $("#modalUserTask").find('.task_client').val();
    let message = $("#modalUserTask").find('.task_message').val();
    let date = $("#modalUserTask").find('.task_date').val();
    let type = $("#modalUserTask").find('.task_type').val();
    let other = $("#modalUserTask").find('.task_other').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/user-task/save',
        type:"POST",
        data:{task_message:message,task_client:client,task_type:type,task_date:date,task_other:other},
        success:function(data){
            var rowCount = $('.task-table tr').length;

            $("#modalUserTask").find(".task_client").val('').trigger('chosen:updated');
            $("#modalUserTask").find(".task_type").val('').trigger('chosen:updated');
            $("#modalUserTask").find(".task_date").val('');
            $("#modalUserTask").find(".task_other").val('');
            $("#modalUserTask").find(".task_message").val('');
            $("#modalUserTask").modal('hide');

            toastr.success('<strong>Success!</strong> Task was successfully saved.');

            toastr.options.timeOut = 1000;

            if(rowCount > 1) {
                if(data.client === '') {
                    if(data.task_type === 'Other') {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    } else {
                        $('.task-' + data.task_id).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '<br /><small class="text-muted">' + data.client + '</small></div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    }
                } else {
                    if(data.task_type === 'Other') {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    } else {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    }
                }
            }

            if($(".task-table .text-center").is(':visible')){
                $(".task-table > tbody").html('');
                if(data.client === '') {
                    if(data.task_type === 'Other') {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    } else {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    }
                } else {
                    if(data.task_type === 'Other') {
                        $('<tr class="task-' + data.task_id + '">' +
                            '<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>' +
                            '</tr>').prependTo(".task-table > tbody");
                    } else {
                        $('.task-' + data.task_id).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '<br /><small class="text-muted">' + data.client + '</small></div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    }
                }
            }
        }
    });
}

function showBillboardMessage(msgid) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/billboard-message/'+msgid+'/show',
        type: "GET",
        success: function (data) {
            $('#modalShowBillboardMessage').modal('show');
            $('#modalShowBillboardMessage').find('.message_id').val(data.message_id);
            if(data.client === ''){
                $('#modalShowBillboardMessage').find('.billboard_client').html('<i>No client found.</i>');
            } else {
                $('#modalShowBillboardMessage').find('.billboard_client').html(data.client);
            }
            if(data.billboard_message === ''){
                $('#modalShowBillboardMessage').find('.billboard_message').html('<i>No message found.</i>');
            } else {
                $('#modalShowBillboardMessage').find('.billboard_message').html(data.billboard_message);
            }
        }
    })
}


function showUserTask(taskid) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/user-task/'+taskid+'/show',
        type: "GET",
        success: function (data) {
            $('#modalShowUserTask').modal('show');
            $('#modalShowUserTask').find('.task_id').val(data.task_id);
            if(data.client === ''){
                $('#modalShowUserTask').find('.task_client').html('<i>No client found.</i>');
            } else {
                $('#modalShowUserTask').find('.task_client').html(data.client);
            }
            if(data.task_type === ''){
                $('#modalShowUserTask').find('.task_type').html('<i>No task type found.</i>');
            } else {
                $('#modalShowUserTask').find('.task_type').html(data.task_type);
            }
            if(data.task_type === 'Other'){
                $('#modalShowUserTask').find('.task_other_div').css('display','block');
                $('#modalShowUserTask').find('.task_other_div').html(data.task_other);
            } else {
                $('#modalShowUserTask').find('.task_other_div').css('display','none');
            }
            if(data.task_date === ''){
                $('#modalShowUserTask').find('.task_date').html('<i>No task date found.</i>');
            } else {
                $('#modalShowUserTask').find('.task_date').html(data.task_date);
            }
            if(data.task_message === ''){
                $('#modalShowUserTask').find('.task_message').html('<i>No message found.</i>');
            } else {
                $('#modalShowUserTask').find('.task_message').html(data.task_message);
            }
        }
    })
}

function completeBillboardMessage(msgid) {
    if (!confirm("Are you sure you want to mark this record as complete?")){
        return false;
    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/billboard-message/' + msgid + '/complete',
            type:"GET",
            success:function(data){
                $('.message-'+msgid).remove();
                toastr.success('<strong>Success!</strong> Record was successfully marked as complete.');

                toastr.options.timeOut = 1000;

                var rowCount = $('.billboard-table tr').length;

                if(rowCount > 1){

                } else {
                    $('.billboard-table tbody').html('<tr>' +
                        '<td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no messages to display.</small></td>' +
                        '</tr>');
                }

            }
        });
    }
}

function completeUserTask(taskid) {
    if (!confirm("Are you sure you want to mark this record as complete?")){
        return false;
    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/user-task/' + taskid + '/complete',
            type:"GET",
            success:function(data){
                $('.task-'+taskid).remove();
                toastr.success('<strong>Success!</strong> Record was successfully marked as complete.');

                toastr.options.timeOut = 1000;

                var rowCount = $('.task-table tr').length;

                if(rowCount > 1){

                } else {
                    $('.task-table tbody').html('<tr>' +
                        '<td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no tasks to display.</small></td>' +
                        '</tr>');
                }

            }
        });
    }
}

function editBillboardMessage() {
    let msgid = $('#modalShowBillboardMessage').find('.message_id').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/billboard-message/' + msgid + '/show',
        type:"GET",
        success:function(data){
            $('#modalShowBillboardMessage').modal('hide');
            $('#modalEditBillboardMessage').modal('show');

            $('#modalEditBillboardMessage').find('.message_id').val(data.message_id);
            if(data.client === ''){
                $('#modalEditBillboardMessage').find('.billboard_client').val('');
            } else {
                $('#modalEditBillboardMessage').find('.billboard_client').val(data.client_id);
                $('#modalEditBillboardMessage').find('.billboard_client').val(data.client_id).trigger('chosen:updated');
            }
            if(data.billboard_message === ''){
                $('#modalEditBillboardMessage').find('.billboard_message').val('');
            } else {
                $('#modalEditBillboardMessage').find('.billboard_message').val(data.billboard_message);
            }
        }
    });

}

function editUserTask() {
    let taskid = $('#modalShowUserTask').find('.task_id').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/user-task/' + taskid + '/show',
        type:"GET",
        success:function(data){
            $('#modalShowUserTask').modal('hide');
            $('#modalEditUserTask').modal('show');

            $('#modalEditUserTask').find('.task_id').val(data.task_id);
            if(data.client === ''){
                $('#modalEditUserTask').find('.task_client').val('');
            } else {
                $('#modalEditUserTask').find('.task_client').val(data.client_id);
                $('#modalEditUserTask').find('.task_client').val(data.client_id).trigger('chosen:updated');
            }
            if(data.task_type === ''){
                $('#modalEditUserTask').find('.task_type').val('');
            } else {
                $('#modalEditUserTask').find('.task_type').val(data.task_type);
            }
            if(data.task_type === 'Other'){
                $('#modalEditUserTask').find('.task_other_div').css('display','block');
                $('#modalEditUserTask').find('.task_other').val(data.task_other);
            } else {
                $('#modalEditUserTask').find('.task_other_div').css('display','none');
            }
            if(data.task_date === ''){
                $('#modalEditUserTask').find('.task_date').val('');
            } else {
                $('#modalEditUserTask').find('.task_date').val(data.task_date);
            }
            if(data.task_message === ''){
                $('#modalEditUserTask').find('.task_message').val('');
            } else {
                $('#modalEditUserTask').find('.task_message').val(data.task_message);
            }
        }
    });

}

function updateBillboardMessage(){
    let msgid = $("#modalEditBillboardMessage").find('.message_id').val();
    let client = $("#modalEditBillboardMessage").find('.billboard_client').val();
    let message = $("#modalEditBillboardMessage").find('.billboard_message').val();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/billboard-message/' + msgid + '/update',
        type:"POST",
        data:{billboard_message:message,billboard_client:client},
        success:function(data){
            var rowCount = $('.billboard-table tr').length;

            $("#modalEditBillboardMessage").find(".billboard_client").val('');
            $("#modalEditBillboardMessage").find(".billboard_client").val('').trigger('chosen:updated');
            $("#modalEditBillboardMessage").find(".billboard_message").val('');
            $("#modalEditBillboardMessage").modal('hide');

            toastr.success('<strong>Success!</strong> Message was successfully saved.');

            toastr.options.timeOut = 1000;

            if(rowCount > 1) {
                if(data.client === '') {
                    $('.message-'+msgid).html('<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')">' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>');
                } else {
                    $('.message-'+msgid).html('<td class="billboard" colspan="100%">' +
                        '<span class="pull-right clickable close-icon" onclick="completeBillboardMessage(' + data.message_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                        '<div class="card-block">' +
                        '<blockquote class="card-blockquote">' +
                        '<div class="blockquote-body" onclick="showBillboardMessage(' + data.message_id + ')"><strong>' + data.client + '</strong> - ' + data.billboard_message + '</div>' +
                        '</blockquote>' +
                        '</div>' +
                        '</td>');
                }
            }
        }
    });
}

function updateUserTask(){
    let taskid = $("#modalEditUserTask").find('.task_id').val();
    let client = $("#modalEditUserTask").find('.task_client').val();
    let message = $("#modalEditUserTask").find('.task_message').val();
    let date = $("#modalEditUserTask").find('.task_date').val();
    let type = $("#modalEditUserTask").find('.task_type').val();
    let other = $("#modalEditUserTask").find('.task_other').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/user-task/' + taskid + '/update',
        type:"POST",
        data:{task_message:message,task_client:client,task_type:type,task_date:date,task_other:other},
        success:function(data){
            var rowCount = $('.task-table tr').length;

            $("#modalEditUserTask").find(".task_client").val('');
            $("#modalEditUserTask").find(".task_client").val('').trigger('chosen:updated');
            $("#modalEditUserTask").find(".task_message").val('');
            $("#modalEditUserTask").modal('hide');

            toastr.success('<strong>Success!</strong> Task was successfully saved.');

            toastr.options.timeOut = 1000;

            if(rowCount > 1) {
                if(data.client === '') {
                    if(data.task_type === 'Other') {
                        $('.task-' + taskid).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    } else {
                        $('.task-' + taskid).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '</div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    }
                } else {
                    if(data.task_type === 'Other') {
                        $('.task-' + taskid).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_other + '<br /><small class="text-muted">' + data.client + '</small></div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    } else {
                        $('.task-' + taskid).html('<td class="usertask" colspan="100%">' +
                            '<span class="pull-right clickable close-icon" onclick="completeUserTask(' + data.task_id + ')" data-effect="fadeOut"><input type="checkbox" /></span>' +
                            '<div class="card-block">' +
                            '<blockquote class="card-blockquote">' +
                            '<div class="blockquote-body" onclick="showUserTask(' + data.task_id + ')"><strong>' + data.task_date + '</strong> - ' + data.task_type + '<br /><small class="text-muted">' + data.client + '</small></div>' +
                            '</blockquote>' +
                            '</div>' +
                            '</td>');
                    }
                }
            }
        }
    });
}

function deleteBillboardMessage() {
    let msgid = $('#modalShowBillboardMessage').find('.message_id').val();

    if (!confirm("Are you sure you want to delete this record?")){
        return false;
    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/billboard-message/' + msgid + '/delete',
            type:"GET",
            success:function(data){

                $('#modalShowBillboardMessage').modal('hide');
                $('.message-'+msgid).remove();
                toastr.success('<strong>Success!</strong> Record was successfully deleted.');

                toastr.options.timeOut = 1000;

                var rowCount = $('.billboard-table tr').length;

                if(rowCount > 1){

                } else {
                    $('.billboard-table tbody').html('<tr>' +
                        '<td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no messages to display.</small></td>' +
                        '</tr>');
                }

            }
        });
    }
}

function deleteUserTask() {
    let taskid = $('#modalShowUserTask').find('.task_id').val();

    if (!confirm("Are you sure you want to delete this record?")){
        return false;
    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/user-task/' + taskid + '/delete',
            type:"GET",
            success:function(data){

                $('#modalShowUserTask').modal('hide');
                $('.task-'+taskid).remove();
                toastr.success('<strong>Success!</strong> Record was successfully deleted.');

                toastr.options.timeOut = 1000;

                var rowCount = $('.task-table tr').length;

                if(rowCount > 1){

                } else {
                    $('.task-table tbody').html('<tr>' +
                        '<td colspan="100%" class="text-center"><small class="alert alert-info w-100 d-block text-muted">There are no tasks to display.</small></td>' +
                        '</tr>');
                }

            }
        });
    }
}

function composeWhatsapp(client_id){
    $('#modalSendWhatsapp').modal('show');

    $("#modalSendWhatsapp").find(".client_id").val('');
    $("#modalSendWhatsapp").find(".whatsapp_message").val('');
    $("#modalSendWhatsapp").find(".template option[value='']").prop('selected', true);
    $('#modalSendWhatsapp').find('.template').trigger('chosen:updated');

    if($(".send_whatsapp_form").is(":visible")) {

    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/clients/'+client_id+'/getdetail',
            type:"GET",
            dataType:"json",
            success:function(data){
                    $("#modalSendWhatsapp").find('.client_id').val(data.id);
                $('body').find('#modalSendWhatsapp').find('.recipient').empty();
                $('body').find('#modalSendWhatsapp').find('.recipient').append('<option value="' + data.contact + '">' + data.clname + ': ' + data.contact + '</option>');
                $('body').find('#modalSendWhatsapp').find('.recipient option[value="' + data.contact + '"]').prop('selected',true);
                $('body').find('#modalSendWhatsapp').find('.recipient').trigger('chosen:updated');
            }
        });
    }
    //tinymce.init(editor_config);
}

function sendWhatsapp(){
    if($(".send_whatsapp_form").is(":visible")) {
        $('.send_whatsapp_form:visible').submit();
    } else {
        var client_id = $("#modalSendWhatsapp").find(".client_id").val();
        var recipient = $("#modalSendWhatsapp").find(".recipient").val();
        var whatsapp_message = $("#modalSendWhatsapp").find(".whatsapp_message").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/message',
            type:"POST",
            data:{client_id:client_id, recipient:recipient, whatsapp_message:whatsapp_message},
            success:function(data){
                $("#modalSendWhatsapp").find(".client_id").val('');
                $("#modalSendWhatsapp").find(".recipient").val();
                $("#modalSendWhatsapp").find(".whatsapp_message").val('');
                $("#modalSendWhatsapp option[value='']").prop('selected', true);
                $("#modalSendWhatsapp").modal('hide');

                toastr.success('<strong>Success!</strong> Whatsapp was successfully sent.');

                toastr.options.timeOut = 1000;
            }
        });
    }
    $('#modalSendWhatsapp').modal('hide');
}

function getWhatsappTemplate(){
    let template = $("#modalSendWhatsapp").find('.template').val();
    var client_id = $("#modalSendWhatsapp").find(".client_id").val();
    // console.log(client_id)

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: '/whatsapptemplates/' + template + '/gettemplate/' + client_id,
        type:"GET",
        dataType:"json",
        success:function(data){
            $("#modalSendWhatsapp").find(".whatsapp_message").val(data.whatsapp_content);
        }
    });
}

function saveClientDetails(){
    if($('#crm_id').val() === '5'){
        if($('#parent_client').val() === ''){
            $('#parent_client').addClass('is-invalid');
        } else {
            let err = 0;
            $( ".kpi" ).each(function( index ) {
                if($( this ).val() == ''){
                    err++;
                    $( this ).addClass('is-invalid');
                } else {
                    $( this ).removeClass('is-invalid');
                }
                /*console.log( index + ": " + $( this ).val() );*/
            });

            if(err === 0) {
                $('#parent_client').removeClass('is-invalid');
                $('.clientdetailsform2:visible').submit();
                $('#overlay').fadeIn();
            }
        }
    } else {
        let err = 0;
        $( ".kpi" ).each(function( index ) {
            if($( this ).val() == ''){
                err++;
                $( this ).addClass('is-invalid');
            } else {
                $( this ).removeClass('is-invalid');
            }
            /*console.log( index + ": " + $( this ).val() );*/
        });

        if(err ===  0) {
            $('.clientdetailsform2:visible').submit();
            $('#overlay').fadeIn();
        }
    }
}

function saveClientDetailsModal(){
    $('.clientdetailsmodalform:visible').submit();
    $('#overlay').fadeIn();
}

function sendMessage(){
    $('#send_message_form').submit();
}

function saveUser() {
    $('#save_user_form').submit();
}

function saveForm() {
    $('#save_form_form').submit();
}

function saveWhatsappTemplate() {
    $('#whatsappform').submit();
}

function saveSettings() {
    $('#save_settings_form').submit();
}

function saveRole() {
    $('#save_role_form').submit();
    $('#overlay').fadeIn();
}

function saveFormSection() {
    $('#save_form_section_form').submit();
}

function saveDocument() {
    $('#save_document_form').submit();
}

function saveTemplate() {
    $('#save_template_form').submit();
}

function saveProcessGroup() {
    $('#save_process_group_form').submit();
}

function saveProcess() {
    $('#save_process_form').submit();
}

function saveStep() {
    $('#save_step_form').submit();
}

function saveDivision() {
    $('#save_division_form').submit();
}

function saveRegion() {
    $('#save_region_form').submit();
}

function saveArea() {
    $('#save_area_form').submit();
}

function saveOffice() {
    $('#save_office_form').submit();
}

function addressKYC(clientID)
{
    $('#overlay').fadeIn();
    axios.post('/api/address/kyc/individual', {
        client_id: clientID,
    })
        .then(function (response) {
            $('#overlay').fadeOut();

            // alert(response.data.message);
            notifyDialog(response.data.message);
        })
        .catch(function (error) {
            $('#overlay').fadeOut();

        });
}

function idvConfirm(clientID)
{
    $('#overlay').fadeIn();
    axios.post('/api/cpb/idv/confirm', {
        client_id: clientID,
    })
    .then(function (response) {
        $('#overlay').fadeOut();
        notifyDialog(response.data.message);
    })
    .catch(function (error) {
        $('#overlay').fadeOut();

    });
}

function getProofOfAddress(clientID)
{
    $('#overlay').fadeIn();
    axios.post('/api/cpb/getproofofaddress', {
        client_id: clientID,
    })
        .then(function (response) {
            $('#overlay').fadeOut();

            notifyDialog(response.data.message);
        })
        .catch(function (error) {
            $('#overlay').fadeOut();

        });
}

function getAVS(clientID)
{
    $('#overlay').fadeIn();
    axios.post('/api/cpb/getavs', {
        client_id: clientID,
    })
        .then(function (response) {
            $('#overlay').fadeOut();

            notifyDialog(response.data.message);
        })
        .catch(function (error) {
            $('#overlay').fadeOut();

        });
}

function editInfo(clientID) {
    $('.noedit').addClass('d-none');
    $('.noedit').removeClass('d-block');
    $('.yesedit').removeClass('d-none');
}

function saveInfo(clientID) {
    let trade_name = $('input[name="trade_name"]').val();
    let contact_name = $('input[name="contact_name"]').val();
    let contact_email = $('input[name="contact_email"]').val();
    let contact_officenr = $('input[name="contact_officenr"]').val();
    let contact_cellnr = $('input[name="contact_cellnr"]').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/saveinfo',
        data:{client_id:clientID, trade_name:trade_name, contact_name:contact_name, contact_email:contact_email, contact_cellnr:contact_cellnr, contact_officenr:contact_officenr},
        success: function (data) {
            if (data.result === 'success') {

                $('input[name="trade_name"]').val(data.trade_name);
                $('.trade_name').html(data.trade_name);

                $('input[name="contact_name"]').val(data.contact_name);
                if(data.contact_name === null){
                    $('.contact_name').html('No contact person captured.');
                } else {
                    $('.contact_name').html(data.contact_name);
                }

                $('input[name="contact_email"]').val(data.contact_email);
                if(data.contact_email === null){
                    $('.contact_email').html('No email captured.');
                } else {
                    $('.contact_email').html(data.contact_email);
                }

                $('input[name="contact_officenr"]').val(data.contact_officenr);
                if(data.contact_officenr === null){
                    $('.contact_officenr').html('No office number captured.');
                } else {
                    $('.contact_officenr').html(data.contact_officenr);
                }

                $('input[name="contact_cellnr"]').val(data.contact_cellnr);
                if(data.contact_cellnr === null){
                    $('.contact_cellnr').html('No cellphone number captured.');
                } else {
                    $('.contact_cellnr').html(data.contact_cellnr);
                }

                toastr.success('<strong>Success!</strong> Client Information successfully saved.');

                toastr.options.timeOut = 1000;

                $('.yesedit').addClass('d-none');
                $('.noedit').addClass('d-block');
                $('.noedit').removeClass('d-none');
            }
        }
    });
}

function isAzureSignedIn(local_redirect_url)
{
    $('#overlay').fadeIn();
    axios.get('/azure/issingedin', {})
    .then(function (response) {
        console.log(response.data.signedin);
        console.log(response);
        $('#overlay').fadeOut();
        if(response.data.signedin == 0){
            let host = window.location.origin;
            window.location.href = host + "/azure/signin?local_redirect_url=" + local_redirect_url;
        }
    })
    .catch(function (error) {
        $('#overlay').fadeOut();
    });
}



$("#client-basket-add").on('click',function(){

    var client_id = $(this).data('client');
    var checked_values = [];
    var nonchecked_values = [];
    $('#client-basket-form .collapse input[type=checkbox]').each( function() {
        if( $(this).is(':checked') ) {
            checked_values.push( $(this).val() );
        } else {
            nonchecked_values.push( $(this).val() );
        }
    });

    $('#overlay').fadeIn();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: '/forms/include-in-basket',
        data: {'client_id':client_id,'checked_values': checked_values,'nonchecked_values': nonchecked_values},
        success: function(data) {
            $('#overlay').fadeOut();

            toastr.success(data.success);

            toastr.options.timeOut = 1000;

            toggelClientBasket();
        }
    });
});

var editor_config = {
    path_absolute : "/",
    branding: false,
    relative_urls: false,
    convert_urls : false,
    menubar : false,
    paste_data_images: true,
    browser_spellcheck: true,
    selector: "textarea.my-editor",
    statusbar: false,
    setup: function (editor) {
        editor.on('change', function () {
            tinymce.triggerSave();
        });
    },
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
    ],
    paste_as_text: true,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",

    external_filemanager_path:"{{url('tinymce/filemanager')}}/",
    filemanager_title:"Responsive Filemanager" ,
    external_plugins: { "filemanager" : "{{url('tinymce')}}/filemanager/plugin.min.js"}
};

function hideClientInfo(){
    if($('.client-sidemenu').hasClass('hideClient')){
        $('.client-sidemenu').removeClass('hideClient');
        $('.client-info').removeClass('hide-client-info');
        $('.nav-client').removeClass('nav-client-expand');
        $('.client-content').removeClass('client-content-expand');
        $('#client-sidebar-btn').html('<span class="fa fa-angle-left"></span>')
    } else {
        $('.client-sidemenu').addClass('hideClient');
        $('.client-info').addClass('hide-client-info');
        $('.nav-client').addClass('nav-client-expand');
        $('.client-content').addClass('client-content-expand');
        $('#client-sidebar-btn').html('<span class="fa fa-angle-right"></span>')
    }
}

function saveReport() {
    $('#reportform').submit();
    $('#overlay').fadeIn();
    }