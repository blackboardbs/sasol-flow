<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCRFFormPartnerships extends Model
{
    protected $table = 'client_crf_form_partnerships';
    protected $primaryKey = 'id';
}
