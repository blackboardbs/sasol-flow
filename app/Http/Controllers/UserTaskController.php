<?php

namespace App\Http\Controllers;

use App\Client;
use App\MicrosoftCalendar;
use App\OfficeUser;
use App\User;
use App\UserTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\TokenStore\MicrosoftTokenCache;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;

class UserTaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){

        $date = $request->input('task_date');
        $client_id = $request->input('task_client');

        $client_name = Client::find($client_id);

            $task = new UserTask();
            $task->message = $request->input('task_message');
            $task->client_id = $client_id;
            $task->task_type = $request->input('task_type');
            $task->task_other = $request->input('task_other');
            $task->task_date = $date;
            $task->user_id = Auth::id();
            $task->office_id = OfficeUser::select('office_id')->where('user_id', Auth::id())->first()->office_id;
            $task->save();

            return response()->json(['message' => 'success', 'task_id' => $task->id, 'task_type' => $task->task_type, 'task_other' => $task->task_other, 'task_date' => Carbon::parse($task->task_date)->format('Y-m-d'), 'task_message' => $task->message, 'client' => ($task->client->company != null ? $task->client->company : $task->client->full_name), 'user' => $task->user->full_name]);
    }

    public function show(Request $request, $task_id){
        $task = UserTask::find($task_id);

        $task_arr = [
            "task_id" => $task->id,
            "client_id" => $task->client_id,
            "task_type"=>$task->task_type,
            "task_other"=>$task->task_other,
            "task_date"=>Carbon::parse($task->task_date)->format("Y-m-d"),
            "client" => ($task->client_id != null ? ($task->client->full_name != '' ? '' : $task->client_name($task->client_id)) : ''),
            "task_message" => $task->message
        ];

        return response()->json($task_arr);
    }

    public function update(Request $request, $task_id){

        $task = UserTask::find($task_id);
        $task->message = $request->input('task_message');
        $task->client_id = $request->input('task_client');
        $task->task_type = $request->input('task_type');
        $task->task_other = $request->input('task_other');
        $task->task_date = $request->input('task_date');
        $task->save();

        return response()->json(['message'=>'success','task_id'=>$task->id,'task_type'=>$task->task_type,'task_other'=>$task->task_other,'task_date'=>Carbon::parse($task->task_date)->format('Y-m-d'),'task_message'=>$task->message,'client'=>($task->client->company != null ? $task->client->company : $task->client->full_name),'user'=>$task->user->full_name]);
    }

    public function delete(Request $request, $task_id){

        UserTask::destroy($task_id);

        return response()->json(['message'=>"success"]);
    }

    public function complete(Request $request, $task_id){

        $task = UserTask::find($task_id);
        $task->status_id = 0;
        $task->task_completed_date = now();
        $task->save();

        return response()->json(['message'=>"success"]);
    }

    public function loadViewData()
    {
        $viewData = [];

        // Check for flash errors
        if (session('error')) {
            $viewData['error'] = session('error');
            $viewData['errorDetail'] = session('errorDetail');
        }

        // Check for logged on user
        if (session('userName'))
        {
            $viewData['userName'] = session('userName');
            $viewData['userEmail'] = session('userEmail');
            $viewData['userTimeZone'] = session('userTimeZone');
        }

        return $viewData;
    }

    private function getGraph(): Graph
    {
        // Get the access token from the cache
        $tokenCache = new MicrosoftTokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
        return $graph;
    }

    private function isAzureSignedIn()
    {
        // Get the access token from the cache
        $tokenCache = new MicrosoftTokenCache();
        $accessToken = $tokenCache->getAccessToken();

        // Create a Graph client
        $graph = new Graph();
        $graph->setAccessToken($accessToken);
        return $graph;
    }
}
