<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInputRadio extends Model
{
    public function form_section_input()
    {
        return $this->morphOne('App\FormSectionInputs', 'input');
    }

    public function items(){
        return $this->hasMany('App\FormInputRadioItem');
    }

    public function data()
    {
        return $this->hasMany('App\FormInputRadioData');
    }

    public function valuess()
    {
        return $this->hasMany('App\FormInputRadioData','form_input_radio_id');
    }
}
