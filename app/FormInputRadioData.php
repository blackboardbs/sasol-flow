<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInputRadioData extends Model
{
    protected $table = 'form_input_radio_data';

    public function item()
    {
        return $this->hasOne('App\FormInputRadioItem', 'id', 'form_input_radio_item_id');
    }
}
