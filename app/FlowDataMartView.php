<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlowDataMartView extends Model
{
    protected $table = "flow_datamart_view";
}
