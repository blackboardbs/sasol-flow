<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCRFFormManagers extends Model
{
    protected $table = 'client_crf_form_managers';
    protected $primaryKey = 'id';
}
