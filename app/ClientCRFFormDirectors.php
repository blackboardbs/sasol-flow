<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCRFFormDirectors extends Model
{
    protected $table = 'client_crf_form_directors';
    protected $primaryKey = 'id';
}
