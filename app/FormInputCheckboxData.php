<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInputCheckboxData extends Model
{
    protected $table = 'form_input_checkbox_data';

    public function item()
    {
        return $this->hasOne('App\FormInputCheckboxItem', 'id', 'form_input_checkbox_item_id');
    }
}
