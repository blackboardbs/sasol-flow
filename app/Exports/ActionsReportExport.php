<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromView;

class ActionsReportExport implements FromView
{
    public function __construct($clients)
    {
        $this->clients = $clients;
    }

    public function view(): View
    {
        return view('reports.assignedactivitiesexport', [
            'activities' => $this->clients,
        ]);
    }
}
