<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormInputCheckbox extends Model
{
    public function form_section_input()
    {
        return $this->morphOne('App\FormSectionInputs', 'input');
    }

    public function items(){
        return $this->hasMany('App\FormInputCheckboxItem');
    }

    public function data()
    {
        return $this->hasMany('App\FormInputCheckboxData');
    }

    public function valuess()
    {
        return $this->hasMany('App\FormInputCheckboxData','form_input_checkbox_id');
    }
}
