<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCRFFormShareholders extends Model
{
    protected $table = 'client_crf_form_shareholders';
    protected $primaryKey = 'id';

}
