<?php

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\Client;
use App\ClientComment;
use App\FormInputDate;
use App\FormInputDateData;
use App\FormInputDropdownData;
use App\FormInputTextData;
use App\Step;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;
use Carbon\Carbon;
use App\User;

class ClientSeeder extends Seeder
{
    public function __construct()
    {
        //
    }

    public function run()
    {
        /*$this->getClientFiles('Gaukes_Moster_Data_New.csv', 45);
        $this->getClientFiles('Philip_Roesch_Data_New.csv', 65);
        $this->getClientFiles('Rayno_van_Vuuren_Data_New.csv', 47);
        $this->getClientFiles('Stian_de_Witt_Data_New.csv', 43);*/
        //$this->getClientFiles('sasolonboarding_import.csv', 3);
        $this->getClientFiles('del_clients.csv', 3);
        //$this->getClientFiles('usercompare.csv', 3);
    }

    public function getClientFiles($fileName, $office_id)
    {
	    dd("clearly the right file");
        $csv = Reader::createFromPath(database_path('data/del_clients.csv', 'r'));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record_key => $record) {
	dd($record);
            $client = new Client;
            $client->company = isset($record['name']) && $record['name'] != '' && $record['name'] != 'NULL' ? $record['name'] : '';
            $client->first_name = isset($record['first_name']) && $record['first_name'] != '' && $record['first_name'] != 'NULL' ? $record['first_name'] : '';
            $client->last_name = isset($record['last_name']) && $record['last_name'] != '' && $record['last_name'] != 'NULL' ? $record['last_name'] : '';
            $client->email = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? $record['email'] : '';
            $client->contact = isset($record['mobileNo']) && $record['mobileNo'] != '' && $record['mobileNo'] != 'NULL' ? $record['mobileNo'] : '';
            $client->introducer_id = 1;
            $client->process_id = 44;
            $client->step_id = 234;
            $client->office_id = $office_id;
            $client->created_at = now();
            $client->save();

            $cp = new \App\ClientProcess;
            $cp->client_id = $client->id;
            $cp->process_id = 44;
            $cp->step_id = 234;
            $cp->active = 0;
            $cp->save();

            // name = 593
            $clientCompany = isset($record['name']) && $record['name'] != '' && $record['name'] != 'NULL' ? $record['name'] : '';
            $formInputCompanyData = new FormInputTextData();
            $formInputCompanyData->form_input_text_id = 592;
            $formInputCompanyData->data = trim($clientCompany);
            $formInputCompanyData->client_id = $client->id;
            $formInputCompanyData->user_id = 1;
            $formInputCompanyData->duration = 120;
            $formInputCompanyData->save();

            // name = 557
            $clientCompany = isset($record['first_name']) && $record['first_name'] != '' && $record['first_name'] != 'NULL' ? $record['first_name'] : '';
            $formInputCompanyData = new FormInputTextData();
            $formInputCompanyData->form_input_text_id = 557;
            $formInputCompanyData->data = trim($clientCompany);
            $formInputCompanyData->client_id = $client->id;
            $formInputCompanyData->user_id = 1;
            $formInputCompanyData->duration = 120;
            $formInputCompanyData->save();

            // name = 592
            $clientCompany = isset($record['last_name']) && $record['last_name'] != '' && $record['last_name'] != 'NULL' ? $record['last_name'] : '';
            $formInputCompanyData = new FormInputTextData();
            $formInputCompanyData->form_input_text_id = 593;
            $formInputCompanyData->data = trim($clientCompany);
            $formInputCompanyData->client_id = $client->id;
            $formInputCompanyData->user_id = 1;
            $formInputCompanyData->duration = 120;
            $formInputCompanyData->save();

            // email = 558
            $clientEmail = isset($record['email']) && $record['email'] != '' && $record['email'] != 'NULL' ? $record['email'] : 'N/A';
            $formInputEmailData = new FormInputTextData();
            $formInputEmailData->form_input_text_id = 558;
            $formInputEmailData->data = trim($clientEmail);
            $formInputEmailData->client_id = $client->id;
            $formInputEmailData->user_id = 1;
            $formInputEmailData->duration = 120;
            $formInputEmailData->save();

            // contact = 567
            $clientContact = isset($record['mobileNo']) && $record['mobileNo'] != '' && $record['mobileNo'] != 'NULL' ? $record['mobileNo'] : 'N/A';
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 567;
            $formInputContactData->data = trim($clientContact);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // engdate = 105
            $clientEngDate = isset($record['engDate']) && $record['engDate'] != '' && $record['engDate'] != 'NULL' ? $record['engDate'] : NULL;
            $formInputDateData = new FormInputDateData();
            $formInputDateData->form_input_date_id = 105;
            $formInputDateData->data = date('y-m-d', strtotime($clientEngDate));
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // assignedhunter = 96
            $engStatus = isset($record['assignedHunter']) && $record['assignedHunter'] != '' && $record['assignedHunter'] != 'NULL' ? $record['assignedHunter'] : NULL;
            $engStatus_id = null;
            switch (trim($engStatus)) {
                case 'Andrew Makofane';
                    $engStatus_id = 736;
                    break;
                case 'Ayanda webster';
                    $engStatus_id = 737;
                    break;
                case 'Barend Van Der Walt';
                    $engStatus_id = 738;
                    break;
                case 'Gugu Kwinda';
                    $engStatus_id = 739;
                    break;
                case 'Lerato Mokoena';
                    $engStatus_id = 740;
                    break;
                case 'Lufuno siphuma';
                    $engStatus_id = 741;
                    break;
                case 'patrick sehotsane';
                    $engStatus_id = 742;
                    break;
                case 'Razia Adam';
                    $engStatus_id = 743;
                    break;
                case 'Zuziwe Khanyi';
                    $engStatus_id = 744;
                    break;
            }

            if (isset($engStatus_id)) {
                $formEngStatusData = new FormInputDropdownData();
                $formEngStatusData->form_input_dropdown_id = 96;
                $formEngStatusData->form_input_dropdown_item_id = $engStatus_id;
                $formEngStatusData->client_id = $client->id;
                $formEngStatusData->user_id = 1;
                $formEngStatusData->duration = 120;
                $formEngStatusData->save();
            }

            // eng status = 90
            // Title : 694 - Attempted discovery call - call with no tangible outcome, 695 - Contract lost, 696 - Contract proposal submitted, 
            // 697 - Negotiation call or visit, 698 - Presentation calls (i.e. sales pitches), 699 - Successful discovery call - spoke with correct contact  and received
            $engStatus = isset($record['engStatus']) && $record['engStatus'] != '' && $record['engStatus'] != 'NULL' ? $record['engStatus'] : NULL;
            $engStatus_id = null;
            switch (trim($engStatus)) {
                case 'Attempted discovery call - call with no tangible outcome';
                    $engStatus_id = 694;
                    break;
                case 'Contract lost';
                    $engStatus_id = 695;
                    break;
                case 'Contract proposal submitted';
                    $engStatus_id = 696;
                    break;
                case 'Negotiation call or visit';
                    $engStatus_id = 697;
                    break;
                case 'Presentation calls (i.e. sales pitches)';
                    $engStatus_id = 698;
                    break;
                case 'Successful discovery call - spoke with correct contact  and received';
                    $engStatus_id = 699;
                    break;
            }

            if (isset($engStatus_id)) {
                $formEngStatusData = new FormInputDropdownData();
                $formEngStatusData->form_input_dropdown_id = 90;
                $formEngStatusData->form_input_dropdown_item_id = $engStatus_id;
                $formEngStatusData->client_id = $client->id;
                $formEngStatusData->user_id = 1;
                $formEngStatusData->duration = 120;
                $formEngStatusData->save();
            }


            // status = 91
            // Title : 700 - Current, 701 - Lost, 702 - Other, 703 - Pipeline
            $status = isset($record['status']) && $record['status'] != '' && $record['status'] != 'NULL' ? $record['status'] : NULL;
            $status_id = null;
            switch (trim($status)) {
                case 'Current';
                    $status_id = 700;
                    break;
                case 'Lost';
                    $status_id = 701;
                    break;
                case 'Other';
                    $status_id = 702;
                    break;
                case 'Pipeline';
                    $status_id = 703;
                    break;
            }

            if (isset($status_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 91;
                $formStatusData->form_input_dropdown_item_id = $status_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            // callscheduledate = 106
            $clientcallScheduleDate = isset($record['callScheduleDate']) && $record['callScheduleDate'] != '' && $record['callScheduleDate'] != 'NULL' ? $record['callScheduleDate'] : NULL;
            $formInputDateData = new FormInputDateData();
            $formInputDateData->form_input_date_id = 106;
            $formInputDateData->data = date('y-m-d', strtotime($clientcallScheduleDate));
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // priority = 92
            // Title : 704 - 1, 705 - 2, 706 - 3, 707 - 4
            $priorityLevel = isset($record['priorityLevel']) && $record['priorityLevel'] != '' && $record['priorityLevel'] != 'NULL' ? $record['priorityLevel'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case '1';
                    $priorityLevel_id = 704;
                    break;
                case '2';
                    $priorityLevel_id = 705;
                    break;
                case '3';
                    $priorityLevel_id = 706;
                    break;
                case '4';
                    $priorityLevel_id = 707;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 92;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            // operatorName = 568
            $operatorName = isset($record['operatorName']) && $record['operatorName'] != '' && $record['operatorName'] != 'NULL' ? $record['operatorName'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 568;
            $formInputContactData->data = trim($operatorName);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // dieselVolume = 569
            $dieselVolume = isset($record['dieselVolume']) && $record['dieselVolume'] != '' && $record['dieselVolume'] != 'NULL' ? $record['dieselVolume'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 569;
            $formInputContactData->data = trim($dieselVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //dieselVolumeUnit = 85
            // Title : 704 - 1, 705 - 2, 706 - 3, 707 - 4
            $priorityLevel = isset($record['dieselVolumeUnit']) && $record['dieselVolumeUnit'] != '' && $record['dieselVolumeUnit'] != 'NULL' ? $record['dieselVolumeUnit'] : NULL;
            $priorityLevel_id = null;
            $priorityLevel2_id = null;
            switch (trim($priorityLevel)) {
                case 'KG';
                    $priorityLevel_id = 708;
                    break;
                case 'M3';
                    $priorityLevel_id = 709;
                    break;
                case 'KG/M3';
                    $priorityLevel_id = 708;
                    $priorityLevel2_id = 709;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 85;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            if (isset($priorityLevel2_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 85;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel2_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            // oreVolume = 570
            $oreVolume = isset($record['oreVolume']) && $record['oreVolume'] != '' && $record['oreVolume'] != 'NULL' ? $record['oreVolume'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 570;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //oreVolumeUnit = 87
            // Title : 704 - Mpta
            $priorityLevel = isset($record['oreVolumeUnit']) && $record['oreVolumeUnit'] != '' && $record['oreVolumeUnit'] != 'NULL' ? $record['oreVolumeUnit'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case 'Mpta';
                    $priorityLevel_id = 710;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 87;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            //region = 88
            // Title : 704 - Mpta
            $priorityLevel = isset($record['region']) && $record['region'] != '' && $record['region'] != 'NULL' ? $record['region'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case 'Northern Cape';
                    $priorityLevel_id = 683;
                    break;
                case 'Limpopo';
                    $priorityLevel_id = 684;
                    break;
                case 'Mpumalanga';
                    $priorityLevel_id = 685;
                    break;
                case 'Gauteng';
                    $priorityLevel_id = 686;
                    break;
                case 'KZN';
                    $priorityLevel_id = 687;
                    break;
                case 'Free State';
                    $priorityLevel_id = 688;
                    break;
                case 'North West';
                    $priorityLevel_id = 710;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 88;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            //segment = 93
            // Title : 704 - Mpta
            $priorityLevel = isset($record['segment']) && $record['segment'] != '' && $record['segment'] != 'NULL' ? $record['segment'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case 'Low Tier';
                    $priorityLevel_id = 711;
                    break;
                case 'Medium Tier';
                    $priorityLevel_id = 712;
                    break;
                case 'Low & Mid Tier';
                    $priorityLevel_id = 713;
                    break;
                case 'Top Tier';
                    $priorityLevel_id = 714;
                    break;
                case 'Regional / Local';
                    $priorityLevel_id = 715;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 93;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            // mine type = 572
            $oreVolume = isset($record['mineType']) && $record['mineType'] != '' && $record['mineType'] != 'NULL' ? $record['mineType'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 572;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // location = 573
            $oreVolume = isset($record['location']) && $record['location'] != '' && $record['location'] != 'NULL' ? $record['location'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 573;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // industry = 574
            $oreVolume = isset($record['industry']) && $record['industry'] != '' && $record['industry'] != 'NULL' ? $record['industry'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 574;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // abilityToWin = 575
            $oreVolume = isset($record['abilityToWin']) && $record['abilityToWin'] != '' && $record['abilityToWin'] != 'NULL' ? $record['abilityToWin'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 575;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // totalMines = 576
            $oreVolume = isset($record['totalMines']) && $record['totalMines'] != '' && $record['totalMines'] != 'NULL' ? $record['totalMines'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 576;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // transportTypeId = 577
            $oreVolume = isset($record['transportTypeId']) && $record['transportTypeId'] != '' && $record['transportTypeId'] != 'NULL' ? $record['transportTypeId'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 577;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //contractId = 578
            $oreVolume = isset($record['contractId']) && $record['contractId'] != '' && $record['contractId'] != 'NULL' ? $record['contractId'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 578;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            // tenderDate = 107
            $clientcallScheduleDate = isset($record['tenderDate']) && $record['tenderDate'] != '' && $record['tenderDate'] != 'NULL' ? $record['tenderDate'] : NULL;
            $formInputDateData = new FormInputDateData();
            $formInputDateData->form_input_date_id = 107;
            $formInputDateData->data = date('y-m-d', strtotime($clientcallScheduleDate));
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            // contractExpiryOn = 108
            $clientcallScheduleDate = isset($record['contractExpiryOn']) && $record['contractExpiryOn'] != '' && $record['contractExpiryOn'] != 'NULL' ? $record['contractExpiryOn'] : NULL;
            $formInputDateData = new FormInputDateData();
            $formInputDateData->form_input_date_id = 108;
            $formInputDateData->data = date('y-m-d', strtotime($clientcallScheduleDate));
            $formInputDateData->client_id = $client->id;
            $formInputDateData->user_id = 1;
            $formInputDateData->duration = 120;
            $formInputDateData->save();

            //contractLengthYear = 579
            $oreVolume = isset($record['contractLengthYear']) && $record['contractLengthYear'] != '' && $record['contractLengthYear'] != 'NULL' ? $record['contractLengthYear'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 579;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //confidenceLevel = 94
            // Title : 704 - Mpta
            $priorityLevel = isset($record['segment']) && $record['segment'] != '' && $record['segment'] != 'NULL' ? $record['segment'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case '0';
                    $priorityLevel_id = 716;
                    break;
                case '5';
                    $priorityLevel_id = 717;
                    break;
                case '10';
                    $priorityLevel_id = 718;
                    break;
                case '15';
                    $priorityLevel_id = 719;
                    break;
                case '25';
                    $priorityLevel_id = 720;
                    break;
                case '35';
                    $priorityLevel_id = 721;
                    break;
                case '50';
                    $priorityLevel_id = 722;
                    break;
                case '60';
                    $priorityLevel_id = 723;
                    break;
                case '75';
                    $priorityLevel_id = 724;
                    break;
                case '80';
                    $priorityLevel_id = 725;
                    break;
                case '90';
                    $priorityLevel_id = 726;
                    break;
                case '100';
                    $priorityLevel_id = 727;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 93;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }

            //capitalInvestment = 580
            $oreVolume = isset($record['capitalInvestment']) && $record['capitalInvestment'] != '' && $record['capitalInvestment'] != 'NULL' ? $record['capitalInvestment'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 580;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //irrInPercent = 581
            $oreVolume = isset($record['irrInPercent']) && $record['irrInPercent'] != '' && $record['irrInPercent'] != 'NULL' ? $record['irrInPercent'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 581;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //npv = 582
            $oreVolume = isset($record['npv']) && $record['npv'] != '' && $record['npv'] != 'NULL' ? $record['npv'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 582;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //paybackPeriodInYear = 583
            $oreVolume = isset($record['paybackPeriodInYear']) && $record['paybackPeriodInYear'] != '' && $record['paybackPeriodInYear'] != 'NULL' ? $record['paybackPeriodInYear'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 583;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //creditTerms = 584
            $oreVolume = isset($record['creditTerms']) && $record['creditTerms'] != '' && $record['creditTerms'] != 'NULL' ? $record['creditTerms'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 584;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //marginContribution = 585
            $oreVolume = isset($record['marginContribution']) && $record['marginContribution'] != '' && $record['marginContribution'] != 'NULL' ? $record['marginContribution'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 585;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //probabilityInPercent = 586
            $oreVolume = isset($record['probabilityInPercent']) && $record['probabilityInPercent'] != '' && $record['probabilityInPercent'] != 'NULL' ? $record['probabilityInPercent'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 586;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //salesFunnelStage = 587
            $oreVolume = isset($record['salesFunnelStage']) && $record['salesFunnelStage'] != '' && $record['salesFunnelStage'] != 'NULL' ? $record['salesFunnelStage'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 587;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();


            //createdBy = 588
            $oreVolume = isset($record['createdBy']) && $record['createdBy'] != '' && $record['createdBy'] != 'NULL' ? $record['createdBy'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 588;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //updatedBy = 589
            $oreVolume = isset($record['updatedBy']) && $record['updatedBy'] != '' && $record['updatedBy'] != 'NULL' ? $record['updatedBy'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 589;
            $formInputContactData->data = trim($oreVolume);
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //createdAt = 590
            $oreVolume = isset($record['createdAt']) && $record['createdAt'] != '' && $record['createdAt'] != 'NULL' ? $record['createdAt'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 590;
            $formInputContactData->data = date('y-m-d', strtotime($oreVolume));
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //updatedAt = 591
            $oreVolume = isset($record['updatedAt']) && $record['updatedAt'] != '' && $record['updatedAt'] != 'NULL' ? $record['updatedAt'] : Null;
            $formInputContactData = new FormInputTextData();
            $formInputContactData->form_input_text_id = 591;
            $formInputContactData->data = date('y-m-d', strtotime($oreVolume));
            $formInputContactData->client_id = $client->id;
            $formInputContactData->user_id = 1;
            $formInputContactData->duration = 120;
            $formInputContactData->save();

            //role = 95
            // Title : 704 - Mpta
            $priorityLevel = isset($record['role']) && $record['role'] != '' && $record['role'] != 'NULL' ? $record['role'] : NULL;
            $priorityLevel_id = null;
            switch (trim($priorityLevel)) {
                case 'Consulting Engineer';
                    $priorityLevel_id = 728;
                    break;
                case 'General manager';
                    $priorityLevel_id = 729;
                    break;
                case 'Mine manager';
                    $priorityLevel_id = 730;
                    break;
                case 'Mine overseer';
                    $priorityLevel_id = 731;
                    break;
                case 'Operations buyer';
                    $priorityLevel_id = 732;
                    break;
                case 'Manager';
                    $priorityLevel_id = 733;
                    break;
                case 'Procurement manager';
                    $priorityLevel_id = 722;
                    break;
                case 'Quarry manager';
                    $priorityLevel_id = 723;
                    break;
            }

            if (isset($priorityLevel_id)) {
                $formStatusData = new FormInputDropdownData();
                $formStatusData->form_input_dropdown_id = 95;
                $formStatusData->form_input_dropdown_item_id = $priorityLevel_id;
                $formStatusData->client_id = $client->id;
                $formStatusData->user_id = 1;
                $formStatusData->duration = 120;
                $formStatusData->save();
            }
        }
        die('done with one');
    }
}
